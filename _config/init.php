<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 6-okt-2009
 */

function __autoload($klasse){
	require_once $GLOBALS['classes'] . '/class.' . $klasse . '.php';
}

date_default_timezone_set('Europe/Brussels');
ini_set('display_errors', 'on');

/*
$GLOBALS['root'] = "/~tim/sntsite/leerkrachten/";
//$GLOBALS['root'] = "/leerkrachten/";
$GLOBALS['fotosLk'] = "http://snt.be/afbeeldingen/fotoboek/afbeeldingen_personeel/";

*/
$GLOBALS['classes'] = "/opt/www/sntweb/web/www.snt-brugge.be/includes/";
$GLOBALS['htdocs'] = "/opt/www/sntweb/web/www.snt-brugge.be/leerkrachten/";
$GLOBALS['url'] = "http://localhost/~tim/sntsite/leerkrachten/";
$GLOBALS['bestanden'] = $GLOBALS['htdocs'] . "docs";
$GLOBALS['fotosLk'] = "http://snt.be/afbeeldingen/fotoboek/";
$GLOBALS['fotosLkFs'] = "../www.snt-brugge.be/afbeeldingen/fotoboek/";


session_start();

$initLoaded = true;

