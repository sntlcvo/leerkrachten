<?php

/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 08-dec-2009
 * algemene klasse met alles die elders niet thuishoort.
 */
class TabLknet2010
{
    public static function toonInhoudAsList($dir, $editable = false, $level = 0)
    {
        if ($level == 0) {
            $klasse = "id=\"menu\" class=\"filetree\"";
        }
        if (!is_array($dir)) {
            return false;
        }
        print "<ul $klasse>";
        foreach ($dir as $i => $v) {
            if (is_array($v)) {
                print "\t<li class=\"closed noshow\"><span class=\"folder\">$i</span>\n";
                $level++;
                self::toonInhoudAsList($v, $editable, $level);
                print "\t</li>\n";
            } else {
                $doel = (pathinfo($v, PATHINFO_EXTENSION) == "html") ? "class=\"fbox iframe\"" : "";
                print "<li class=\"closed\"><span class=\"file\"><a href=\"$v\" $doel title=\"$i\">$i</a>";
                if ($editable) {
                    print " <a class=\"fbox iframe\" href=\"{$GLOBALS['root']}_inhoud/popup/documentenWijzig.php?file=" . urlencode($v) . "\">
							<img src=\"{$GLOBALS['root']}_images/potlood.png\"></a>";
                }
                print "</span></li>";
            }
        }
        print "</ul>";
    }

    public static function toonInhoudAsList2($dir, $editable = false, $level = 0, $allowDownload = true)
    {
        $klasse = ($level == 0) ? "id=\"menu\" class=\"filetree\"" : '';
        if (!is_array($dir)) {
            return false;
        }
        print "<ul $klasse>";
        foreach ($dir as $i => $v) {
            if (is_array($v)) {
                print "\t<li class=\"closed noshow\"><span class=\"folder\">$i</span>\n";
                $level++;
                self::toonInhoudAsList2($v, $editable, $level, $allowDownload);
                print "\t</li>\n";
            } else {
                $doel = (pathinfo($v, PATHINFO_EXTENSION) == "html") ? "class=\"fbox iframe\"" : "";
                if ($allowDownload) {
                    print "<li><span class=\"file\"><a href=\"{$GLOBALS['root']}_inhoud/documentenSender.php?file=$v\" $doel title=\"$i\">$i</a>";
                } else {
                    print "<li><span class=\"file\">$i";
                }
                if ($editable) {
                    print " <a class=\"fbox iframe\" href=\"{$GLOBALS['root']}_inhoud/popup/documentenWijzig.php?file=" . urlencode($v) . "\">
							<img src=\"{$GLOBALS['root']}_images/potlood.png\"></a>";
                }
                print "</span></li>";
            }
        }
        print "</ul>";
    }
}