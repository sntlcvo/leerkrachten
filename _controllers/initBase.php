<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 6-okt-2009
 */
date_default_timezone_set('Europe/Brussels');

$GLOBALS['bestanden'] = "docs";
$GLOBALS['root'] = "/";

//$GLOBALS['docs'] = realpath($_SERVER['DOCUMENT_ROOT'] . '/../docs/');
$GLOBALS['DOC_ROOT'] = realpath($_SERVER['DOCUMENT_ROOT'] . '/../');
$GLOBALS['fotosLk'] = "http://snt.be/afbeeldingen/fotoboek/";

//$GLOBALS['fotosLkFs'] = "../htdocs/afbeeldingen/fotoboek/afbeeldingen_personeel/";
$GLOBALS['fotosLkFs'] = "../www.snt-brugge.be/afbeeldingen/fotoboek/";
ini_set('display_errors', 'on');


$initLoaded = true;

set_include_path(implode(PATH_SEPARATOR, array(
	realpath(BASE_PATH . '/../includes'),
	realpath(BASE_PATH . '/../library'),
    realpath(BASE_PATH . '/../library/includes'),
	realpath(BASE_PATH . '/_forms'),
	get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';

$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('Tab_', '');
$loader->setFallbackAutoloader(true);

$config = new Zend_Config_Ini(BASE_PATH.'/../library/includes/configs/application.ini', 'production');

$params = array(
            'host' => $config->db->host,
            'username' => $config->db->user,
            'password' => $config->db->password,
            'dbname' => $config->db->db);
$db = Zend_Db::factory('PDO_MYSQL', $params);
Zend_Db_Table_Abstract::setDefaultAdapter($db);

$writer = new Zend_Log_Writer_Firebug();
$logger = new Zend_Log($writer);
$logger->registerErrorHandler();

session_start();

$date = new DateTime();
$id = md5($date->format('Ymd'));
//Zend_Debug::dump($_SESSION);
if (isset($_REQUEST['zimbraid'])){
	$_SESSION['objPersoneel'] = new TabPersoneel();
	$r = $_SESSION['objPersoneel']->aanmeldControleZimbra($_REQUEST['zimbrauser']);
	if ($r == true){
		$_SESSION['loginOK'] = true;
	}
} elseif (isSet($_SESSION['objPersoneel'])) {
	$sid = 'zimbraid=' . $id . '&zimbrauser=' . $_SESSION['objPersoneel']->getEmail();
}
