<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 11-sep-2009
 */
//require '../includes/init.php';

require 'class.TabLknet2010.php';
/*
 function __autoload($classname)
 {
 if (file_exists('../includes/class.'.$classname.'.php')){
 //print "laden van $classname";
 require_once '../includes/class.' . $classname .'.php';
 }
 }
 */
//print BASE_PATH;

set_include_path(implode(PATH_SEPARATOR, array(
realpath(BASE_PATH . '/../includes'),
realpath(BASE_PATH . '/../zf/library'),
get_include_path(),
)));

//print get_include_path();
require_once 'Zend/Loader/Autoloader.php';

$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('Tab_', '');
$loader->setFallbackAutoloader(true);


$config = new Zend_Config_Ini(BASE_PATH.'/../includes/configs/application.ini', 'production');
$params = array(
            'host' => $config->db->host,
            'username' => $config->db->user,
            'password' => $config->db->password,
            'dbname' => $config->db->db);
$db = Zend_Db::factory('PDO_MYSQL', $params);
Zend_Db_Table_Abstract::setDefaultAdapter($db);

require 'initBase.php';