<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 6-okt-2009
 */
date_default_timezone_set('Europe/Brussels');

$GLOBALS['bestanden'] = "docs";
//$GLOBALS['root'] = "/~tim/sntsite/leerkrachten/";
$GLOBALS['root'] = "/leerkrachten/";

//$GLOBALS['docs'] = realpath($_SERVER['DOCUMENT_ROOT'] . '/../docs/');
$GLOBALS['DOC_ROOT'] = realpath($_SERVER['DOCUMENT_ROOT'] . '/../');
$GLOBALS['fotosLk'] = "http://snt.be/afbeeldingen/fotoboek/afbeeldingen_personeel/";

//$GLOBALS['fotosLkFs'] = "../htdocs/afbeeldingen/fotoboek/afbeeldingen_personeel/";
$GLOBALS['fotosLkFs'] = "../www.snt-brugge.be/afbeeldingen/fotoboek/afbeeldingen_personeel/";
ini_set('display_errors', 'off');

session_start();

$initLoaded = true;

