<?php
class kalenderForm
{
	static public function create($action = 'new')
	{
		$form = new Zend_Form();
		$view = new Zend_View();
		
		$form->setView($view)
			->setAction('kalenderVerwerk.php')
			->setMethod('post')
			->setAttrib('id', 'test');
			
		$titel = $form->createElement('text', 'titel', array(
						'label' => 'Titel'))
					->setRequired(true);
		$bericht = $form->createElement('textarea', 'ckeditor')
					->setRequired(true);
		$datumAct = $form->createElement('text', 'datum', array(
						'label' => 'Datum activiteit',
						'class' => 'datepicker',
						'maxlength' => 10,
						'size' => 10
					));
		$beginUur = $form->createElement('text', 'beginUur', array(
						'label' => 'Begin uur activiteit',
						'maxlength' => 2,
						'size' => 2, 
					))
						->addValidator('digits');
		$beginMin = $form->createElement('text', 'beginMin', array(
						'label' => 'Begin minuten activiteit',
						'maxlength' => 2,
						'size' => 2, 
					));
					
		$datumPubBegin = $form->createElement('text', 'beginPub', array(
						'label' => 'Begin publicatie',
						'class' => 'datepicker',
						'maxlength' => 10,
						'size' => 10
					));
		$datumPubEind = $form->createElement('text', 'eindPub', array(
						'label' => 'Einde publicatie',
						'class' => 'datepicker',
						'maxlength' => 10,
						'size' => 10
					));
		$id = $form->createElement('hidden', 'id')
						->setValue(0);
		if ($action == 'edit'){
			$wis = $form->createElement('checkbox', 'wis', array(
						'label' => 'Wis activiteit'
					));
			$knop = $form->createElement('submit', 'submit', array(
						'label' => 'Wijzig'
					));
		} else {
			$knop = $form->createElement('submit', 'submit', array(
						'label' => 'Verzend'
					));		
		}
		$form->addElement($titel)
			->addElement($bericht)
			->addElement($datumAct)
			->addElement($beginUur)
			->addElement($beginMin)
			->addElement($datumPubBegin)
			->addElement($datumPubEind)
			->addElement($id)
			->addElement($knop)
			;
		if ($action == 'edit'){
			$form->addElement($wis);
		}
		
		return $form;
	}
}