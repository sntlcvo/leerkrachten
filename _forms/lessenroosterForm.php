<?php
class lessenroosterForm extends myForm
{
	static public function vakkenlijst($action, $vakkenLijst, $vakkenId)
	{
		self::__init();
		
		self::$form
			->setAction($action);
			
        $vakkenSelect = self::$form->createElement('select', 'vak', 
            array('label' => 'Kies het vak uit deze lijst'))
            ->setMultiOptions($vakkenLijst)
            ->setValue($vakkenId);
            
        $knop = self::$form->createElement('submit', 'submit', array(
                        'label' => 'Verzend'
                    )); 
            
        self::$form->addElement($vakkenSelect)
            ->addElement($knop);
            
        return self::$form;
	}
	
   static public function korteInhoud($action, $vakId, $vakInhoud)
    {
        self::__init();
        
        self::$form
            ->setAction($action);
            
        $id = self::$form->createElement('hidden', 'id')
            ->setValue($vakId);
        
        $inhoud = self::$form->createElement('textarea', 'inhoud', 
                array(
                    'label' => 'Inhoud',
                    'rows' => 20,
                    'cols' => 20,
                    'class' => 'ckeditor',
                ))
                
                ->setValue($vakInhoud);
            
        $knop = self::$form->createElement('submit', 'submit', 
                array('label' => 'Verzend')); 
            
        self::$form->addElement($id)
            ->addElement($inhoud)
            ->addElement($knop);
            
        return self::$form;
    }
}