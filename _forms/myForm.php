<?php 
class myForm
{
    static $form;
    static $view;
    
    static function __init()
    {
        self::$form = new Zend_Form();
        self::$view = new Zend_View();
        
        self::$form->setView(self::$view)
            ->setMethod('post');
    }
}