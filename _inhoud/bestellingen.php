<?php require 'bestellingenController.php'; ?>
<h1>Overzicht bestellingen</h1>
<p><a href="<?php print $GLOBALS['root']?>_inhoud/popup/bestellingenNieuw.php" class="fbox_small iframe">voeg een nieuwe bestelling toe</a></p>

<div id="bestellingen">
<table>
	<tr>
		<th>leerkracht</th>
		<th>wat</th>
		<th>date</th>
		<th>geleverd</th>
	</tr>
	<tr>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<?php foreach ($bestellingen as $b):?>
	<tr onclick="toonVeld('#sub_<?php print $b['bestelId']?>', '.subitems')" class="handje">
		<td width="260"><?php print $b['leerkracht']?></td>
		<td><?php print $b['wat']?></td>
		<td><?php TabFuncties::printDatum($b['date'], true)?></td>
		<td align="center" width="70"><?php print $b['geleverd']?></td>
	</tr>
	<tr id="sub_<?php print $b['bestelId']?>" class="subitems" style="display:none">
		<td><?php print $b['info']?></td>
		<td><?php print ($b['date_besteld'] != "0000-00-00") ? TabFuncties::printDatum($b['date_besteld'], false) : "nog niet"?></td>
		<td><?php print $b['firma']?></td>
		<td align="center">
			<a class="fbox_small iframe" href="<?php print $GLOBALS['root']?>_inhoud/popup/bestellingenWijzig.php?id=<?php print $b['bestelId']?>">
			wijzig</a></td>
	</tr>
	<tr><td colspan="5" class="onderlijn"></td></tr>
	<?php endforeach;?>
</table>

</div>
