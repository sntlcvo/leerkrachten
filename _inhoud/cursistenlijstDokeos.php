<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 2-dec-2009
 */
require 'cursistenlijstDokeosController.php';

?>
<h1>Cursistenlijsten</h1>
<p>lijst op basis van de dokeosgegevens</p>
<form method="post" action="index.php?actie=submenu__inhoud&value=2__cursistenlijstDokeos">
	kies de klascode: <select name="klasCodeId"><?php print $klassenLijst?></select>
	<input type="submit" value="verzend" name="submit" />
</form>
<?php if (is_array($userLijst)):?>
<h2>dit zijn de gebruikers</h2>
<ul>
<?php foreach ($userLijst AS $user):?>
	<li><?php print $user['lastname'] . " " . $user['firstname'] . ", " . $user['username']?></li>
<?php endforeach;?>
</ul> 
<?php endif;?>

<?php if (is_array($userLijst)):?>
<h2>dit zijn de gebruikers voor de evalutieproeven</h2>
<ul>
<?php foreach ($userLijst AS $user):?>
	<li><?php print $user['lastname'] . " " . $user['firstname']?></li>
<?php endforeach;?>
</ul> 
<?php endif;?>