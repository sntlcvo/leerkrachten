<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 23-sep-2009
 */
require 'defaultController.php'

?>
<h1>Zimbra</h1>
<p>Het Leerkrachtennet werd overgebracht naar Zimbra. Maak vanaf nu gebruik van SNT Plus op <a href="http://zimbra.snt.be/" target="_blank">SNT Zimbra</a></p>
<?php if (is_array($verjaardagen)):?>
<h1>Verjaardagen</h1>
<ul>
<?php foreach ($verjaardagen as $vd):?>
	<li class="noshow"><?php print $vd['voll_naam']?> (<?php print $vd['verjaardag']?>)</li>
<?php endforeach; ?>
</ul>
<?php endif;?>
<h1>Meer info over het leerkrachtennet</h1>
die vind je <a href="_inhoud/documentenSender.php?file=/opt/www/sntweb/web/docs/lknet/to/tips_en_trucs/sitemap.pdf">hier</a>
<h1>Webmail</h1>
rechtstreekse toegang tot de <a href="http://zimbra.snt.be/" target="_blank">webmail</a>
<h1>Mailproblemen?</h1>
status terug te vinden op: <a href="http://priorweb.be/helpdesk/status" target="_blank">Statuspagina PriorWeb</a>