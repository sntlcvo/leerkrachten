<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 2-okt-2009
 */

function createListedItems($data)
{
    $vorigAantal = 0;
    $regels = [];
    foreach ($data as $index => $value) {
        $regel = "";
        $beginRegel = "";
        $eindRegel = "";
        $indexA = explode("_-_", $index);
        $aantal = $indexA[0];
        $index = $indexA[1];
        for ($i = 1; $i <= $aantal; $i++) {
            $beginRegel .= "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $vorigAantal = $aantal;
        $regels[] = "<p id=\"sub_$index\">$beginRegel$index ($value)</p>\n";
        // <li ><p class=\"handje\">$icon <a href=\"$inhoud\" target=\"_blank\">$naam</a></p></li>
    }
    return $regels;
}

$folderArr = explode('__', $_GET['value']);
$folder = $folderArr[1];
/*
$soort = $params[0];
$sub = $params[1];
*/
//print_r($params);
$soort = array_shift($params);
$sub = implode('/', $params);

//$extras = $_GET['extras'];

$allExtras = explode('_-_', $_GET['extras']);

//$allowRootA = explode("_", $extras);
if (isset($allExtras[0])) {
    $allowRootA = explode("_", $allExtras[0]);
    $allowRoot = $allowRootA[1];
}

$allowDownload = null;
if (isset($allExtras[1])) {
    $allowDownloadA = explode('_', $allExtras[1]);
    $allowDownload = $allowDownloadA[1];
}
$download = true;

if ($allowDownload === 'false') {
    $download = false;
}

$objFS = new TabFileSystem();
$files = $objFS->readDir($GLOBALS['DOC_ROOT'] . "/docs/$soort/$sub", true);
$bestanden = $objFS->getFullFileNames($files);
$lijst = createListedItems($bestanden);

//$toonUpload = ($soort == "lknet" && $_SESSION['objPersoneel']->getGroep() < 20) ? false : true;
$toonUpload = ($_SESSION['objPersoneel']->getGroep() >= $objMenu->getMinUpload($folder))
    ? true : false;
