<?php

define('BASE_PATH', realpath(dirname(__FILE__).'/..'));
require '../_controllers/initBase.php';

$bestand = $_GET['file'];
$bestandsnaam = basename($bestand);


$finfo = new finfo(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension


/* get mime-type for a specific file */
$fileInfo = $finfo->file($bestand);
// We'll be outputting a PDF
header("Content-type: $fileInfo");

// It will be called downloaded.pdf
header("Content-Disposition: attachment; filename=\"$bestandsnaam\"");

// The PDF source is in original.pdf
readfile($bestand);
