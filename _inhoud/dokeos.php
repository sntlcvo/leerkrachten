<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 30-okt-2009
 */

?>
<h1>Administratie dokeos</h1>
<ul>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/dokeosUserPassword.php" class="fbox_small iframe" title="Wijzigen wachtwoord cursist">Wijzigen wachtwoord cursist</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/dokeosUserEmail.php" class="fbox_small iframe">Opvragen emailadres 1 cursist</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/dokeosUserEmailClass.php" class="fbox iframe">Opvragen emailadres op klascode</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/dokeosClasscodeGenerator.php" class="fbox iframe">Klascodegenerator</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/dokeosClass2Course.php" class="fbox_small iframe">Klassen aan cursussen toevoegen</a></li>
	<!--  wachten op vraag van sophie tot de nieuwe versie!! -->
	<!--
	Dokeos: gebruikers in klas 
	Dokeos: beheerders aan cursus toevoegen (beheerders) 
	Dokeos: exporteren van cursuscodes 
	Dokeos: klassen aan cursus toevoegen  
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/dokeosUploadUsers.php" class="fbox_small iframe">Gebruikers aan Dokeos toevoegen</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrErasePeriod.php" class="fbox iframe">Gebruikers aan klas toevoegen</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrCourses.php" class="fbox iframe">Beheerders aan cursus toevoegen</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrCourses.php" class="fbox iframe">Exporteren van cursuscodes</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrCourses.php" class="fbox iframe">Klassen aan cursus toevoegen</a></li>
	-->
</ul>

