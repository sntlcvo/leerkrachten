<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2010.1 - maart 2010
 */

require 'dokeosKlassenController.php';

?>
<h1>Toevoegen van klassen aan Dokeos</h1>

<p>hiermee voegt u klassen toe aan Dokeos</p>
<form method="post" action="_inhoud/popup/dokeosKlassenVerwerk.php"
	id="frmToevoegen">
<div id="lijstKlascodes" style="float: left">
    <select name="klascodes[]" multiple="multiple" size="10" id="klascodes">
	<?php foreach ($klassenLijst as $id => $kl): ?>
	<option value="<?php print $id?>"><?php print $kl?></option>
	<?php endforeach;?>
</select></div>
<div id="" style="float: left"><input type="submit" name="submit"
	value="toevoegen -&gt;>" id="smbToevoegen" /></div>
<div id="lijstDokeoscodes"><select name="dokeosCodes" size="10"
	style="width: 340px" id="dokeoscodes">
	<?php foreach ($dokeosCodes as $id => $kl): ?>
	<option value="<?php print $id?>"><?php print $id ?> - <?php print $kl?></option>
	<?php endforeach;?>
</select></div>
</form>
<br />
<div id="status"></div>

<script type="text/javascript">
jQuery('#smbToevoegen').click(function(){
        aantaldokeos = jQuery('#dokeoscodes :selected').size();
        aantalklas = jQuery('#klascodes :selected').size();
        if (aantaldokeos == 0 || aantalklas == 0){
            alert('je dient links minstens 1 klascode en rechts 1 dokeoscode te selecteren')
        } else {
            a = confirm('weet u zeker dat u deze klassen wilt toevoegen aan de dokeoscursus?');
            if (a){
            $frmToevoegen = jQuery('#frmToevoegen');
            url = $frmToevoegen.attr('action');
            data = $frmToevoegen.serialize();

            jQuery.post(url, jQuery("#frmToevoegen").serialize(), function(data){
                    jQuery('#status').html(data);
                    });
            }
        }
	return false;
});
</script>
