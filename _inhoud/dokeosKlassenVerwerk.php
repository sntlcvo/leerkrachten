<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 4-mrt-2010
 */

require 'init.php';
$objD = new TabDokeosCourses();
$aantal = 0;
foreach($_POST['klascodes'] as $kcId){
	$aantal += $objD->addClassId2Course($_POST['dokeosCodes'], $kcId);
}

$klassen = ($aantal == 1) ? 'klas' : 'klassen'; 

print "er werden $aantal $klassen toegevoegd aan " . $_POST['dokeosCodes'];