$formulier = jQuery('#formulier');
$cursisten = jQuery('#cursisten');

$formulier.submit(function(){
	$cursisten.html('<img src="_images/spinner.gif" border="0" />');
	url = $formulier.attr('action');
	jQuery.post(url, $formulier.serialize(), function(data){
		$cursisten.html(data);
	});
	return false;
});