<?php
/**
 * @package lknet
 * @author tim
 * @version 2010.1 - 7-mei-2010
 */
require 'exportDokeosCursistenController.php';
?>
<h2>Export van Dokeos voor LimeSurvey</h2>
<form method="post" action="_inhoud/popup/exportDokeosCursistenVerwerk.php" id="formulier">
<table>
	<tr>
		<td rowspan="2"><select name="klascodes[]" multiple="multiple" size="20"><?php print $klascodesLijst?></select></td>
		<td rowspan="2" width="50"></td>
		<td height="40"><input type="submit" value="verzend" name="submit" /></td>
	</tr>
	<tr>
		
		<td id="cursisten" valign="top"></td>
	</tr>
</table>
</form>

<script type="text/javascript" src="_inhoud/exportDokeosCursisten.js"></script>