<?php
/**
 * @package lknet
 * @author tim
 * @version 2010.1 - 7-mei-2010
 */

$objD = new TabDokeosCourses();

$klascodes = TabFuncties::htmlize($objD->getAllClassCodes(true), false);
$klascodesLijst = TabFuncties::createOptionList($klascodes);