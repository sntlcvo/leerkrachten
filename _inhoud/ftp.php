<?php
require 'ftpController.php';
?>
<h1>FTP-gebruikers</h1>
<p><a href="<?php print $GLOBALS['root']?>_inhoud/popup/ftpNieuw.php" class="fbox_small iframe">
	voeg nieuwe gebruiker toe</a></p>

<p>
<?php if($_GET['toonWachtwoord'] == "ja"): ?>
<a href="index.php?actie=submenu__inhoud&value=6__ftp&toonWachtwoord=nee">verberg wachtwoord</a>
<?php else: ?>
<a href="index.php?actie=submenu__inhoud&value=6__ftp&toonWachtwoord=ja">toon wachtwoord</a>
<?php endif;?>
</p>
<table width="100%">
<tr>
	<th>gebruikersnaam</th>
	<th>wachtwoord</th>
	<th>homedir</th>
	<th># logins</th>
	<th>laatste toegang</th>
	<th>laatste wijziging</th>
</tr>
<?php foreach ($users as $u):?>
<tr>
	<td class="onderlijn"><?php print $u['userid']?></td>
	<td class="onderlijn"><?php print $u['passwd']?></td>
	<td class="onderlijn"><?php print $u['homedir']?></td>
	<td class="onderlijn"><?php print $u['count']?></td>
	<td class="onderlijn"><?php print $u['accessed']?></td>
	<td class="onderlijn"><?php print $u['modified']?></td>
	<!-- td class="onderlijn">wijzig <?php print $u['id']?> </td-->
</tr>
<?php endforeach;?>

</table>