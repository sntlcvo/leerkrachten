<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2011.1
 */
require 'ideeenbusController.php';

?>
<h1>idee&euml;nbus</h1>
<p>Heb je een idee / opmerking? Laat het ons dan weten via dit formulier</p>
<?php if (isSet($status)): ?>
<?php print $status?>
<?php endif;?>
<form method="post" action="index.php?actie=submenu__inhoud&value=87__ideeenbus" id="formulier">
<table border="0">
<tr>
	<td>verstuur anoniem: </td>
	<td><input type="checkbox" name="anoniem"></td>
</tr>
<tr>
	<td>onderwerp: </td>
	<td><input type="text" name="subject"></td>
</tr>
<tr>
	<td>bericht: </td>
	<td><textarea rows="10" cols="30" name="message"></textarea></td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" name="submit" value="verzend"></td>
</tr>
</table>
</form>