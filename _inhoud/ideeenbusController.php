<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2011
 */

if (isSet($_POST['submit'])){
	$naam = (!isset($_POST['anoniem'])) ? $_SESSION['objPersoneel']->getNaam() : 'anoniem';
	$onderwerp = $_POST['subject'];
	$message = $_POST['message'];
	$bericht = <<<eod
Een idee van $naam:
Onderwerp: $onderwerp
Bericht:
$message
eod;
	$headers = "FROM: website@snt.be\r\n";
	mail("iko@snt.be", "ideeenbus: $onderwerp", $bericht, $headers);
	$status = "Uw bericht werd verzonden";
}

//var_dump($_SESSION['objPersoneel']->getNaam());