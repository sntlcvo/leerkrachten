<h2>Kalender schooljaar 2008-2009</h2>
<p><strong>Start schooljaar:</strong> maandag 3 september 2007</p>
<p><strong>Vakanties:</strong></p>
<table border="1" cellpadding="2" cellspacing="2">
	<tr>
		<th width="35%" scope="row"><span class="style28">Herfstvakantie</span></th>
		<td width="65%">26/10/08 t.e.m. 02/11/08</td>
	</tr>
	<tr>
		<th scope="row">Kerstvakantie</th>
		<td>21/12/08 t.e.m. 04/01/09</td>
	</tr>
	<tr>
		<th scope="row">Krokusvakantie</th>
		<td>22/02/09 t.e.m. 01/03/09</td>
	</tr>
	<tr>
		<th scope="row">Paasvakantie</th>
		<td>05/04/09 t.e.m. 19/04/09</td>
	</tr>
	<tr>
		<th scope="row">Andere verlofdagen</th>
		<td>11/11/08<br />
		01/05/09<br />
		02/05/09<br />
		21/05/09<br />
		22/05/09<br />
		23/05/09<br />
		01/06/09</td>
	</tr>
</table>
