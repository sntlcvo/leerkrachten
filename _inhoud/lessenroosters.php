<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 30-okt-2009
 */


?>
<h1>Administratie lessenroosters</h1>
<ul>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrImportCursussen.php" class="fbox_wide iframe">Importeren van lessenrooster</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrLinkCourse2Web.php" class="fbox iframe">Koppelen snt.be</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrCourseData.php" class="fbox_wide iframe">Wijzigen cursusgegevens</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrErasePeriod.php" class="fbox_small iframe">Periode onbeschikbaar maken op snt.be</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrCourses.php" class="fbox_small iframe">Wijzigen cursusnaam</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrVakken.php" class="fbox_xwide iframe">Nieuwe vakken aanmaken</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrAddOneCourse.php" class="fbox_wide iframe">Toevoegen van 1 cursus</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrDeleteOneCourse.php" class="fbox_small iframe">Wissen van 1 cursus</a></li>
	<li><a href="<?php print $GLOBALS['root']?>_inhoud/popup/lrEditShortContentCursussen.php" class="fbox_wide iframe">Wijzigen korte inhoud cursussen</a></li>
</ul>
