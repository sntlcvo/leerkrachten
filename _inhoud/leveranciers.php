<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 17-nov-2009
 */

require 'leveranciersController.php';
?>
<h1>Overzicht leveranciers</h1>
<a href="<?php print $GLOBALS['root']?>_inhoud/popup/leveranciersNieuw.php" class="fbox iframe">Voeg een nieuwe toe</a>
<table width="600"> 
<?php foreach($leveranciers as $l):?>
	<tr>
		<td width="250">
			<?php print $l['firma']?>
			<?php if ($l['contact'] != "" || $l['tech_contact'] != ""):?> 
			(<?php print $l['contact']?> 
				<?php if ($l['contact'] != "" && $l['tech_contact'] != ""):?>
				/
				<?php endif;?>
			<?php print $l['tech_contact']?>)
			<?php endif;?>
		</td>
		<td width="100">
			<?php print $l['tel']?> 
				<?php if ($l['tel'] != "" && $l['gsm'] != ""):?>
				/ 
				<?php endif; ?>
			<?php print $l['gsm']?>
		</td>
		<td><?php print $l['email']?></td>
		<td width="50"><a href="#" onclick="toonVeld('#sub_<?php print $l['id']?>', '.subveld')">meer</a></td>
	</tr>
	<tr id="sub_<?php print $l['id']?>" class="subveld" style="display: none">
		<td><?php print $l['adres']?>, <?php print $l['postcode']?> <?php print $l['gemeente']?></td>
		<td><?php print $l['fax']?></td>
		<td><?php print $l['website']?></td>
		<td>
			<a href="<?php print $GLOBALS['root']?>_inhoud/popup/leveranciersWijzig.php?id=<?php print $l['id']?>" class="fbox iframe">wijzig</a></td>
	</tr>
	<tr>
		<td colspan="4" height="1" class="onderlijn"></td>
	</tr>
<?php endforeach;?>
</table>