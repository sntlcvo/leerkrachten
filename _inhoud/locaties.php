<?php
require 'locatiesController.php';
?>
<h1>Overzicht locaties</h1>
<a href="<?php print $GLOBALS['root']?>_inhoud/popup/locatiesNieuw.php" title="voeg nieuwe locatie toe" class="fbox iframe">voeg nieuwe locatie toe</a>
<table>
<?php foreach ($locaties as $l): ?>
<tr>
	<td class="vet onderlijn"><?php print $l['naam']?> (<?php print $l['afkorting']?>)</td>
	<td class="onderlijn"><?php print $l['straat']?> <?php print $l['nummer']?>, <?php print $l['postcode']?>, <?php print $l['gemeente']?></td>
	<td class="onderlijn">
		<a href="<?php print $GLOBALS['root']?>_inhoud/popup/locatiesWijzig.php?id=<?php print $l['id']?>" title="Wijzig locatie" class="fbox iframe">
			wijzig</a></td>
</tr>
<?php endforeach; ?>
</table>
