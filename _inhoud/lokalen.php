<?php require 'lokalenController.php'; ?>
<h1>Overzicht lokalen</h1>
<a href="<?php print $GLOBALS['root']?>_inhoud/popup/lokalenNieuw.php" class="fbox_small iframe">voeg nieuw lokaal toe</a>

<table width="100%">
<?php foreach ($lijstLokalen as $lokaal): ?>
<tr>
	<td class="onderlijn" width="75"><?php print $lokaal['nummer']; ?>: </td>
	<td class="onderlijn"><?php print $lokaal['naam']; ?></td>
	<td class="onderlijn" width="60">
		<a class="fbox_small iframe" href="<?php print $GLOBALS['root']?>_inhoud/popup/lokalenWijzig.php?id=<?php print $lokaal['id']?>" >
			wijzig</a></td>
</tr>
<?php endforeach; ?>
</table>
