<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 23-sep-2009
 */
require 'mededelingenController.php';

?>
<h1>Mededelingen <?php print $titel?></h1>

<?php if ($_SESSION['objPersoneel']->getGroep() >= 20): ?>
	<a href="_inhoud/popup/mededelingenNieuw.php?soort=<?php print $params[0]?>" class="fbox iframe">toevoegen mededelingen</a>
<?php endif;?>

<?php if (is_array($mededelingen)): ?>
<?php foreach ($mededelingen as $m): ?>
	<h4><?php TabFuncties::htmlize($m['titel']) ?> <span class="small">(<?php print $m['beginDatum']?>)</span>
		<?php if ($_SESSION['objPersoneel']->getGroep() >= 20): ?>
			<a href="_inhoud/popup/mededelingenWijzig.php?id=<?php print $m['berichtId']?>" class="fbox iframe">
				<img src="_images/potlood.png" /></a>
		<?php endif;?>
	
		</h4>
	<?php print stripslashes($m['bericht'])?>
	<br /><br />
<?php endforeach;?>
<?php endif;?>
