<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 23-sep-2009
 */

$objMededelingen = new TabMededelingen();
if (isSet($params[0])){
	$mededelingen = $objMededelingen->leesAlleBerichten($params[0]);
	$titel = TabFuncties::htmlize($objMededelingen->getSoort($params[0]), false);
}

