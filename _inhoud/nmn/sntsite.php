<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */

require 'sntsiteController.php';

?>
<h1>Administratie snt.be</h1>
<ul>
	<li><a href="_inhoud/popup/siteEdit.php" class="fbox_wide iframe" 
		title="Wijzigen snt.be">wijzigen inhoud snt.be</a></li>
	<li><a href="_inhoud/popup/kalenderNieuw.php" class="fbox iframe" 
		title="nieuwsflits snt.be">nieuwsflits toevoegen</a></li>
	<li><a href="_inhoud/popup/cursusnieuwsEdit.php" class="fbox iframe" 
		title="nieuwsflits snt.be">cursusnieuws toevoegen</a></li>
	<li><a href="_inhoud/popup/nieuwsbrief.php" class="fbox iframe" 
		title="nieuwsbrief snt.be">nieuwsbrief versturen</a></li>
</ul>
<?php if (isSet($kal)):?>
<h2>huidige kalender</h2>
<table>
<?php foreach ($kal as $k):?>
	<tr>
		<td class="vet"><?php print stripslashes($k['titel'])?></td>
		<td align="right">
			<?php print $k['beginDatum']?>
			<?php if ($k['beginTijd'] != "00:00"):?>
			(<?php print $k['beginTijd']?>)
			<?php endif ?>
			<?php if ($k['eindDatum'] != "00/00"):?>
			- 
			<?php print($k['eindDatum'])?>
			<?php if ($k['eindTijd'] != "00:00" && $k['eindTijd'] != "" ):?>
			(<?php print $k['eindTijd']?>)
			<?php endif;?>
			<?php endif;?>
		</td>
	</tr>
	<tr>
		<td><?php print $k['inhoud']?></td>
		<td align="right">
			<a href="<?php print $GLOBALS['root']?>_inhoud/popup/kalenderWijzig.php?id=<?php print $k['id']?>" class="fbox iframe" 
		title="Kalender snt.be">wijzig</a>
	</td>
	</tr>
	<tr>
		<td class="onderlijn" colspan="2"></td>
	</tr>
<?php endforeach?>
</table>
<?php endif?>
<br /><br /><br />
<?php if (isset($cursusnieuws)):?>
<h1>huidig cursusnieuws</h1>
<table width="100%">
<?php foreach ($cursusnieuws as $k): ?>
	<tr>
		<td class="vet"><?php print stripslashes($k['titel'])?></td>
		<td align="right">
			gepubliceerd van - tot <br />
			<?php print $k['beginPubDatum']?> - 
			<?php print ($k['eindPubDatum'] != '00/00/0000') ? 
				$k['eindPubDatum'] : "&infin;" ?>
		</td>
	</tr>
	<tr>
		<td><?php print $k['inhoud']?></td>
		<td align="right">
			<a href="_inhoud/popup/cursusnieuwsEdit.php?id=<?php print $k['id']?>" 
				class="fbox iframe" title="Cursusnieuws snt.be">wijzig</a>
	</td>
	</tr>
	<tr>
		<td class="onderlijn" colspan="2"></td>
	</tr>
<?php endforeach?>
</table>
<?php endif?>