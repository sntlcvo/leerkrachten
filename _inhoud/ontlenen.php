<?php require 'ontlenenController.php'; ?>
<h1>Aanvraag tot uitlenen van materiaal</h1>
<form action="index.php?actie=submenu__inhoud&value=4__ontlenen" method="post">
<table width="90%">
<tr>
	<td colspan="2" class="fout"><?php print $melding; ?></td>
</tr>
<tr>
	<td width="100">lokaal:</td>
	<td>
		<select name="lokaal"><?php print $lokalenOptions; ?></select>
	</td>
</tr>
<tr>
	<td>wat:</td>
	<td>
		<input type="text" name="wat" maxlength="150" size="20">
	</td>
</tr>
<tr>
	<td>nodig vanaf:</td>
	<td>
		<input type="text" name="datum" class="datepicker" value="<?php print $dag ?> / <?php print $maand ?> / <?php print $jaar ?>" />
	</td>
</tr>
<tr>
	<td></td>
	<td>
		<input type="submit" name="submit" value="aanvraag tot ontlenen">
	</td>
</tr>
</table>
</form>
<!-- p>klik op de naam van de tabel om op te sorteren</p-->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th width="150">
			<a href="index.php?actie=submenu__inhoud&value=4__ontlenen&sort=leerkracht">leerkracht</a>
		</th>
		<th width="90">
			<a href="index.php?actie=submenu__inhoud&value=4__ontlenen&sort=lokaal">lokaal</a>
		</th>
		<th width="300">
			<a href="index.php?actie=submenu__inhoud&value=4__ontlenen&sort=wat">wat</a>
		</th>
		<th width="100">
			<a href="index.php?actie=submenu__inhoud&value=4__ontlenen&sort=datum_uit">datum uit</a>
		</th>
		<th width="50">
			<a href="index.php?actie=submenu__inhoud&value=4__ontlenen&sort=mag_ontleend"></a>
		</th>
		<th width="100">
			<a href="index.php?actie=submenu__inhoud&value=4__ontlenen&sort=datum_terug">datum in</a>
		</th>
		<th>
			
		</th>
	</tr>
	<?php if (is_array($ontleningen)): ?>
	<?php foreach ($ontleningen as $ontlening): ?>
	<tr>
		<td class="onderlijn"><?php print $ontlening['leerkracht']; ?></td>
		<td class="onderlijn"><?php print $ontlening['lokaal']; ?></td>
		<td class="onderlijn"><?php print $ontlening['wat']; ?></td>
		<td class="onderlijn"><?php print $ontlening['datum_uit'] ?></td>
		<td class="onderlijn">
			<img src="_images/thumbs_<?php print $ontlening['bevestiging']; ?>.png"	height="19" width="19"></td>
		<td class="onderlijn"><?php print ($ontlening['date_in'] != 0) ? $ontlening['datum_in'] : "nog niet" ?></td>
		<td class="onderlijn">
			<?php if ($_SESSION['objPersoneel']->getGroep() >= 30): ?>
				<a href="_inhoud/popup/ontlenenEdit.php?id=<?php print $ontlening['id']?>" class="fbox_small iframe"><img src="_images/potlood.png" ></a>
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; ?>
	<?php endif; ?>
</table>
<script type="text/javascript">
$('.datepicker').datepicker({
	dateFormat: 'dd/mm/yy',
	changeMonth: true,
	changeYear: true,
	defaultDate: +7
});
</script>