<?php

$objLokalen = new TabLokaal();

$lokalenOptions = TabFuncties::createOptionList($objLokalen->getLokalen(null, true), -99, "--");

$week = mktime (0, 0, 0, date('m'), date('d')+7, date('Y'));
$dag = date('d', $week);
$maand = date('m', $week);
$jaar = date('Y', $week);

$objOntleningen = new TabOntlening();


if (isSet($_POST['submit'])){
	if ($_POST['wat'] == "") {
		$melding = "gelieve iets in te vullen";
	} else {
		$objOntleningen->setOntleningen($_SESSION['objPersoneel']->getId(), $_POST['lokaal'], $_POST['wat'], 'niet_ok', $_POST['datum'], 0);
		$melding = "je aanvraag werd genoteerd, je wordt op de hoogte gehouden via de website";
	}
}

$ontleningen = TabFuncties::htmlize($objOntleningen->getOntleningen(), false);
?>