<?php
require 'personeelGegevensController.php';
?>
<h1>Persoonlijke gegevens leerkrachten - wijzigen</h1>
<?php if ($_SESSION['objPersoneel']->getGroep() >= 40):?>
<div><a href="<?php print $GLOBALS['root']?>/_inhoud/popup/personeelGegevensNieuw.php" class="fbox_small iframe" >nieuwe toevoegen</a></div>
<?php endif;?>
<table>
	<?php if (is_array($pers)): ?>
	<?php foreach($pers as $p):?>
	<tr>
		<td width="230" class="onderlijn <?php print ($p['actief'] == 'nee') ? "lichter" : "" ?>"><?php print $p['voll_naam']?></td>
		<td class="onderlijn <?php print ($p['actief'] == 'nee') ? "lichter" : "" ?>"><?php print $p['email']?></td>
		<td class="onderlijn <?php print ($p['actief'] == 'nee') ? "lichter" : "" ?>"><?php print $p['telefoon']?>
			<?php if ($p['telefoon'] != "" && $p['gsm'] != ""): ?>
			  <br />
			<?php endif;?> 
			<?php print $p['gsm']?></td>
		<td class="onderlijn <?php print ($p['actief'] == 'nee') ? "lichter" : "" ?>">
			<a href="<?php print $GLOBALS['root']?>/_inhoud/popup/personeelGegevensWijzig.php?id=<?php print $p['id']?>" class="fbox_small iframe">wijzig</a></td>
	</tr>
	<?php endforeach;?>
	<?php endif;?>
</table>
