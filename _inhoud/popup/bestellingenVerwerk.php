<?php
/**
 * @package lknet2010 
 * @author tim
 * @version 2009.1 - 25-nov-2009
 */

require 'init.php';
//print_r($_POST);
//print_r($_SESSION);
$objB = new TabBestellingen();
switch($_POST['submit']){
	case "bestelling plaatsen":
		$info = ($_POST['info'] != "geef hier uw verdere omschrijving") ? $_POST['info'] : "";
		$objB->addBestelling($_SESSION['objPersoneel']->getId(), $_POST['wat'], $info);
		break;
	case "aanpassen":
		if (isSet($_POST['verwijderen'])){
			$objB->verwijderen($_POST['bestelId']);
		} else {
			$objB->editBestelling($_POST['bestelId'], $_POST['dag'], $_POST['maand'], $_POST['jaar'], $_POST['firma'], $_POST['geleverd']);
		}
}
?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?> 