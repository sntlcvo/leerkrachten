<?php
require 'bestellingenWijzigController.php';
?>
<?php require 'html_head.php'?>

<h1>Overzicht bestelling</h1>
<form method="post" action="bestellingenVerwerk.php">

<table>
	<tr>
		<td width="130">aangevraagd door</td>
		<td><?php print $bestelling['leerkracht']; ?></td>
	</tr>
	<tr>
		<td valign="top">op</td>
		<td><?php TabFuncties::printdatum($bestelling['datum']); ?></td>
	</tr>
	<tr>
		<td valign="top">omschrijving</td>
		<td><?php print $bestelling['wat'] ?><br />
			<?php print $bestelling['info'] ?></td>
	</tr>
	<tr>
		<td valign="top">besteld</td>
		<td valign="top">
			<input type="text" name="dag" value="<?php print date("d"); ?>" maxlength="2" size="2"> /
			<input type="text" name="maand" value="<?php print date("m"); ?>" maxlength="2" size="2"> /
			<input type="text" name="jaar" value="<?php print date("Y"); ?>" maxlength="4" size="4">  
		</td>
	</tr>
	<tr>
		<td valign="top">bij</td>
		<td valign="top"><select name="firma"><?php print $leveranciersLijst; ?></select></td>
	</tr>
	<tr>
		<td valign="top">geleverd</td>
		<td valign="top"><select name="geleverd"><?php print $jaNeeLijst; ?></select></td>
	</tr>
	<tr>
		<td valign="top">verwijderen?</td>
		<td><input type="checkbox" name="verwijderen" onclick="bevestig(this)"></td>
	</tr>
	
	<tr>
		<td colspan="2" align="center">
			<input type="hidden" name="bestelId" value="<?php print $bestelling['bestelId']; ?>">
			<input type="submit" name="submit" value="aanpassen">
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'; ?>