<?php
require 'init.php';
$objB = new TabBestellingen();
$objL = new TabStock();

$bestelling = $objB->getBestellingen($_GET['id']);
$leveranciersLijst = TabFuncties::createOptionList($objL->getLeveranciers(null, true), $bestelling['firmaId'], "maak uw keuze");
$jaNeeLijst = TabFuncties::createOptionListJaNee($bestelling['geleverd']);

$bestelling = TabFuncties::htmlize($bestelling, false);


?>