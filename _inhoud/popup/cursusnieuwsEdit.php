<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */
require 'cursusnieuwsEditController.php';
?>
<?php require 'html_head.php'?>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true,
		});
	});
</script>


<h1>Cursusnieuws</h1>
<h2><?php Tab_Functies::viewEscape($action)?></h2>
<form method="post" action="cursusnieuwsVerwerk.php">
<table>
	<tr>
		<td width="120">titel</td>
		<td>
			<input type="text" name="titel" value="<?php Tab_Functies::viewEscape($cn['titel'])?>" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="ckeditor" id="ckeditor"><?php print $cn['inhoud']?></textarea>
		</td>
	</tr>
	<tr>
		<td>te verschijnen vanaf: </td>
		<td>
			<input type="text" name="begin" class="datepicker" size="10" maxlength="10"
				value="<?php print $cn['beginPubDatum'] ?>" />
		</td>
	</tr>
	<tr>
		<td>te verschijnen tot: </td>
		<td>
			<input type="text" name="eind" class="datepicker" size="10" maxlength="10"
				value="<?php print $cn['eindPubDatum']?>" />	
		</td>
	</tr>
	<?php if ($action == 'wijzig'):?>
	<tr>
		<td>wis cursusnieuws?</td>
		<td><input type="checkbox" name="wissen" onclick="bevestig(this);" /></td>
	</tr>
	<?php endif; ?>
	<tr>
		<td></td>
		<td>
			<?php if ($action == 'wijzig'):?>
			<input type="hidden" name="id" value="<?php print $cn['id']?>" />
			<?php endif;?>
			<input type="submit" name="submit" value="<?php print $action?>" />
		</td>
	</tr>
</table>

</form>
<?php require 'html_foot.php'?>