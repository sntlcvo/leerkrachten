<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */

require 'init.php';
if (isSet($_GET['id'])){
	$cn = Tab_KalenderCursusnieuws::getNieuws($_GET['id']);
	$action = "wijzig";
	$cn['eindPubDatum'] = ($cn['eindPubDatum'] == '0000-00-00') ? '' : $cn['eindPubDatum'];
} else {
	$cn = array(
		'titel' => '',
		'inhoud' => '',
		'beginPubDatum' => date('d/m/Y'),
		'eindPubDatum' => '',
	);
	$action = "voeg toe";
}
