<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */

require 'init.php';
//print_r($_POST);
//exit();
$objK = new TabKalender();
$actie = isSet($_POST['wissen']) ? "wissen" : $_POST['submit'];

switch($actie){
	case "wijzig":
		Tab_KalenderCursusnieuws::editNieuws($_POST['id'], $_POST['titel'], $_POST['ckeditor'], $_POST['begin'], $_POST['eind']);
		break;
	case "voeg toe":
		Tab_KalenderCursusnieuws::addNieuws($_POST['titel'], $_POST['ckeditor'], $_POST['begin'], $_POST['eind']);
		break;
	case "wissen":
		Tab_KalenderCursusnieuws::deleteNieuws($_POST['id']);
		break;
	default:
		print "ongeldige keuze";
		exit();
}

?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>