<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 4-dec-2009
 */
require 'documentenNieuwController.php';
?>
<?php require 'html_head.php'?>
<h1>Nieuw document aanmaken</h1>
<form method="post" action="documentenVerwerk.php">
<table>
	<tr>
		<td>titel</td>
		<td><input type="text" name="titel" /></td>
	</tr>
	<tr>
		<td>locatie</td>
		<td><select name="locatie"><?php print $lijst?></select></td>
	</tr>
	<tr>
		<td></td>
		<td><textarea name="bericht" id="ckeditor"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="hidden" name="hoofdmap" value="<?php print $hoofd?>" />
			<input type="submit" name="submit" value="maak document" />
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>