<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 1-dec-2009
 */
require 'documentenUploadController.php';
?>
<?php require 'html_head.php'?>
<h1>Nieuw document toevoegen</h1>
<p>let op: geen speciale tekens in de naam van je document gebruiken</p>
<?php if (isSet($foutMelding)): ?>
<span class="fout">
<?php print $foutMelding?>
</span>
<?php endif;?>
<form method="post" action="documentenVerwerk.php?hoofdmap=<?php print $hoofd?>" enctype="multipart/form-data">
<table>
	<tr>
		<td>document</td>
		<td><input type="file" name="document" /></td>
	</tr>
	<tr>
		<td>locatie</td>
		<td><select name="locatie"><?php print $lijst?></select></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="hidden" name="hoofdmap" value="<?php print $hoofd?>" />
			<input type="submit" name="submit" value="upload" />
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>