<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 1-dec-2009
 */
require 'init.php';

$objFS = new TabFileSystem();

//print_r($_GET);
//2__documenten_-_lknet_-_documenten_downloaden
//$params = $_GET['p'];

$params = explode("__", $_GET['p']);
$tmp = array_shift($params);
$tmp2 = explode('_-_', $params[0]);
$doc = array_shift($tmp2);
$sub = implode('/', $tmp2);
if (isset($_GET['hoofdmap'])){
	$hoofd = $_GET['hoofdmap'];
} else {
	//$hoofd = "../../".$GLOBALS['bestanden']."/$sub";
	$hoofd = $GLOBALS['DOC_ROOT'] . '/docs/' . $sub;
}

$mappen = $objFS->getFullDirNames($objFS->readDir($hoofd, true));

if ($_GET['allowRoot'] == "true"){
	$r = array('/' => '/');
	$mappen = array_merge($r, $mappen);
}

if (count($mappen) == 1){
	$lijst = TabFuncties::createOptionList($mappen);
} else {
	$lijst = TabFuncties::createOptionList($mappen, -99, "maak uw keuze");
}

if (isSet($_SESSION['error'])){
	$foutMelding = implode("<br />\n", $_SESSION['error']);
	unset($_SESSION['error']);
}