<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 1-dec-2009
 */
require 'init.php';

if (count($_POST) == 0){
	$_SESSION['error']['fileuploadPost'] = "er werden geen gegevens doorgestuurd, gelieve opnieuw te proberen, of ftp te gebruiken";
}

$actie = (isSet($_POST['wissen'])) ? 'wissen' : $_POST['submit'];
$objFS = new TabFileSystem();

switch ($actie){
	case "upload":
		$pad = $_POST['hoofdmap'] . $_POST['locatie'];
		if (count($_FILES) == 0){
			$_SESSION['error']['fileuploadFiles'] = "er werd geen bestand doorgestuurd, gelieve opnieuw te proberen, of ftp te gebruiken";
		}
		$doel = "$pad/".$_FILES['document']['name'];
		$bron = $_FILES['document']['tmp_name'];
		$returnError = null;
		if (!$objFS->copyBestand($bron, $doel, $_FILES['document']['error'], $returnError)){
			$_SESSION['error']['fileuploadCopy'] = $returnError;
		}
		break;
	case "maak document":
		$pad = $_POST['hoofdmap'] . $_POST['locatie'];
		$doel = $_POST['titel'] .".html";
		if (isSet($_POST['wissen'])){
			$objFS->wisBestand($pad, $doel);
		} else {
			$objFS->createFile($pad, $doel, $_POST['bericht']);
		}
		break;
	case "wissen":
		if (isSet($_POST['wissen'])){
			$pad = $_POST['hoofdmap'];
			$doel = ($_POST['type'] == 'html') ? $_POST['titel'] .".html" : $_POST['titel']; 
			$objFS->wisBestand($_POST['hoofdmap'], $doel);
		}
}

if (isSet($_SESSION['error'])){
	header('location: documentenUpload.php?hoofdmap='.$_GET['hoofdmap']);
	exit();
}

?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>