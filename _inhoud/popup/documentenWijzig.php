<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 4-dec-2009
 */
require 'documentenWijzigController.php';
?>
<?php require 'html_head.php'?>
<?php if ($toonEditor):?>
<h1>Wijzigen van bestand</h1>
<form method="post" action="documentenVerwerk.php">
<table>
	<tr>
		<td>titel</td>
		<td><input type="text" name="titel" value="<?php print $titel ?>" /></td>
	</tr>
	<!-- tr>
		<td>locatie</td>
		<td><select name="locatie"><?php print $lijst?></select></td>
	</tr-->
	<tr>
		<td></td>
		<td><textarea name="bericht" id="ckeditor"><?php print $inhoud?></textarea></td>
	</tr>
	<tr>
		<td width="50">bestand wissen</td>
		<td><input type="checkbox" name="wissen" onclick="bevestig(this)" /></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="hidden" name="hoofdmap" value="<?php print $hoofd?>" />
			<input type="hidden" name="type" value="html" />
			<input type="submit" name="submit" value="wijzig document" />
		</td>
	</tr>
</table>
</form>
<?php else:	//editor niet tonen, bestand wissen?>
<h1>Wissen van bestand</h1>
<form method="post" action="documentenVerwerk.php">
	<table>
		<tr>
			<td colspan="2">document <span class="vet"><?php print "$titel.$extensie"?></span> wissen?</td>
		</tr>
		<tr>
			<td width="50"></td>
			<td><input type="checkbox" name="wissen" onclick="bevestig(this)" /></td>
		</tr>
		<tr>
			<td width="50"></td>
			<td>
				<input type="hidden" name="titel" value="<?php print "$titel.$extensie" ?>" />
				<input type="hidden" name="hoofdmap" value="<?php print $hoofd?>" />
				<input type="hidden" name="type" value="andere" />
				<input type="submit" name="submit" value="wissen" />
			</td>
		</tr>
		
	</table>
	
</form>

<?php endif;?>

<?php require 'html_foot.php'?>