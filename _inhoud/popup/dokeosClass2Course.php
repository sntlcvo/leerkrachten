<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 15-dec-2009
 */
require 'dokeosClass2CourseController.php';
?>
<?php require 'html_head.php'?>
<h1>Klassen aan curssussen toevoegen</h1>
<p>Voeg klassen aan cursussen toe aan Dokeos via een csv-bestand</p>
<p>Dit bestand moet volgende vorm hebben: <br />
	&nbsp;&nbsp;&nbsp;<em>klas;cursus</em><br />
Zowel klas als cursus moeten al bestaan.</p>

<?php if (isSet($fout)): ?>
<div id="fout" class="fout">
	<?php print $fout?>
</div>
<?php endif; ?>
<form method="post" enctype="multipart/form-data" action="<?php print $_SERVER['PHP_SELF'] ?>">
	geef het csv-bestand op om klassen aan cursussen toe te voegen. 
	<input type="file" name="csvbestand">
	<input type="submit" name="submit" value="ok">
</form>
<?php if (isSet($codesNietOk)):?>
volgende klassen werden niet toegevoegd, mogelijke problemen (klascode bestaat niet, dokeoscursus bestaat niet)
<ul>
<?php foreach($codesNietOk as $klascode => $dokeosCursus): ?>
	<li><?php print $klascode ?>: <?php print $dokeosCursus?></li>
<?php endforeach;?>
</ul>
<?php endif;?>

<?php if (isSet($codesOk)):?>
volgende klassen werden aan de cursus toegevoegd
<ul>
<?php foreach($codesOk as $klascode => $dokeosCursus): ?>
	<li><?php print $klascode ?>: <?php print $dokeosCursus?></li>
<?php endforeach;?>
</ul>
<?php endif;?>

<?php require 'html_foot.php'?>
