<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 15-dec-2009
 */
require 'init.php';
$splitsing = ";";
$objD = new TabDokeosCourses();
//csv moet opgebouwd zijn als klascode;dokeoscode;


$deze_pagina = $_SERVER['PHP_SELF'];
if (isSet($_POST['submit'])){
	if (isSet($_FILES['csvbestand']['name'])){
		//copy naar vaste plaats
		$tijd_naam = $_FILES['csvbestand']['tmp_name'];
		$bestandsnaam = "tmp/" . $_FILES['csvbestand']['name'];
		if (!copy ($tijd_naam, $bestandsnaam)){
			$fout = "kopieren niet gelukt";
		} else {
			$regels = file($bestandsnaam);
			foreach($regels as $regel){
				$regel = trim($regel);
				$gegevens = explode($splitsing, $regel);
				$klasCode = $gegevens[0];
				$dokeosCursus = $gegevens[1];
				if ($objD->addClass2Course($dokeosCursus, $klasCode)){
					$codesOk[$klasCode] = $dokeosCursus;
				} else {
					$codesNietOk[$klasCode] = $dokeosCursus;
				}
			}
		}
	}
}
