<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require 'dokeosClasscodeGenerator2Controller.php';

?>
<?php require 'html_head.php'?>
<h1>Klascodes beginnend met de letter '<?php print $currLetter; ?>'</h1>
<form method="post" action="dokeosClasscodeGeneratorVerwerk.php">
<?php if (!is_array($klascodes)): ?>
er zijn geen klascodes die beginnen met '<?php print $currLetter?>'
<?php else: ?>

<ul>
<?php foreach ($klascodes as $kg): ?>
<li>
	<?php print $kg['cursusnaam']?> - <?php print $kg['klascode']?>
	<select name="klascodes[<?php print $kg['klascode']?>]">
		<?php print TabFuncties::createOptionList($dokeosCourseCodes, $objLR->haalKlasDokeosCode($kg['klascode']))?>
	</select>
</li>
<?php endforeach;?>
</ul>
<?php endif?>
<br />
<input type="submit" name="submit" value="verdergaan">
</form>

<p><a href="dokeosClasscodeGenerator.php">terug naar het begin</a></p>
<?php require 'html_foot.php'?>