<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require 'init.php';
$objDC = new TabDokeosCourses();
$dokeosCourseCodes = $objDC->getCourseCodes(true);
$dokeosCourseCodes[-99] = "--maak uw keuze--";
$dokeosCourseCodes[0] = "--niet van toepassing--";
$dokeosCourseCodes['ZZZZ'] = "--terug verwijderen--";
asort($dokeosCourseCodes);

$objLR = new TabLessenrooster();


//haal alle klascodes op uit de leerkrachtensite ->ok
//haal alle codes op uit dokeos ->ok
//genereer een lijst waarbij de klascode wordt weergeven ->ok
//en een selectielijst met alle dokeoscodes -> ok
//resultaat wegschrijven naar db
//achteraf mogelijk om te exporteren naar csv

if ($_POST['submit'] == "periode kiezen"){
	$_SESSION['periodeId'] = $_POST['periodeId'];
	$_SESSION['currLetter'] = $_POST['letter'];
	//1 letter terugkeren
} elseif (!isSet($_SESSION['periodeId'])){
	header("location: dokeosClasscodeGenerator.php");
	exit();
}

if (isSet($_SESSION['currLetter'])){
	//de eerste letter bepalen,
	//als deze niet is ingesteld wordt er begonnen met de a
	$prevLetter = $_SESSION['currLetter'];
	$intLetter = ord($prevLetter);
	if ($intLetter >= 97 && $intLetter <= 122){
		$_SESSION['currLetter'] = $currLetter = chr($intLetter+1);
	} else {
		$_SESSION['currLetter'] = $currLetter = 'a';
	}
} else {
	$_SESSION['currLetter'] = $currLetter = 'a';
}

$klascodes = $objLR->getAlleKlascodes($_SESSION['periodeId'], $currLetter);

