<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 16 dec 2009
 */

require 'init.php';

//eerst nog id van periode ophalen -> OK
//haal alle klascodes op uit de leerkrachtensite
//haal alle codes op uit dokeos
//genereer een lijst waarbij de klascode wordt weergeven
//en een selectielijst met alle dokeoscodes
//resultaat wegschrijven naar db
//achteraf mogelijk om te exporteren naar csv

//stap 1
//lijst ophalen voor de periodes
unset($_SESSION['periodeId']);
unset($_SESSION['currLetter']); 
$objLR = new TabLessenrooster();
$periodes = TabFuncties::createOptionList($objLR->getPeriodes(true), -99, "--kies semester--");
for ($i = 97; $i <= 122; $i++){
	$letter = chr($i);
	//de effectieve waarde moet eentje lager zijn dan waar we willen beginnen
	//in de volgende pagina wordt er eentje bijgeteld
	$prevLetter = chr($i-1);
	$lijstLetters .= "<option value=\"$prevLetter\">$letter</option>";
}
