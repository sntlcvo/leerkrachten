<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require 'init.php';
$csvbestand = "../../tmp/lijst.csv";

//opbouwen csv, en een link maken om het bestand te downloaden
//csv is opgebouwd als klascode;dokeoscode;

$objLR = new TabLessenrooster();
$lijst = $objLR->haalKlasDokeosCode();

foreach($lijst as $klassen){
	$klasCode = $klassen['klascode'];
	$dokeosCode = $klassen['dokeoscode'];
	$regels[] = "$klasCode;$dokeosCode\n";
}

file_put_contents($csvbestand, $regels);
