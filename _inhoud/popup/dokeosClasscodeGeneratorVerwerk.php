<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require 'init.php';
$objLR = new TabLessenrooster();

//resultaat wegschrijven naar db
//achteraf mogelijk om te exporteren naar csv
if (is_array($_POST['klascodes'])){
	foreach($_POST['klascodes'] as $klasCode => $dokeosCode){
		if ($dokeosCode != '0' && $dokeosCode != '-99'){
			if ($dokeosCode == 'ZZZZ'){
				$objLR->deleteKlasDokeosCode($klasCode);
			} else {
				$objLR->schrijfKlasDokeosCode($klasCode, $dokeosCode);
			}
		}
	}
}

if ($_SESSION['currLetter'] == "z"){
	header("location: dokeosClasscodeGeneratorEind.php");
} else {
	header("location: dokeosClasscodeGenerator2.php");
}
