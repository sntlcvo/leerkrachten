<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'header_php.inc.php';
$objDokeos = new dokeosCourses();
$klasCodes = $objDokeos->getAllClassCodes();
/*    [2] => Array
        (
            [classId] => 759
            [classCode] => 2BCN INI A13
        )
*/
foreach ($klasCodes as $code){
	$regels[] = $code['classCode'] . "\n";
}
$bestand = ("tmp/klascodes.csv");

file_put_contents($bestand, $regels);

print $bestand;
?>