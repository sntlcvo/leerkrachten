function haalCsv(){
    new Ajax.Request('dokeosExportCourseCodesController.php', {
        method: 'get',
        onSuccess: function(transport){
            var response = transport.responseText || "no response text";
            toonCsv(response);
        },
		onLoad: function(){
			toonSpinner();
		},
        onFailure: function(){
            alert('Something went wrong...')
        }
    });
}

function toonCsv(response){
    veld = $('csvLink');
	tekst = '<a href="' + response + '">klik hier om te downloaden</a>';
    veld.update(tekst);
}

function toonSpinner(){
	veld = $('csvLink');
	tekst = '<img src="afbeeldingen/spinner.gif" border="0">';
	veld.update(tekst);
}
