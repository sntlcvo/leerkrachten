<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'header_php.inc.php';
$objDokeos = new dokeosCourses();
$courseCodes = $objDokeos->getCourseCodes();

foreach ($courseCodes as $code){
	$regels[] = $code['code'] . "\n";
}
$bestand = ("tmp/cursuscodes.csv");

file_put_contents($bestand, $regels);

print $bestand;
?>