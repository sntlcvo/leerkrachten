<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'dokeosUploadUsersController.php';
?>
<?php require 'html_head.php'?>
<h1>Gebruikers aan Dokeos toevoegen</h1>

<form method="post" action="dokeosUploadUsers.php" enctype="multipart/form-data">
<p>Geef de locatie van het csv-bestand op: <input name="csv" type="file"></p>
<p><input type="submit" name="submit" value="verzenden"></p>
</form>
<p>het csv-bestand moet als volgt opgebouwd zijn: </p>
<p class="vet">lastname;firstname;username;password;email;official_code;phone</p>
<p>cursistennummer is de loginnaam die gebruikt wordt (sntxxxxxxx)</p>

<div id="lijst">
<?php if (is_array($bestaande)): ?>
volgende gebruikers bestonden al in het systeem: 
<ul>
<?php foreach ($bestaande as $naam): ?>
	<li><?php print $naam; ?>
<?php endforeach; ?>
</ul>
<?php endif; ?>
</div>

<div id="lijst2">
<?php if (is_array($toegevoegd)): ?>
er zijn <?php print $aantal; ?> gebruikers toegevoegd
<ul>
<?php foreach ($toegevoegd as $naam): ?>
	<li><?php print $naam; ?>
<?php endforeach; ?>
</ul>
<?php endif; ?>
</div>
<?php require 'html_foot.php'?>