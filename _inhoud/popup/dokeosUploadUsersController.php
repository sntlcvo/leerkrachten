<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'init.php';

if (isSet($_POST['submit'])){
	$objDokeos = new TabDokeosCourses();
	//bestand kopieren
	$bestand = dirname(__FILE__). "/../../tmp/" . $_FILES['csv']['name'];
	copy($_FILES['csv']['tmp_name'], $bestand);
	//bestand inlezen
	$inhoud = file($bestand);
	$aantal = 0;
	foreach ($inhoud as $regel){
		$gegevens = explode(";", $regel);
		foreach ($gegevens as $i => $v){
			switch($i){
				case 0: $index = "lastname"; break;
				case 1: $index = "firstname"; break;
				case 2: $index = "username"; break;
				case 3: $index = "password"; break;
				case 4: $index = "email"; break;
				case 5: $index = "official_code"; break;
				default: $index = "nvt"; break;
			}
			$data[$index] = trim($v);
		}
		//regel per regel controleren of een gebruiker al in de db voorkomt
		$userId = $objDokeos->findUserIdByUserName($data['username']);
		if ($userId != ""){
			//gebruiker bestaat -> niet toevoegen
			$bestaande[] = $data['username'];
		} else {
			//gebruiker bestaat -> toevoegen
			$objDokeos->addUsers2Dokeos($data);
			$toegevoegd[] = $data['username'];
			$aantal ++;
		}
		if (is_array($bestaande)) TabFuncties::htmlize($bestaande, false);
		if (is_array($toegevoegd)) TabFuncties::htmlize($toegevoegd, false);
	}
	//bestand verwijderen
	unlink($bestand);
}
?>

