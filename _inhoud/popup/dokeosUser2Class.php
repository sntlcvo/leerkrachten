<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'dokeosUser2ClassController.php';
?>
<h3>Gebruikers aan klassen toevoegen via CSV</h3>
<form action="index.php?pag=dokeosUser2Class" method="post"
	enctype="multipart/form-data">Locatie van het XML- of CSV-bestand: <input
	name="csv" type="file" /> <input name="submit" value="OK" type="submit" /></form>
<p>Het CSV-bestand moet er als volgt uitzien (Velden in <b>vet</b> zijn
verplicht.) :</p>
<div>
<b>UserName</b>;<b>ClassCode</b><br />
jdoe;class01<br />
a.dam;class01<br />
</div>

<div id="onbestaandeGebruikers">
<?php if (is_array($onbestaandeGebruikers)): ?>
<p class="vet">volgende gebruikers werden niet gevonden:</p>
<ul>
<?php foreach ($onbestaandeGebruikers as $gebruiker): ?>
	<li><?php print $gebruiker; ?></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
</div>

<div id="onbestaandeKlassen">
<?php if (is_array($onbestaandeKlassen)): ?>
<p class="vet">volgende klascodes werden niet gevonden:</p>
<ul>
<?php foreach ($onbestaandeKlassen as $klas): ?>
	<li><?php print $klas; ?></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
</div>

<div id="gebruikerAlInKlas">
<?php if (is_array($gebruikerAlInKlas)): ?>
<p class="vet">volgende gebruikers zaten al in de juiste klas:</p>
<ul>
<?php foreach ($gebruikerAlInKlas as $gebruiker): ?>
	<li><?php print $gebruiker; ?></li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
</div>

<div id="aantal">
<?php if (isSet($aantal)): ?> er werden <?php print $aantal ?> gegevens toegevoegd / gewijzigd
<?php endif; ?>
</div>

<div id="toegevoegdeGebruikers">
<?php if (is_array($toegevoegdeGebruikers)): ?>
<p class="vet">volgende gebruikers werden toegevoegd:</p>
<ul>
<?php foreach ($toegevoegdeGebruikers as $gebruiker): ?>
	<li><?php print $gebruiker; ?></li>
	<?php endforeach; ?>
</ul>
	<?php endif; ?>
</div>