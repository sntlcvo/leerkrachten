<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'header_php.inc.php';
if (isSet($_POST['submit'])){
	$objDokeos = new dokeosCourses();
	//kopieren bestand
	$bestand = "tmp/" . $_FILES['csv']['name'];
	copy($_FILES['csv']['tmp_name'], $bestand);
	//uitlezen
	$inhoud = file($bestand);
	$aantal = 0;
	$onbestaandeKlassen = array();
	$onbestaandeGebruikers = array();
	foreach ($inhoud as $regel){
		$regel = trim($regel);
		$gegevens = explode(";", $regel);
		//0 is username
		//1 is classcode
		$classId = $objDokeos->getClassId($gegevens[1]);
		if ($classId != ""){
			$userId = $objDokeos->findUserIdByUserName($gegevens[0]);
			if ($userId != ""){
				//gebruiker toevoegen aan klasse
				if ($objDokeos->addUser2Class($userId, $classId)){
					//gebruiker is toegevoegd;
					$toegevoegdeGebruikers[] = $gegevens[0] . " (" . $gegevens[1] . ")";
					$aantal ++;
				} else {
					//gebruiker bestond al in klas;
					$gebruikerAlInKlas[] = $gegevens[0] . " (" . $gegevens[1] . ")";
				}
			} else {
				if (!in_array($gegevens[0], $onbestaandeGebruikers)){
					$onbestaandeGebruikers[] = $gegevens[0];
				}
			}
		} else {
			if (!in_array($gegevens[1], $onbestaandeKlassen)) {
				$onbestaandeKlassen[] = $gegevens[1];
			}
		}
	}
	//wissen bestand
	unlink($bestand);
}

?>