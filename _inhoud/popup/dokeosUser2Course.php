<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'dokeosUser2CourseController.php';
?>
<h3>Gebruikers aan cursussen toevoegen CSV</h3>
<form action="index.php?pag=dokeosUser2Course" method="post"
	enctype="multipart/form-data">Locatie van het XML- of CSV-bestand: <input
	name="csv" type="file" /> <input name="submit" value="OK" type="submit" /></form>
<p>Het CSV-bestand moet er als volgt uitzien (Velden in <b>vet</b> zijn
verplicht.) :</p>
<div><b>UserName</b>;<b>CourseCode</b>;<b>Status</b><br />
jdoe;course01;1<br />
a.dam;course01;5<br />
<br />
1: Leerkracht<br />
5: Cursist</div>

<div id="aantal"><?php if (isSet($aantal)): ?> er werden <?php print $aantal ?>
gegevens toegevoegd / gewijzigd <?php endif; ?></div>

<div id="onbestaandeGebruikers"><?php if (is_array($onbestaandeGebruikers)): ?>
<p class="vet">volgende gebruikers werden niet gevonden:</p>
<ul>
<?php foreach ($onbestaandeGebruikers as $gebruiker): ?>
	<li><?php print $gebruiker; ?></li>
	<?php endforeach; ?>
</ul>
	<?php endif; ?></div>

<div id="onbestaandeCursussen"><?php if (is_array($onbestaandeCursussen)): ?>
<p class="vet">volgende cursussen werden niet gevonden:</p>
<ul>
<?php foreach ($onbestaandeCursussen as $cursus): ?>
	<li><?php print $cursus; ?></li>
	<?php endforeach; ?>
</ul>
	<?php endif; ?></div>
