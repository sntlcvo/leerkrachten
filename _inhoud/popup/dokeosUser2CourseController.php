<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'header_php.inc.php';
if (isSet($_POST['submit'])){
	$objDokeos = new dokeosCourses();
	//kopieren bestand
	$bestand = "tmp/" . $_FILES['csv']['name'];
	copy($_FILES['csv']['tmp_name'], $bestand);
	//uitlezen
	$inhoud = file($bestand);
	$aantal = 0;
	foreach ($inhoud as $regel){
		$regel = trim($regel);
		$gegevens = explode(";", $regel);
		if ($objDokeos->courseExists($gegevens[1])){
			if ($objDokeos->addUser2Course($gegevens)){
				$aantal ++;					
			} else {
				$onbestaandeGebruikers[] = $gegevens[0];
			}
		} else {
			$onbestaandeCursussen[] = $gegevens[1];
		}
	}
	//toevoegen / update db
	//wissen bestand
	unlink($bestand);
}

?>