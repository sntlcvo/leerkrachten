<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'dokeosUserEmailController.php';

?>
<?php require 'html_head.php'?>
<h1>Emailadres van een dokeos-gebruiker zoeken</h1>
<div id="formulier">
<form action="dokeosUserEmail.php" method="post">
<p>gelieve de gebruikersnaam op te geven:<input type="text" name="username" value="snt"></p>
<p><input type="submit" value="verzenden" name="submit"></p>
</form>
</div>
<?php if (isSet($email)): ?>
<div id="melding" class="vet">het mailadres van <?php print $_POST['username'] ?> is <?php print $email?></div>
<?php endif;?>
<?php require 'html_foot.php'?>