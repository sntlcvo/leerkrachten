<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'dokeosUserEmailClassController.php';

?>
<?php require 'html_head.php'?>
<h1>Emailadres van dokeos-gebruikers zoeken per klas</h1>
<div id="formulier">
<form action="dokeosUserEmailClass.php" method="post">
<p>gelieve de klascode te selecteren:<input type="text" name="klascode" value=""></p>
<p><input type="submit" value="verzenden" name="submit"></p>
</form>
</div>
<?php if (isSet($lijst) && is_array($lijst)): ?>
<div id="melding" class="vet">
	<h2>gegevens voor <?php print $_POST['klascode'] ?></h2>
	<ul>
		<?php foreach ($lijst as $l):?>
			<li><?php print $l['lastname']?> <?php print $l['firstname']?>: <?php print $l['email']?> </li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif;?>
<?php require 'html_foot.php'?>