<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'dokeosUserPasswordController.php';

?>
<?php require 'html_head.php'?>
<h1>Wijzigen van een wachtwoord van een dokeos-user</h1>
<div id="fout" class="rode_achtergrond"><?php print $melding; ?></div>
<div id="formulier">
<form action="dokeosUserPassword.php" method="post">
<p>gelieve de gebruikersnaam op te geven:<input type="text" name="username" value="snt"><br>
gelieve een nieuw wachtwoord op te geven:<input type="text" name="password">
</p>
<p><input type="submit" value="verzenden" name="submit"></p>
</form>
</div>
<?php require 'html_foot.php'?>