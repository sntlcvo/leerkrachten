<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'init.php';
$objDokeos = new TabDokeosCourses();

if (isSet($_POST['submit'])){
	$password = $_POST['wachtwoord'];
	//opzoeken als gebruiker bestaat
	if ($objDokeos->updateUserPassword($_POST['username'], $_POST['password'])){
		$melding = "het wachtwoord werd aangepast";
	} else {
		$melding = "gebruiker werd niet gevonden";
	}
}