<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2010.1 - 11-mei-2010
 */
require 'init.php';
$csv = 'tmp/cursisten.csv';
$objD = new TabDokeosCourses();
/*Verplichte
 velden: firstname,lastname,email*/
if (isSet($_POST['klascodes'])){
	$cursisten = array();
	foreach($_POST['klascodes'] as $kc){
		$cursistenTmp = $objD->findUsers($kc, true);
		if (is_array($cursistenTmp)){
			$cursisten = array_merge($cursisten, $cursistenTmp);
		}
	}
	$regels[] = "\"firstname\",\"lastname\",\"email\"\r\n";
	foreach($cursisten as $c){
		$regels[] = "\"{$c['lastname']}\",\"{$c['firstname']}\",\"{$c['email']}\"\n";
	}
	file_put_contents($csv,$regels);
}

print "<a href=\"_inhoud/popup/$csv\">download csv</a>";