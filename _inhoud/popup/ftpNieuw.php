<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 20-nov-2009
 */
require 'init.php';
?>
<?php require 'html_head.php'?>
<h1>toevoegen nieuwe ftp-gebruiker</h1>
<form method="post" action="ftpVerwerk.php">
<table>
	<tr>
		<td>userid</td>
		<td><input type="text" name="userid" id="userid" onblur="kopiewaarde('#userid', '#userdir', true)"></td>
	</tr>
	<tr>
		<td>passwd</td>
		<td><input type="text" name="passwd"></td>
	</tr>
	<tr>
		<td>userdir</td>
		<td><input type="text" name="userdir" id="userdir" value="/home/users/"></td>
	</tr>
	<tr>
		<td width="150">beveiligen via /home/users/.htpasswd?</td>
		<td><input type="checkbox" name="beveilig" /></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" value="voeg gebruiker toe"></td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>