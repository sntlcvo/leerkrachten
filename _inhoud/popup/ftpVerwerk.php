<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 20-nov-2009
 */
require 'init.php';
$objFtp = new TabFtp();

if ($_POST['submit'] == "voeg gebruiker toe"){
	$beveilig = (isSet($_POST['beveilig'])) ? true : false;
	if ($objFtp->addUser($_POST['userid'], $_POST['passwd'], $_POST['userdir'], $beveilig) && $beveilig){
		$aanmakenHtP = true;
	}
}

?>
<?php require 'html_head.php'?>
<h1>ftp gebruiker</h1>
<p>gebruiker '<?php print $_POST['userid']?>' werd aangepast.</p>
<?php if ($aanmakenHtP): ?>
<p>het htpassword bestand moet eventueel nog aangepast worden zie 
	<a href="http://www.mijnhomepage.nl/htaccess/directory-beveiligen.php" target="_blank">hier</a></p>
<?php endif;?>
<p><a href="#" onclick="parent.location.reload();">sluit dit venster</a></p>
<?php require 'html_foot.php'?>