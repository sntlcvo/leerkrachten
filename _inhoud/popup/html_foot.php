<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 7-okt-2009
 */
?>
</div>
<script type="text/javascript">
$(document).ready(function() {
	CKEDITOR.replace('ckeditor');
});
$('.datepicker').datepicker({
	dateFormat: 'dd/mm/yy',
	changeMonth: true,
	changeYear: true
});
</script>
</body>
</html>