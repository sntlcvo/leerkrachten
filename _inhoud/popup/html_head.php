<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 7-okt-2009
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;" />
<link href="../../_css/stijl.css" rel="stylesheet" type="text/css" />
<link href="../../_css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../_js/jquery.js" charset="utf-8"></script>
<script type="text/javascript" src="../../_js/jquery-ui-1.7.2.custom.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../../_ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../_js/tab.js" charset="utf-8"></script>
</head>
<body>
<div id="inhoud_iframe">