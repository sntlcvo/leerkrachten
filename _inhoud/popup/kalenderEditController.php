<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */
require 'init.php';
if (isSet($_GET['id'])){
	$form = kalenderForm::create('edit');
	$data = Tab_Kalender::getKalenderItems($_GET['id']);
	$formData = array(
		'titel' => $data['titel'],
		'ckeditor' => $data['inhoud'],
		'datum' => $data['datumFull'],
		'beginUur' => $data['tijdUur'],
		'beginMin' => $data['tijdMin'],
		'beginPub' => $data['beginPubDatum'],
		'eindPub' => $data['eindPubDatum'],
		'id' => $_GET['id'],
	);
	$form->populate($formData);
} else {
	$form = kalenderForm::create('new');
}

if (isSet($_GET['fout'])){
	$form->populate($_SESSION['form']);
	unset($_SESSION['form']);
}