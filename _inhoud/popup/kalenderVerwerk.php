<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */

require 'init.php';
$form = kalenderForm::create();
if (!$form->isValid($_POST)){
	$_SESSION['form'] = $form->getValidValues($_POST);
	header('location: kalenderEdit.php?fout=1');
} else {
	$datum = $_POST['datum'] . ' ' . $_POST['beginUur'] . ':' . $_POST['beginMin'];
	if ($_POST['id'] == 0){
		Tab_Kalender::addKalenderItem($_POST['titel'], $_POST['ckeditor'], $datum, $_POST['beginPub'], $_POST['eindPub']);
	} elseif (isSet($_POST['wis']) && $_POST['wis'] == 1){
		Tab_Kalender::deleteKalenderItem($_POST['id']);
	} else {
		Tab_Kalender::editKalenderItem($_POST['id'], $_POST['titel'], $_POST['ckeditor'], $datum, $_POST['beginPub'], $_POST['eindPub']);
	}
}

?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>