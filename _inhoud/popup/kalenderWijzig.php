<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */
require 'kalenderWijzigController.php';
?>
<?php require 'html_head.php'?>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>


<h1>Nieuwsflits</h1>
<p>wijzigen</p>
<form method="post" action="kalenderVerwerk.php">
<table>
	<tr>
		<td width="90">titel</td>
		<td><input type="text" name="titel" value="<?php TabFuncties::htmlize($kal['titel'])?>" />
		<input type="checkbox" name="flashy_titel" <?php if ($kal['flashy'] == 'ja'): ?>checked="checked"<?php endif?> />flashy?</td>
	</tr>
	<tr>
		<td colspan="2"><textarea name="ckeditor" id="ckeditor"><?php print $kal['inhoud']?></textarea></td>
	</tr>
	<tr>
		<td>begin</td>
		<td>
			<input type="text" name="begin" class="datepicker" size="10" maxlength="10" value="<?php print $kal['beginDatumFull']?>" />
			<input type="text" name="beginUur" size="2" maxlength="2" value="<?php print $kal['beginUur']?>" />:
			<input type="text" name="beginMin" size="2" maxlength="2" value="<?php print $kal['beginMin']?>" />
		</td>
	</tr>
	<tr>
		<td>eind</td>
		<td>
			<input type="text" name="eind" class="datepicker" size="10" maxlength="10" value="<?php print ($kal['eindDatumFull'] != "00/00/0000") ? $kal['eindDatumFull'] : ""?>" />
			<input type="text" name="eindUur" size="2" maxlength="2" value="<?php print $kal['eindUur']?>" />:
			<input type="text" name="eindMin" size="2" maxlength="2" value="<?php print $kal['eindMin']?>" />
		</td>
	</tr>
	<tr>
		<td>wis activiteit?</td>
		<td><input type="checkbox" name="wissen" onclick="bevestig(this);" /></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="hidden" name="id" value="<?php print $kal['id']?>" />
			<input type="submit" name="submit" value="wijzig" />
		</td>
	</tr>
</table>

</form>
<?php require 'html_foot.php'?>