<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'init.php';

?>
<?php require 'html_head.php'?>
<h1>Nieuwe leverancier toevoegen</h1>
<form action="leveranciersVerwerk.php" method="post" id="formulier">
<table>
	<tr>
		<td>firma</td>
		<td><input type="text" maxlength="150" size="20" name="firma"></td>
	</tr>
	<tr>
		<td>contactpersoon</td>
		<td><input type="text" maxlength="150" size="20" name="contact"></td>
	</tr>
	<tr>
		<td>technisch contact</td>
		<td><input type="text" maxlength="150" size="20" name="tech_contact"></td>
	</tr>
	<tr>
		<td>straat &amp; nr</td>
		<td><input type="text" maxlength="150" size="20" name="adres"></td>
	</tr>
	<tr>
		<td>postcode</td>
		<td><input type="text" maxlength="15" size="10" name="postcode"></td>
	</tr>
	<tr>
		<td>gemeente</td>
		<td><input type="text" maxlength="150" size="20" name="gemeente"></td>
	</tr>
	<tr>
		<td>telefoon</td>
		<td><input type="text" maxlength="30" size="20" name="tel"></td>
	</tr>
	<tr>
		<td>gsm</td>
		<td><input type="text" maxlength="30" size="20" name="gsm"></td>
	</tr>
	<tr>
		<td>fax</td>
		<td><input type="text" maxlength="30" size="20" name="fax"></td>
	</tr>
	<tr>
		<td>email</td>
		<td><input type="text" maxlength="150" size="20" name="email"></td>
	</tr>
	<tr>
		<td>website</td>
		<td>http://<input type="text" maxlength="150" size="20" name="website"></td>
	</tr>
	<tr>
		<td align="center" colspan="2"><input type="submit" value="voeg toe" name="submit">
	</tr>
</table>
</form>
