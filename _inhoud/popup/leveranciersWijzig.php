<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'leveranciersWijzigController.php';

?>
<?php require 'html_head.php'?>
<h1>Aanpassen van <?php print $leverancier['firma']; ?></h1>
<form action="leveranciersVerwerk.php" method="post" id="formulier">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>firma</td>
		<td><input type="text" maxlength="150" size="20" name="firma" value="<?php print $leverancier['firma']; ?>"></td>
	</tr>
	<tr>
		<td>contactpersoon</td>
		<td><input type="text" maxlength="150" size="20" name="contact" value="<?php print $leverancier['contact']; ?>"></td>
	</tr>
	<tr>
		<td>technisch contact</td>
		<td><input type="text" maxlength="150" size="20" name="tech_contact" value="<?php print $leverancier['tech_contact']; ?>"></td>
	</tr>
	<tr>
		<td>straat &amp; nr</td>
		<td><input type="text" maxlength="150" size="20" name="adres" value="<?php print $leverancier['adres']; ?>"></td>
	</tr>
	<tr>
		<td>postcode</td>
		<td><input type="text" maxlength="15" size="10" name="postcode" value="<?php print $leverancier['postcode']; ?>"></td>
	</tr>
	<tr>
		<td>gemeente</td>
		<td><input type="text" maxlength="150" size="20" name="gemeente" value="<?php print $leverancier['gemeente']; ?>"></td>
	</tr>
	<tr>
		<td>telefoon</td>
		<td><input type="text" maxlength="30" size="20" name="tel" value="<?php print $leverancier['tel']; ?>"></td>
	</tr>
	<tr>
		<td>gsm</td>
		<td><input type="text" maxlength="30" size="20" name="gsm" value="<?php print $leverancier['gsm']; ?>"></td>
	</tr>
	<tr>
		<td>fax</td>
		<td><input type="text" maxlength="30" size="20" name="fax" value="<?php print $leverancier['fax']; ?>"></td>
	</tr>
	<tr>
		<td>email</td>
		<td><input type="text" maxlength="150" size="20" name="email" value="<?php print $leverancier['email']; ?>"></td>
	</tr>
	<tr>
		<td>website</td>
		<td>http://<input type="text" maxlength="150" size="20" name="website" value="<?php print $leverancier['website']; ?>"></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="hidden" name="id" value="<?php print $leverancier['id']; ?>">
			<input type="submit" value="aanpassen" name="submit">&nbsp;&nbsp;&nbsp;
			<input type="reset" value="wissen" name="wissen">
			</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>