<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require "init.php";
?>
<?php require "html_head.php"?>
<form action="locatiesVerwerk.php" method="post" id="formulier">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td height="20" valign="bottom">
		<h3>voeg nieuwe toe</h3>
		</td>
	</tr>
	<tr>
		<td>naam</td>
		<td><input type="text" maxlength="150" size="20" name="naam"></td>
	</tr>
	<tr>
		<td>afkorting</td>
		<td><input type="text" maxlength="3" size="2" name="afkorting"></td>
	</tr>
	<tr>
		<td>straat</td>
		<td><input type="text" maxlength="150" size="20" name="straat"></td>
	</tr>
	<tr>
		<td>nummer</td>
		<td><input type="text" maxlength="150" size="20" name="nummer"></td>
	</tr>
	<tr>
		<td>postcode</td>
		<td><input type="text" maxlength="15" size="10" name="postcode"></td>
	</tr>
	<tr>
		<td>gemeente</td>
		<td><input type="text" maxlength="150" size="20" name="gemeente"></td>
	</tr>
	<tr>
		<td align="center" colspan="2"><input type="submit" value="voeg toe" name="submit">&nbsp;&nbsp;&nbsp;
		<input type="reset" value="wissen" name="wissen"></td>
	</tr>
</table>
</form>
<?php require "html_foot.php"?>