<?php
require 'init.php';
$objL = new TabLocatie();
$actie = $_POST['submit'];

switch ($actie){
	case "voeg toe": 
		$objL->addLocatie($_POST['naam'], $_POST['afkorting'], $_POST['straat'], $_POST['nummer'], $_POST['postcode'], $_POST['gemeente']); 
		break;
	case "aanpassen": 
		$objL->editLocatie($_POST['id'], $_POST['naam'], $_POST['afkorting'], $_POST['straat'], $_POST['nummer'], $_POST['postcode'], $_POST['gemeente']); 
		break;
}

?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>