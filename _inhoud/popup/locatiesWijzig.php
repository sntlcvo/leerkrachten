<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'locatiesWijzigController.php';
?>
<?php require 'html_head.php'?>
<h1>Aanpassen van <?php print $locatie['naam']; ?></h1>
<form action="locatiesVerwerk.php" method="post" id="formulier">
<table>
	<tr>
		<td>naam</td>
		<td><input type="text" maxlength="150" size="20" name="naam" value="<?php print $locatie['naam']; ?>"></td>
	</tr>
	<tr>
		<td>afkorting</td>
		<td><input type="text" maxlength="3" size="2" name="afkorting" value="<?php print $locatie['afkorting']; ?>"></td>
	</tr>
	<tr>
		<td>straat</td>
		<td><input type="text" maxlength="150" size="20" name="straat" value="<?php print $locatie['straat']; ?>"></td>
	</tr>
	<tr>
		<td>nummer</td>
		<td><input type="text" maxlength="150" size="20" name="nummer" value="<?php print $locatie['nummer']; ?>"></td>
	</tr>
	<tr>
		<td>postcode</td>
		<td><input type="text" maxlength="150" size="20" name="postcode" value="<?php print $locatie['postcode']; ?>"></td>
	</tr>
	<tr>
		<td>gemeente</td>
		<td><input type="text" maxlength="150" size="20" name="gemeente" value="<?php print $locatie['gemeente']; ?>"></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="hidden" name="id" value="<?php print $locatie['id']; ?>">
			<input type="submit" value="aanpassen" name="submit">
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>