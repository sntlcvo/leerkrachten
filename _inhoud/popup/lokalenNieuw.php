<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'lokalenNieuwController.php';
?>
<?php require 'html_head.php'?>
<form action="lokalenVerwerk.php" method="post" id="formulier">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td height="20" valign="bottom">
		<h3>voeg nieuw lokaal toe</h3>
		</td>
	</tr>
	<tr>
		<td>klasnummer</td>
		<td><input type="text" maxlength="150" size="20" name="nummer"></td>
	</tr>
	<tr>
		<td>locatie</td>
		<td><select name="locatie_id"><?php print $locatiesOptions; ?></select></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="submit" value="voeg toe" name="submit">&nbsp;&nbsp;&nbsp;
			<input type="reset" value="wissen" name="wissen">
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>