<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 19-nov-2009
 */

require 'init.php';
$objLocatie = new TabLocatie();
$locatiesOptions = TabFuncties::createOptionList(TabFuncties::htmlize($objLocatie->getLocaties(null, true)), -99, "---Maak uw keuze---");