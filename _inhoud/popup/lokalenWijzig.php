<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'lokalenWijzigController.php';

?>
<?php require 'html_head.php'?>
<form action="lokalenVerwerk.php" method="post" id="formulier">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td height="20" valign="bottom">
		<h3>aanpassen van <?php print $lokaal['nummer']; ?></h3>
		</td>
	</tr>
	<tr>
		<td>klasnummer</td>
		<td><input type="text" maxlength="150" size="20" name="nummer" value="<?php print $lokaal['nummer']; ?>"></td>
	</tr>
	<tr>
		<td>locatie</td>
		<td><select name="locatie_id"><?php print $locatiesOptions; ?></select></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="hidden" name="id" value="<?php print $lokaal['id']; ?>">
			<input type="submit" value="aanpassen" name="submit">
			</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>