<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 19-nov-2009
 */

require 'init.php';
$objLocatie = new TabLocatie();

$objLokaal = new TabLokaal();
$lokaal = $objLokaal->getLokalen($_GET['id']);
$locatiesOptions = TabFuncties::createOptionList(TabFuncties::htmlize($objLocatie->getLocaties(null, true)), $lokaal['locatie_id'], "---Maak uw keuze---");