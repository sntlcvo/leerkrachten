<?php require 'lrAddOneCourseController.php'; ?>
<?php require 'html_head.php'?>
<h1>Aanmaken van 1 cursus</h1>
<form method="post" action="lrAddOneCourseVerwerk.php">
<table>
	<tr>
		<td width="150">semester: </td>
		<td>
			<select name="periode"><?php print $periodeLijst;?></select>
		</td>
	</tr>
	<tr>
		<td>cursus: </td>
		<td><select name="cursusId"><?php print $cursussenLijst;?></select></td>
	</tr>
	<tr>
		<td>lokaal: </td>
		<td><select name="lokaalId"><?php print $lokalenLijst;?></select></td>
	</tr>
	<tr>
		<td>klascode: </td>
		<td><input type="text" name="klascode" /></td>
	</tr>
	<tr>
		<td>leerkracht:</td>
		<td><select name="leerkrachtId"><?php print $leerkrachtenLijst;?></select></td>
	</tr>
	<tr>
		<td>begindatum:</td>
		<td><input type="text" name="begindatum" size="6" /></td>
	</tr>
	<tr>
		<td>einddatum:</td>
		<td><input type="text" name="einddatum" size="6" /></td>
	</tr>
	<tr>
		<td>beginuur:</td>
		<td><input type="text" name="beginuur" size="6" /></td>
	</tr>
	<tr>
		<td>einduur:</td>
		<td><input type="text" name="einduur" size="6" /></td>
	</tr>
	<tr>
		<td>lesmoment: </td>
		<td><input type="text" name="lesmoment" size="3" />(3 cijfers: 1-dag, 2-vm/nm/av, 3-0)</td>
	</tr>
	<tr>
		<td>tonen op website:</td>
		<td><input type="checkbox" name="beschikbaar" checked /></td>
	</tr>
	<tr>
		<td>op maandag?</td>
		<td><input type="checkbox" name="maandag" /></td>
	</tr>
	<tr>
		<td>op dinsdag?</td>
		<td><input type="checkbox" name="dinsdag" /></td>
	</tr>
	<tr>
		<td>op woensdag?</td>
		<td><input type="checkbox" name="woensdag" /></td>
	</tr>
	<tr>
		<td>op donderdag?</td>
		<td><input type="checkbox" name="donderdag" /></td>
	</tr>
	<tr>
		<td>op vrijdag?</td>
		<td><input type="checkbox" name="vrijdag" /></td>
	</tr>
	<tr>
		<td>op zaterdag?</td>
		<td><input type="checkbox" name="zaterdag" /></td>
	</tr>
	<tr>
		<td>go-cursus?</td>
		<td><input type="checkbox" name="speciaal_go" /></td>
	</tr>
	<tr>
		<td>55+-cursus?</td>
		<td><input type="checkbox" name="speciaal_55" /></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" value="voeg toe" /></td>
	</tr>
</table>
</form>
<?php require "html_foot.php"; ?>