<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 9-dec-2009
 */
require 'init.php';
$objLessenrooster = new TabLessenrooster();

if ($_POST['submit'] == 'voeg toe'){
	if (isSet($_POST['maandag']))
		$dagen[] = "maandag";
	if (isSet($_POST['dinsdag']))
		$dagen[] = "dinsdag";
	if (isSet($_POST['woensdag']))
		$dagen[] = "woensdag";
	if (isSet($_POST['donderdag']))
		$dagen[] = "donderdag";
	if (isSet($_POST['vrijdag']))
		$dagen[] = "vrijdag";
	if (isSet($_POST['zaterdag']))
		$dagen[] = "zaterdag";
	if (isSet($_POST['speciaal_go']))
		$speciaal[] = "speciaal_go";
	if (isSet($_POST['speciaal_55']))
		$speciaal[] = "speciaal_55";
	//print_r($dagen);
	$beschikbaar = (isSet($_POST['beschikbaar'])) ? true : false;
	$objLessenrooster->addEenCursus($_POST['cursusId'], $_POST['periode'], $_POST['lokaalId'], $_POST['klascode'],
		$_POST['leerkrachtId'], $_POST['begindatum'], $_POST['einddatum'], $_POST['beginuur'], $_POST['einduur'], $_POST['lesmoment'],
		$dagen, $beschikbaar, $speciaal);
}
?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>