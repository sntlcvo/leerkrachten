<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'lrCourseDataController.php';

?>
<?php require "html_head.php"?>
<h1>Wijzigen cursusgegevens</h1>
<p>Wijzig gegevens van de cursus</p>
<form method="post" action="lrCourseData.php">
	klascode: <select name="cursusid"><?php print $lijstKlascodes;?></select>
	<input type="submit" name="submit" value="verzend">
</form>
<?php if (is_array($rooster)): ?>

<h2><?php print $rooster['klascode'] ?></h2>
<form method="post" action="lrCourseData.php">
<div id="cursus">
	cursus: <select name="cursusid"><?php print $lijstCursussen ?></select>
</div>
<div id="leerkracht">
	leerkracht: <select name="leerkrachtid"><?php print $lijstPersoneel ?></select>
</div>
<div id="lokaal">
	lokaal: <select name="lokaalid"><?php print $lijstLokalen ?></select>
</div>
<div id="dagen">
<table border="0">
	<tr>
		<td>
			<input type="checkbox" name="maandag" title="maandag" <?php print ($rooster['maandag'] == "ja") ? "checked" : ""; ?>>maandag
		</td>
		<td>
			<input type="checkbox" name="donderdag" title="donderdag" <?php print ($rooster['donderdag'] == "ja") ? "checked" : ""; ?>>donderdag
		</td>
	</tr>
	<tr>
		<td>
			<input type="checkbox" name="dinsdag" title="dinsdag" <?php print ($rooster['dinsdag'] == "ja") ? "checked" : ""; ?>>dinsdag
		</td>
		<td>
			<input type="checkbox" name="vrijdag" title="vrijdag" <?php print ($rooster['vrijdag'] == "ja") ? "checked" : ""; ?>>vrijdag			
		</td>
	</tr>
	<tr>
		<td>
			<input type="checkbox" name="woensdag" title="woensdag" <?php print ($rooster['woensdag'] == "ja") ? "checked" : ""; ?>>woensdag
		</td>
		<td>
			<input type="checkbox" name="zaterdag" title="zaterdag"	<?php print ($rooster['zaterdag'] == "ja") ? "checked" : ""; ?>>zaterdag
		</td>
	</tr>
</table>
</div>
<div id="les_moment">
lesmoment: <input type="text" name="lesmoment" value="<?php print $rooster['lesmoment']?>" maxlength="3" size="3" />
</div>
<div id="dag_uur">
<p>
	les begint om: <input type="text" size="4" name="beginuur" title="beginuur" value="<?php print $rooster['beginuur']; ?>" /> 
	tot: <input	type="text" size="4" name="einduur" title="einduur"	value="<?php print $rooster['einduur']; ?>" /></p>
<p>
	eerste les op: <input type="text" size="6" name="begindatum" title="begindatum"	value="<?php print $rooster['begindatum']; ?>" /> <br />
	laatste les op: <input type="text" size="6" name="einddatum" title="einddatum" value="<?php print $rooster['einddatum']; ?>" /> <br />
	semester: <select name="periode"><?php print $lijstPeriodes ?></select>
</p>
</div>
<div id="groepen">
	speciale groepen:
	<ul>
		<li class="noshow"><input type="checkbox" name="speciaal_55" title="55+" <?php print ($rooster['speciaal_55'] == "ja") ? "checked" : ""; ?>> 55+</li>
		<li class="noshow"><input type="checkbox" name="speciaal_go" title="GO" <?php print ($rooster['speciaal_go'] == "ja") ? "checked" : ""; ?>> GO</li>
		<li class="noshow"><input type="text" size="5" name="speciaal_andere" title="speciale groepen"	value="<?php print $rooster['speciaal_andere'] ?>"></li>
	</ul>
</div>
<div id="tonen">
	tonen op website: 
	<input type="checkbox" name="beschikbaar"	title="tonen op website" <?php print ($rooster['beschikbaar'] == "ja") ? "checked" : ""; ?>>
	<input type="hidden" name="cursusgegevens_id" value="<?php print $rooster['id']; ?>"> 
	<input type="submit" name="submit" value="wijzig">
</div>
</form>
<?php else: ?>

<?php endif; ?>
<?php require "html_foot.php"?>