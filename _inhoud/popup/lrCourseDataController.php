<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'init.php';

$cursusId = $_POST['cursusid'];

$objRooster = new TabLessenrooster();
$objCursus = new TabCursus();
$objPersoneel = new TabPersoneel();
$objLokaal = new TabLokaal();

if ($_POST['submit'] == "verzend"){
	$rooster = TabFuncties::htmlize($objRooster->ophalenVolledigRooster(null, $_POST['cursusid']), false);	
	$lijstPeriodes = TabFuncties::createOptionList($objRooster->getPeriodesEenvoudig(), $rooster['periode_id']);
	$lijstCursussen = TabFuncties::createOptionList($objCursus->getCursussen(null, true), $rooster['cursus_id']);
	$lijstPersoneel = TabFuncties::createOptionList($objPersoneel->ophalenEenvoudig(), $rooster['leerkracht_id']);
	$lijstLokalen = TabFuncties::createOptionList($objLokaal->getLokalen(null, true), $rooster['lokaal_id']);

} elseif ($_POST['submit'] == "wijzig"){
	//wijzig de gegevens
	$gegevens['cursus_id'] = $_POST['cursusid'];
	$gegevens['lokaal_id'] = $_POST['lokaalid'];
	$gegevens['leerkracht_id'] = $_POST['leerkrachtid'];
	$gegevens['begindatum'] = $_POST['begindatum'];
	$gegevens['einddatum'] = $_POST['einddatum'];
	$gegevens['beginuur'] = $_POST['beginuur'];
	$gegevens['einduur'] = $_POST['einduur'];
	$gegevens['beschikbaar'] = isSet($_POST['beschikbaar']) ? "ja" : "nee";
	$gegevens['maandag'] = isSet($_POST['maandag']) ? "ja" : "nee";
	$gegevens['dinsdag'] = isSet($_POST['dinsdag']) ? "ja" : "nee";
	$gegevens['woensdag'] = isSet($_POST['woensdag']) ? "ja" : "nee";
	$gegevens['donderdag'] = isSet($_POST['donderdag']) ? "ja" : "nee";
	$gegevens['vrijdag'] = isSet($_POST['vrijdag']) ? "ja" : "nee";
	$gegevens['zaterdag'] = isSet($_POST['zaterdag']) ? "ja" : "nee";
	$gegevens['lesmoment'] = $_POST['lesmoment'];
	$gegevens['speciaal_go'] = isSet($_POST['speciaal_go']) ? "ja" : "nee";
	$gegevens['speciaal_55'] = isSet($_POST['speciaal_55']) ? "ja" : "nee";
	$gegevens['speciaal_andere'] = $_POST['speciaal_andere'];
	$gegevens['periode_id'] = $_POST['periode'];
	$gegevens['beschikbaar'] = isSet($_POST['beschikbaar']) ? "ja" : "nee";
	$gegevens['id'] = $_POST['cursusgegevens_id']; 
	$objRooster->updateRooster($gegevens);
	$cursusId = -99;
}

$klascodes = $objRooster->getKlascodes();
$lijstKlascodes = TabFuncties::createOptionList($klascodes, $cursusId, 'maak uw keuze');

?>