<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'lrCoursesController.php';

?>
<?php require "html_head.php"?>
<h1>Wijzigen cursusnamen</h1>
<?php print $melding; ?>
<form method="post" action="lrCourses.php">
	cursusnaam: 
		<select name="cursusid" id="cursusnaam_oud" onchange="kopiewaarde('#cursusnaam_oud', '#cursusnaam_nieuw', false, 4);">
			<?php print $optionCursussen; ?></select><br />
	wijzig naar: <input type="text" name="cursusnaam" id="cursusnaam_nieuw" size="50" value="<?php print $cursusNaam; ?>"><br />
	<input type="submit" name="submit" value="verzend">
</form>

<?php require "html_foot.php"?>