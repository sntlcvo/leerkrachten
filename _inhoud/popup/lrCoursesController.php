<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 * @todo lege cursussen wegwerken
 */
require 'init.php';

$objCursus = new TabCursus();
$lijstCursussen = $objCursus->getCursussenFull();


foreach ($lijstCursussen as $cursus){
	$cursussen[$cursus['id']] = $cursus['cursusnaam'] . " (" . $cursus['aantal'] . ")";
}

$optionCursussen = TabFuncties::createOptionList($cursussen, $_POST['cursusid'], "maak uw keuze");

if ($_POST['submit'] == "verzend"){
	$cursusId = $_POST['cursusid'];
	$cursusNaam = trim($_POST['cursusnaam']);
	//update van de cursus
	$objCursus->updateCursusNaam($cursusId, $cursusNaam);
	$melding = "cursusnaam bijgewerkt naar $cursusNaam";
}
?>