<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'lrDeleteOneCourseController.php';

?>
<?php require "html_head.php"?>
<h1>Wissen van een cursus</h1>
<p>Wis een cursus aan de hand van de klascode</p>
<form method="post" action="lrDeleteOneCourseVerwerk.php">
<table>
	<tr>
		<td>klascode</td>
		<td><select name="cursusid"><?php print $lijstKlascodes;?></select></td>
	</tr>
	<tr>
		<td>wissen?</td>
		<td><input type="checkbox" id="wissen" name="wissen" onclick="bevestig(this)" /></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" value="verzend" /></td>
	</tr>
</table>
</form>
<?php require "html_foot.php"?>