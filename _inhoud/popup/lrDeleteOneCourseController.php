<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'init.php';

$objRooster = new TabLessenrooster();
$lijstKlascodes = TabFuncties::createOptionList( $objRooster->getKlascodes(), $cursusId, 'maak uw keuze');
