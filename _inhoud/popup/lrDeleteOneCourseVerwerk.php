<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'init.php';
$objLR = new TabLessenrooster();
if (isSet($_POST['wissen'])){
	$result = $objLR->deleteEenCursus($_POST['cursusid']);
} else {
	$result = false;
}
?>
<?php require 'html_head.php'?>
<h1>Cursus wissen</h1>
<?php if ($result): ?>
	<p>cursus werd gewist</p>
<?php else: ?>
	<p>cursus werd NIET gewist</p>
<?php endif;?>
<?php //require 'html_reload.php'?>
<?php require 'html_foot.php'?>