<?php require 'lrEditShortContentCursussenController.php'; ?>
<?php require 'html_head.php'?>
<h1>De korte inhoud van cursussen bijwerken</h1>
<h2>Selecteer een cursus</h2>
<?php print $vakkenForm ?>
<?php if (isSet($melding)): ?>
<div id="melding"><?php print $melding; ?></div>
<?php endif; ?>
<?php if (isSet($inhoudForm)): ?>
<h2>Geef de inhoud op:</h2>
<?php print $inhoudForm ?>
<?php endif?>

<?php require 'html_foot.php'?>