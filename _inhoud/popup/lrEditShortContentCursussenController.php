<?php
require "init.php";

$vakken = Tab_Cursussen::getVakken();

foreach($vakken as $v){
	$vakkenLijst[$v['id']] = $v['vak'];
}

$vakId = 0;

if (isSet($_POST['inhoud'])){
	$vakId = $_POST['id'];
	$inhoud = $_POST['inhoud'];
	$r = Tab_Cursussen::setInhoud($vakId, $inhoud);
	if ($r){
		$melding = "cursusinhoud werd bijgewerkt";
	}
} elseif (isSet($_POST['vak'])){
	$vakId = $_POST['vak'];
	$vak = Tab_Cursussen::getInhoudByVakId($_POST['vak']);
    $inhoudForm = lessenroosterForm::korteInhoud('lrEditShortContentCursussen.php', 
        $vakId, $vak->inhoud);
}

$vakkenForm = lessenroosterForm::vakkenlijst('lrEditShortContentCursussen.php', 
    $vakkenLijst, $vakId);

