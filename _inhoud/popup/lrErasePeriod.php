<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 29-okt-2009
 */
require 'lrErasePeriodController.php';
?>
<?php require "html_head.php"?>
<h1>Periode onbeschikbaar maken</h1>
<p>Geef aan van welke periode de cursussen niet meer op snt.be moeten getoond worden.</p>
<p>Let op, het kan zijn dat je nog een aantal cursussen weer zichtbaar moet plaatsen (NT2, ...).</p>
<?php if ($melding):?>
<div id="melding"><?php print $melding?></div>
<?php endif;?>
<form method="post" action="lrErasePeriod.php">
<select name="oldPeriod"><?php print $pLijst;?></select>
<br />
<input type="submit" name="submit" value="verzend" onClick="return confirm('weet u zeker dat u deze gegevens op onbeschikbaar wilt plaatsen?')" />
</form>
<?php require "html_foot.php"?>