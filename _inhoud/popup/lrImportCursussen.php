<?php require 'lrImportCursussenController.php'; ?>
<?php require 'html_head.php'?>
<h1>Lessenroosters uploaden</h1>
<h2>Verwijderen van semesters</h2>
<form method="post" action="lrImportCursussen.php">
<table width="90%">
<tr>
	<td class="vet">Kies eerst de correcte periode, let op, de lessenroosters worden definitief verwijderd voor deze periode!</td>
</tr>
<tr>
	<td>
		<select name="periode"><?php print $periodeLijst;?></select>
	</td>
</tr>
<tr>
	<td>
		<label><input type="radio" name="semesterVerwijder" value="alleenRoosters" checked />Alleen het rooster verwijderen</label><br />
		<label><input type="radio" name="semesterVerwijder" value="roostersEnSemesters" />Rooster &eacute;n semester verwijderen</label>
	</td>
</tr>
<tr>
	<td>
		<input type="submit" name="submit" value="wissen" onclick="return confirm('weet u zeker dat u deze periode wilt wissen?')">
	</td>
</tr>
</table>
</form>
<div id="melding">
<?php print $foutmelding; ?>
</div>
<h2>Toevoegen van lessenroosters</h2>
<form method="post" action="lrImportCursussen.php" enctype="multipart/form-data">
<table align="center" width="90%">
<tr>
	<td colspan="2" class="vet">
	Let op: eerst het lessenrooster van de periode wissen, voor het uploaden van een nieuwe
	</td>
</tr>
<tr>
	<td>Kies eerst de correcte periode</td>
	<td>
		<select name="periode" onchange="disableInvoer(this, 'jaarVeld','semesterVeld')";><?php print $periodeLijst;?></select>
	</td>
</tr>
<tr>
	<td>of voeg een nieuwe periode toe (jaar vb "20062007" + semester vb "1")</td>
	<td>
		<input type="text" name="jaar" value="jaar" onfocus="wisVeld(this, 'jaar');" id="jaarVeld">
		<input type="text" name="semester" value="semester" onfocus="wisVeld(this, 'semester');" id="semesterVeld">
	</td>
</tr>
<!--tr>
	<td colspan="2" style="font-size:large;">LET OP! oude lessenroosters worden verwijderd!!!</td>
</tr-->
<tr>
	<td colspan="2" class="vet">Cursus;Lokaal;Klas;StamboekNr;BeginDatum;EindDatum;BeginUur;EindUur;Lesmoment;Beschikbaar</td>
</tr>
<tr>
	<td colspan="2">bijvoorbeeld:</td>
</tr>
<tr>
	<td colspan="2" class="vet">ACCESS 2007 - DEEL 1;28;2TS GB BD11;26708140856;15/01/09;14/05/09;18:00;21:20;430;ONWAAR</td>
</tr>
<tr>
	<td>geef het csv-bestand op met de lessenroosters</td>
	<td><input type="file" name="csvbestand"></td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" name="submit" value="voeg toe"></td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>