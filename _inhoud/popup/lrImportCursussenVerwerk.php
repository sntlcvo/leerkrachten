<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 30-okt-2009
 */
require 'init.php';
$objLessenrooster = new TabLessenrooster();

$splitsing = ";";
if (isset($_POST['submit'])){
	switch($_POST['submit']){
		case "wissen":
			$inclSemester = ($_POST['semesterVerwijder'] == "roostersEnSemesters") ? true : false;
			$aantal = $objLessenrooster->deletePeriode($_POST['periode'], $inclSemester);
			$foutmelding = "er werden $aantal gegevens verwijderd";
			break;
		case "voeg toe":
			//print_r($_FILES);
			//exit();
			if ($_POST['periode'] == -99){	//de periodeId moet bepaald worden via het jaar en semester
				$periodeId = $objLessenrooster->addPeriode($_POST['jaar'], $_POST['semester']);
			} else {
				$periodeId = $_POST['periode'];
			}
			if ($periodeId == false){
				$foutmelding = "u dient een geldige periode of schooljaar/semester op te geven";
			} else {
				try {
					//$verwijderAantal = $objLessenrooster->verwijderPeriode($periodeId);
					$gegevens = $objLessenrooster->addLessenroosterCsv($periodeId, $_FILES['csvbestand']);
					$foutmelding = "er zijn {$gegevens['aantal']} roosters toegevoegd";
					if (is_array($gegevens['fouteLokalen'])){
						$foutmelding .= "<br>volgende lokalen bestaan nog niet: <br>";
						foreach($gegevens['fouteLokalen'] as $lokaal){
							$foutmelding .= "$lokaal<br>\n";
						}
					}
					if (is_array($gegevens['fouteStamboek'])){
						$foutmelding .= "<br>volgende stamboeknummers bestaan nog niet: <br>";
						foreach($gegevens['fouteStamboek'] as $sb){
							$foutmelding .= "$sb<br>\n";
						}
					}
					if (is_array($gegevens['bestaatAl'])){
						$foutmelding .= "<br>volgende klascode bestaan al voor deze periode: <br>";
						foreach($gegevens['bestaatAl'] as $sb){
							$foutmelding .= "$sb<br>\n";
						}
					}
				} catch(myException $e) {
					$foutmelding = $e->foutVerwerking();
				}
			}
			break;
	}
}
?>
