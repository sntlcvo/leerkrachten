<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 * @version 2009
 */

require_once 'lrLinkCourse2WebController.php';
$i = 1;

?>
<?php require "html_head.php"?>
<h1>Linken van cursussen</h1>
<p>Linkt cursussen aan de snt.be-link</p>
<?php if (is_array($welGevonden)): ?>
<h2>Gevonden</h2>
<p>volgende cursussen werden gematcht met een link, gelieve nogmaals na te zien</p>
<ul>
<?php foreach ($welGevonden as $cursusnaam => $link): ?>
	<li>
	<div id="naam_<?php print $i; ?>" onclick="toonVeld('#lijn_<?php print $i?>')" class="handje">
		<?php print $cursusnaam; ?>: <?php print $link ?>
	</div>
	<div id="lijn_<?php print $i?>" style="display: none" class="subLijn">
	<form method="post" id="formulier_<?php print $i?>" action="lrLinkCourse2WebVerwerk.php">
		<select name="vak"><?php print $vakkenLijst; ?></select>
		<input type="hidden" name="cursus" value="<?php print $cursusnaam ?>">
		<input type="submit" name="verzenden" value="verzenden" onclick="verzendForm('#formulier_<?php print $i?>', '#naam_<?php print $i; ?>')">
	</form>
	</div>
	</li>
	<?php $i++?>
<?php endforeach; ?>
</ul>
<?php endif; ?>

<?php if (is_array($nietGevonden)): ?>
<h2>Niet gevonden</h2>
<p>Voor deze cursussen werd er geen overeenkomstige link gevonden</p>
<ul>
<?php foreach($nietGevonden as $cursusnaam): ?>
	<li>
	<div id="naam_<?php print $i; ?>" onclick="toonVeld('#lijn_<?php print $i?>')" class="handje">
		<?php print $cursusnaam ?>
	</div>
	<div id="lijn_<?php print $i?>" style="display: none" class="subLijn"> 
	<form method="post" id="formulier_<?php print $i?>" action="lrLinkCourse2WebVerwerk.php">
		<select name="vak"><?php print $vakkenLijst; ?></select>
		<input type="hidden" name="cursus" value="<?php print $cursusnaam ?>">
		<input type="submit" name="verzenden" value="verzenden" onclick="verzendForm('#formulier_<?php print $i?>', '#naam_<?php print $i; ?>')">
	</form>
	</div>
	</li>
<?php $i++?>
<?php endforeach; ?>
</ul>
<?php endif; ?>

<?php if (is_array($gelockteCursussen)): ?>
<h2>Gelockte cursussen</h2>
<p>Een aantal cursussen waren gelockt (werden vroeger al nagezien) en werden niet aangepast</p>
<ul>
<?php foreach($gelockteCursussen as $cursusnaam => $link): ?>
	<li>
	<div id="naam_<?php print $i; ?>" onclick="toonVeld('#lijn_<?php print $i?>')" class="handje">
		<?php print $cursusnaam ?>: <?php print $link ?>
	</div>
	<div id="lijn_<?php print $i?>" style="display: none" class="subLijn">
	<form method="post" id="formulier_<?php print $i?>" action="lrLinkCourse2WebVerwerk.php">
		<select name="vak"><?php print $vakkenLijst; ?></select>
		<input type="hidden" name="cursus" value="<?php print $cursusnaam ?>">
		<input type="submit" name="verzenden" value="verzenden" onclick="verzendForm('#formulier_<?php print $i?>', '#naam_<?php print $i; ?>')">
	</form>
	</div>
	</li>
<?php $i++?>
<?php endforeach; ?>
</ul>
<?php endif; ?>
<?php require "html_foot.php"?>