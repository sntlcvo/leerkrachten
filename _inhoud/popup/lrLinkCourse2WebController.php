<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require "init.php";

$objCursus = new TabCursus();
$cursussen = $objCursus->getCursussen(null, true);

$objVak = new TabVak();
$vakken = $objVak->getVakken();
foreach ($vakken as $vak) {
	$lijst[$vak['id']] = $vak['naam'] . " - " . $vak['link'];
}

foreach ($cursussen as $cursusId => $cursus){
	$vakNaam = $vakLink = "";
	if ($objCursus->isCursusLinkLocked($cursusId)){
		$vakNaam = $objVak->getVakByCourseId($cursusId);
		$gelockteCursussen[$cursus] = $vakNaam;
	} else {
		foreach($lijst as $vakId => $vak){
			$gevonden = false;
			$vakArray = explode(" - ", $vak);
			$vakNaam = $vakArray[0];
			$vakLink = $vakArray[1];
			//controleren als cursusnaam voorkomt in de vaknaam of in de vaklink
			//if (stripos($cursus, $vakNaam) !== false || strpos($cursus, $vakLink) !== false){
			if (stripos($cursus, $vakNaam) !== false){
				$gevonden = true;
				$objCursus->updateCursusLink($cursusId, $vakId);
				$welGevonden[$cursus] = $vakNaam;
				break;
			}
		}
		if ($gevonden == false){
			$nietGevonden[] = $cursus;
		}
	}
}

$vakkenLijst = TabFuncties::createOptionList($objVak->getVakken(null,true), -99, "maak uw keuze");
if (is_array($welGevonden))
	$welGevonden = TabFuncties::htmlize($welGevonden, false);

if (is_array($nietGevonden))
	$nietGevonden = TabFuncties::htmlize($nietGevonden, false);

if (is_array($gelockteCursussen))
	$gelockteCursussen = TabFuncties::htmlize($gelockteCursussen, false);
