<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'lrVakkenController.php';

?>
<?php require 'html_head.php'?>
<form method="post" id="vak_formulier" action="lrVakkenVerwerk.php">
<table width="800">
	<tr>
		<td>naam vak:</td>
		<td><input type="text" name="vaknaam"></td>
	</tr>
	<tr>
		<td>link vak:</td>
		<td><input type="text" name="vaklink"></td>
	</tr>
	<tr>
		<td>richting:</td>
		<td><select name="richting"><?php print $richtingenLijst ?></select></td>
	</tr>
	<tr>
	<td></td>
	<td><input type="submit" name="submit" value="verzenden">
	</tr>
</table>
</form>

<h3>huidige lijst</h3>
<div id="vak_link">

</div>
<table width="800">
<?php if (is_array($vakken)): ?>
<?php foreach($vakken as $vak): ?>
	<tr id="vak_<?php print $vak['id']; ?>">
		<td class="onderlijn"><?php print $vak['naam'] ?></td>
		<td class="onderlijn"><?php print $vak['link'] ?></td>
		<td class="onderlijn"><?php print $vak['richting'] ?></td>
	</tr>
<?php endforeach; ?>
<?php endif; ?>
</table>
