<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'init.php';

$objVak = new TabVak();
$vakken = TabFuncties::htmlize($objVak->getVakken(), false);

$objRichting = new richting();
$richtingen = TabFuncties::htmlize($objRichting->getRichtingen(null, true), true);

$richtingenLijst = TabFuncties::createOptionList($richtingen, -99, "maak uw keuze"); 

