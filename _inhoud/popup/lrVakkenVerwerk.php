<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'init.php';

$objVak = new TabVak();

$objVak->addVak($_POST['vaknaam'], $_POST['vaklink'], $_POST['richting']);
?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>