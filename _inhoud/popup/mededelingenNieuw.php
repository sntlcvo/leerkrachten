<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 2-okt-2009
 */
require 'mededelingenNieuwController.php';
?>
<?php require 'html_head.php'?>
<div id="admin">
<h1>toevoegen mededelingen</h1>
<form action="mededelingenNieuwVerwerk.php" method="post">
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>titel: </td>
		<td><input type="text" name="titel" value="" /></td>
	</tr>
	<tr>
		<td>soort: </td>
		<td>
			<select name="soortId">
			<?php print $soortenMededelingenLijst ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>te verschijnen vanaf: </td>
		<td>
			<input type="text" value="<?php print $bericht['begindag']; ?>" name="begindag" size="2" maxlength="2" /> / 
			<input type="text" value="<?php print $bericht['beginmaand']; ?>" name="beginmaand" size="2" maxlength="2" /> / 
			<input type="text" value="<?php print $bericht['beginjaar']; ?>" name="beginjaar" size="4" maxlength="4" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea cols="70" rows="10" name="ckeditor"><?php print $bericht['bericht']; ?></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left">
			<input type="hidden" name="actie" value="nieuw" />
			<input type="submit" name="submit" value="plaats mededeling" />
		</td>
	</tr>
</table>
</form>
</div>
<?php require 'html_foot.php'?>