<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 2-okt-2009
 */
require "init.php";

$objMededeling = new mededelingen();

//$beperkt = ($_SESSION['objPersoon']->getGroep() < 30) ? "true" : "false";

$bericht['begindag'] = date("d");
$bericht['beginmaand'] = date("m");
$bericht['beginjaar'] = date("Y");


$soortenMededelingen = $objMededeling->soortenMededelingen(false, true);
$soortenMededelingenLijst = TabFuncties::createOptionList($soortenMededelingen, $_GET['soort']);