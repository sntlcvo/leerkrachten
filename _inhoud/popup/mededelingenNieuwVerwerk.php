<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 2-okt-2009
 */

//print_r($_POST);
require 'init.php';
$bericht = stripslashes($_POST['ckeditor']);


$objMededeling = new TabMededelingen();
switch ($_POST['actie']){
	case "wijzig":
		if (isSet($_POST['wissen'])){
			$objMededeling->wisBericht($_POST['id']);
		} else {
			$objMededeling->wijzigBericht($_POST['id'], $_POST['titel'], $_POST['ckeditor'], $_POST['soortId'], $_POST['begindag'], $_POST['beginmaand'], $_POST['beginjaar']);
		}
		break;
	case "nieuw":
		$objMededeling->nieuwBericht($_POST['titel'], $_POST['ckeditor'], $_POST['soortId'], $_POST['begindag'], $_POST['beginmaand'], $_POST['beginjaar']);
		break;
}

?>
<?php require 'html_head.php'; ?>
<?php require 'html_reload.php'?>
<a href="<?php print $GLOBALS['root']?>index.php?actie=submenu__inhoud&value=1__mededelingen_-_<?php print $_POST['soortId']?>">keer terug</a>
<?php require 'html_foot.php'?>