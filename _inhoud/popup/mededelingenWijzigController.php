<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 2-okt-2009
 */
require 'init.php';

$objMededeling = new TabMededelingen();
$bericht = $objMededeling->getMededeling($_GET['id']);
//print_r($bericht);
$datum = explode("-", $bericht['begin']);

$bericht['begindag'] = $datum[2];
$bericht['beginmaand'] = $datum[1];
$bericht['beginjaar'] = $datum[0];


$soortenMededelingen = $objMededeling->soortenMededelingen(false, true);
$soortenMededelingenLijst = TabFuncties::createOptionList($soortenMededelingen, $bericht['soortId']);