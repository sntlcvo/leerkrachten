<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */
require 'init.php';
?>
<?php require 'html_head.php'?>
<h1>Nieuwsbrief</h1>
<p>versturen van een nieuwsbrief</p>
<form method="post" action="nieuwsbriefVerwerk.php" id="formulier">
<table>
	<tr>
		<td>titel</td>
		<td><input type="text" name="titel" /></td>
	</tr>
	<tr>
		<td>groep</td>
		<td>
			<select name="groep">
				<option value="prive" selected>prive</option>
				<option value="bedrijf">bedrijf</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="ckeditor" id="ckeditor"></textarea>
			<textarea name="bericht" id="bericht" style="display: none"></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" value="verzend" id="submitForm" /></td>
	</tr>
</table>

</form>
<div id="status">
bezig met versturen
<img src="../../_images/spinner.gif" border="0">
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
	$status = jQuery('#status');
	$status.hide();
	$submitForm = jQuery('#submitForm')
	$submitForm.click(function(){
		var inhoud = CKEDITOR.instances.ckeditor.getData();
		jQuery('#bericht').val(inhoud);
		$formulier = jQuery('#formulier');
		url = $formulier.attr('action');
		$status.show();
		$submitForm.attr('disabled', true);
		jQuery.post(url,$formulier.serialize(), function(data){
			$status.html(data);
		});
		 
		return false;
	});
});
</script>
<?php require 'html_foot.php'?>