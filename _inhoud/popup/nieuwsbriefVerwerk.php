<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 28-jan-2010
 */

require 'init.php';
ini_set('max_execution_time', 300);
//print_r($_POST);
$objNB = new TabNieuwsbrieven();
$objNB->setVerzender('adm@snt.be');
$objNB->setTitel($_POST['titel']);
$objNB->setBericht(stripslashes($_POST['bericht']));
$objNB->setGroep($_POST['groep']);
$objNB->setTemplate('../../_templates/test_templ.html');
$aantal = $objNB->verzendenMail();

print "er werden $aantal mails verstuurd";