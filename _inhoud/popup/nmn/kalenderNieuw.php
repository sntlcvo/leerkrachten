<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */
require 'init.php';
?>
<?php require 'html_head.php'?>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>


<h1>Nieuwsflits</h1>
<p>toevoegen</p>
<form method="post" action="kalenderVerwerk.php">
<table>
	<tr>
		<td>titel</td>
		<td><input type="text" name="titel" /></td>
	</tr>
	<tr>
		<td colspan="2"><textarea name="ckeditor" id="ckeditor"></textarea></td>
	</tr>
	<tr>
		<td>begin</td>
		<td>
			<input type="text" name="begin" class="datepicker" size="10" maxlength="10" />
			<input type="text" name="beginUur" size="2" maxlength="2" />:
			<input type="text" name="beginMin" size="2" maxlength="2" />
		</td>
	</tr>
	<tr>
		<td>eind</td>
		<td>
			<input type="text" name="eind" class="datepicker" size="10" maxlength="10" />
			<input type="text" name="eindUur" size="2" maxlength="2" />:
			<input type="text" name="eindMin" size="2" maxlength="2" />
		</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" value="verzend" /></td>
	</tr>
</table>

</form>
<?php require 'html_foot.php'?>