<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */

require 'init.php';
//print_r($_POST);
//exit();
$objK = new TabKalender();
$bTijd = $_POST['beginUur'] . ":" . $_POST['beginMin'];
$eTijd = $_POST['eindUur'] . ":" . $_POST['eindMin'];
$actie = isSet($_POST['wissen']) ? "wissen" : $_POST['submit'];
$flashy = isSet($_POST['flashy_titel']) ? 'ja' : 'nee';

switch($actie){
	case "wijzig":
		$objK->editKalenderItem($_POST['id'], $_POST['titel'], addslashes($_POST['ckeditor']), $_POST['begin'], $bTijd, $_POST['eind'], $eTijd, $flashy);
		break;
	case "verzend":
		$objK->addKalenderItem($_POST['titel'], addslashes($_POST['ckeditor']), $_POST['begin'], $bTijd, $_POST['eind'], $eTijd, $flashy);
		break;
	case "wissen":
		$objK->deleteKalenderItem($_POST['id']);
		break;
	default:
		print "ongeldige keuze";
		exit();
}

?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>