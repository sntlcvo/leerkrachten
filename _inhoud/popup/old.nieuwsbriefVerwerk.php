<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 28-jan-2010
 */

require 'init.php';

$objNB = new TabNieuwsbrieven();
$objNB->setVerzender('adm@snt.be');
$objNB->setTitel($_POST['titel']);
$objNB->setBericht($_POST['ckeditor']);
$objNB->setTemplate('../../_templates/test_templ.html');
$objNB->verzendenMail();
?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>