<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'ontlenenEditController.php';
?>
<?php require 'html_head.php'?>
<h2>aanpassen van een ontlening</h2>
<p><?php print $item['leerkracht']?></p>
<p>lokaal: <?php print $item['lokaal']?></p>
<p>wat: <?php print $item['wat']?></p>
<form action="ontlenenVerwerk.php" method="post" id="formulier">
<table>
	<tr>
		<td>goedgekeurd?</td>
		<td>
			<input type="radio" name="bevestiging" value="ok" <?php print ($item['bevestiging'] == "ok") ? "checked" : "" ?> />
				<img src="<?php print $GLOBALS['root']?>/_images/thumbs_ok.png" width="25" /><br />
			<input type="radio" name="bevestiging" value="niet_ok" <?php print ($item['bevestiging'] == "niet_ok") ? "checked" : "" ?> />
				<img src="<?php print $GLOBALS['root']?>/_images/thumbs_niet_ok.png" width="25" /><br />
		</td>
	</tr>
	<tr>
		<td>datum terug</td>
		<td>
			<input type="text" name="datumTerug" class="datepicker" />
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="hidden" name="id" value="<?php print $item['id']?>" />
			<input type="submit" name="submit" value="aanpassen" />
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
$('.datepicker').datepicker({
	dateFormat: 'dd/mm/yy',
	changeMonth: true,
	changeYear: true
});
</script>
<?php require 'html_foot.php'?>