<?php
require 'personeelDetailsController.php';
?>
<?php require 'html_head.php'?>
<h1><?php TabFuncties::htmlize($personeelData['voll_naam']) ?></h1>
<table border="0">
<tr>
	<td rowspan="5"><?php testFotoEnPrint($personeelData['voll_naam'])?></td>
	<td align="left">email: </td>
	<td align="left"><?php TabFuncties::htmlize($personeelData['email']); ?></td>
</tr>
<tr>
	<td align="left">telefoon: </td>
	<td align="left"><?php print $personeelData['telefoon'] ?></td>
</tr>
<tr>
	<td align="left">gsm: </td>
	<td align="left"><?php print $personeelData['gsm'] ?></td>
</tr>
<tr>
	<td align="left">adres: </td>
	<td align="left">
		<?php TabFuncties::htmlize($personeelData['adres']) ?>,<br />
		<?php TabFuncties::htmlize($personeelData['woonplaats']) ?>
	</td>
</tr>
<tr>
	<td align="left">verjaardag: </td>
	<td align="left">
		<?php print $personeelData['geboortedate']; ?></td>
</tr>
</table>
<br />
<form method="post" id="periodeForm" action="personeelDetails.php" >
<table>
<tr>
	<td></td>
	<td colspan="6" align="left">
		<select name="periode" id="periodeSelect">
			<?php print $periodesLijst; ?>
		</select>
		<input type="hidden" name="personeelslidId" value="<?php print $personeelslidId; ?>" />
		<input type="submit" name="submit" value="kies een andere periode" />
	</td>
	</tr>
	<tr>
		<td style="width:110px"></td>
		<th style="width:110px">maandag</th>
		<th style="width:110px">dinsdag</th>
		<th style="width:110px">woensdag</th>
		<th style="width:110px">donderdag</th>
		<th style="width:110px">vrijdag</th>
		<th style="width:110px">zaterdag</th>
	</tr>
	<tr style="height:90px">
		<th>voormiddag</th>
		<?php foreach ($dagen as $dag): ?>
		<td class="rand klein" >
			<?php $tijd = $dag . 'vm'; if (is_array($rooster[$dag.'vm'])): ?>
				<?php print $rooster[$dag.'vm']['lokaal']; ?>, 
				<?php print $rooster[$dag.'vm']['klascode']; ?>,  
				<?php print $rooster[$dag.'vm']['cursusnaam']; ?>
			<?php else: ?>
			&nbsp;
			<?php endif; ?>
		</td>
		<?php endforeach; ?>
	</tr>
	<tr style="height:90px">
		<th>namiddag</th>
		<?php foreach ($dagen as $dag): ?>
		<td class="rand klein">
			<?php $tijd = $dag . 'nm'; if (is_array($rooster[$dag.'nm'])): ?>
				<?php print $rooster[$dag.'nm']['lokaal']; ?>, <br />
				<?php print $rooster[$dag.'nm']['klascode']; ?>, <br /> 
				<?php print $rooster[$dag.'nm']['cursusnaam']; ?>
			<?php else: ?>
			&nbsp;
			<?php endif; ?>
		</td>
		<?php endforeach; ?>
	</tr>
	<tr style="height:90px">
		<th>avond</th>
		<?php foreach ($dagen as $dag): ?>
		<td class="rand klein">
			<?php $tijd = $dag . 'av'; if (is_array($rooster[$dag.'av'])): ?>
				<?php print $rooster[$dag.'av']['lokaal']; ?>, <br />
				<?php print $rooster[$dag.'av']['klascode']; ?>, <br />
				<?php print $rooster[$dag.'av']['cursusnaam']; ?>
			<?php else: ?>
			&nbsp;
			<?php endif; ?>
		</td>
		<?php endforeach; ?>
	</tr>
	<tr>
		<td class="rand" colspan="7">eerste cijfer: lokaalnummer, tweede code:
		klascode, derde code: vak</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>