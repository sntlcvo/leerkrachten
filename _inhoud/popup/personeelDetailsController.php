<?php
require_once 'init.php';

$objLessenrooster = new TabLessenrooster();
$objPersoneel = new TabPersoneel();


if (isSet($_GET['id'])){
	$personeelslidId = $_GET['id'];
	$currPeriode = $objLessenrooster->getPeriodeId();
} else {
	$personeelslidId = $_POST['personeelslidId'];
	$currPeriode = $_POST['periode'];
}

//$personeelData = TabFuncties::htmlize($objPersoneel->ophalen($personeelslidId, true));
$personeelData = $objPersoneel->ophalen($personeelslidId);

$periodesLijst = TabFuncties::createOptionList($objLessenrooster->getPeriodesEenvoudig(), $currPeriode);

$rooster = $objLessenrooster->getLessenrooster($personeelslidId, $currPeriode);

$dagen = array(1=>"ma", 2=>"di", 3=>"wo", 4=>"do", 5=>"vr", 6=>"za");
$tijdstippen = array(1=>"vm", 2=>"nm", 3=>"av");

function testFotoEnPrint($naam){
	$naamImg = $GLOBALS['fotosLk'] . strtolower(str_replace(' ', '_', $naam)) . ".jpg";
	$naamFs = '../../'.$GLOBALS['fotosLkFs'] . strtolower(str_replace(' ', '_', $naam)) . ".jpg";;
	$geenFoto = $GLOBALS['fotosLk'] . "geen_foto.jpg";
	
	if (file_exists($naamFs)){
		print "<img src=\"$naamImg\">";
	} else {
		print "<img src=\"$geenFoto\" height=\"120\">";
	}
}