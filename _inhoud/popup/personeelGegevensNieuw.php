<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 10-nov-2009
 */
require 'personeelGegevensNieuwController.php';
?>
<?php require 'html_head.php'?>
<h1>Toevoegen van een nieuwe leerkracht</h1>
<form method="post" action="personeelGegevensVerwerk.php">
<table>
	<tr>
		<td>naam: </td>
		<td><input type="text" name="naam" value="" /></td>
	</tr>
	<tr>
		<td>stamboek: </td>
		<td><input type="text" name="stamboek" value="" /></td>
	</tr>
	<tr>
		<td>straat + nr: </td>
		<td><input type="text" name="adres" value="" /></td>
	</tr>
	<tr>
		<td>gemeente: </td>
		<td><input type="text" name="woonplaats" value="" /></td>
	</tr>
	<tr>
		<td>telefoon: </td>
		<td><input type="text" name="telefoon" value="" /></td>
	</tr>
	<tr>
		<td>gsm: </td>
		<td><input type="text" name="gsm" value="" /></td>
	</tr>
	<tr>
		<td>email: </td>
		<td><input type="text" name="email" value="" /></td>
	</tr>
	<tr>
		<td>wachtwoord: </td>
		<td><input type="text" name="wachtwoord" value="" /></td>
	</tr>
	<tr>
		<td>geboortedatum: </td>
		<td>
			<input type="text" name="geboortedatumDag" value="" size="2" maxlength="2" /> / 
			<input type="text" name="geboortedatumMaand" value="" size="2" maxlength="2" /> /
			<input type="text" name="geboortedatumJaar" value="" size="4" maxlength="4" />
		</td>
	</tr>
	<tr>
		<td>actief: </td>
		<td>
			<input type="radio" name="actief" value="ja" "checked" />ja
			<input type="radio" name="actief" value="nee" />nee
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="submit" value="voeg toe" />
		</td>
	</tr>
	<?php //foto ?>
</table>
	
</form>
<?php require 'html_foot.php'?>