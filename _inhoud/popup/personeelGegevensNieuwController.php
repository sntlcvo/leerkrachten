<?php
/**
 * @package lknet
 * @author tim
 * @version 2009.1 - 10-nov-2009
 */
require 'init.php';
$objPersoneel = new TabPersoneel();
$pers = $objPersoneel->ophalen($_GET['id']);

$gb = explode("-", $pers[geboortedate]);
$gbJaar = $gb[0];
$gbMaand = $gb[1];
$gbDag = $gb[2];
