<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 10-nov-2009
 */

require_once 'init.php';

$objPersoneel = new TabPersoneel();
switch ($_POST['submit']){
	case "wijzig":
		$objPersoneel->updateGegevens($_POST['id'], $_POST['naam'], $_POST['stamboek'], $_POST['adres'], $_POST['woonplaats'], $_POST['telefoon'], 
			$_POST['gsm'], $_POST['geboortedatumDag'], $_POST['geboortedatumMaand'], $_POST['geboortedatumJaar'], $_POST['actief']);
		break;
	case "voeg toe":
		$objPersoneel->addGegevens($_POST['naam'], $_POST['stamboek'], $_POST['email'], $_POST['wachtwoord'], $_POST['adres'], $_POST['woonplaats'], 
			$_POST['telefoon'], $_POST['gsm'], $_POST['geboortedatumDag'], $_POST['geboortedatumMaand'], $_POST['geboortedatumJaar'], $_POST['actief']);
		break;
	default:
		print "ongeldige actie!!";
		exit();
}
?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<a href="<?php print $GLOBALS['root']?>index.php?actie=submenu__inhoud&value=6__personeelGegevens">keer terug</a>
<?php require 'html_foot.php'?>