<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 10-nov-2009
 */
require 'personeelGegevensWijzigController.php';
?>
<?php require 'html_head.php'?>
<h1>Wijzigen van gegevens van <?php print $pers['voll_naam']?></h1>
<form method="post" action="personeelGegevensVerwerk.php">
<table>
	<tr>
		<td>naam: </td>
		<td><input type="text" name="naam" value="<?php print $pers['voll_naam']?>" /></td>
	</tr>
	<tr>
		<td>stamboek: </td>
		<td><input type="text" name="stamboek" value="<?php print $pers['stamboek']?>" /></td>
	</tr>
	<tr>
		<td>straat + nr: </td>
		<td><input type="text" name="adres" value="<?php print $pers['adres']?>" /></td>
	</tr>
	<tr>
		<td>gemeente: </td>
		<td><input type="text" name="woonplaats" value="<?php print $pers['woonplaats']?>" /></td>
	</tr>
	<tr>
		<td>telefoon: </td>
		<td><input type="text" name="telefoon" value="<?php print $pers['telefoon']?>" /></td>
	</tr>
	<tr>
		<td>gsm: </td>
		<td><input type="text" name="gsm" value="<?php print $pers['gsm']?>" /></td>
	</tr>
	<tr>
		<td>geboortedatum: </td>
		<td>
			<input type="text" name="geboortedatumDag" value="<?php print $gbDag?>" size="2" maxlength="2" /> / 
			<input type="text" name="geboortedatumMaand" value="<?php print $gbMaand?>" size="2" maxlength="2" /> /
			<input type="text" name="geboortedatumJaar" value="<?php print $gbJaar?>" size="4" maxlength="4" />
		</td>
	</tr>
	<tr>
		<td>actief: </td>
		<td>
			<input type="radio" name="actief" value="ja" <?php print ($pers['actief'] == "ja") ? "checked" : "" ?> />ja
			<input type="radio" name="actief" value="nee" <?php print ($pers['actief'] == "nee") ? "checked" : "" ?> />nee
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="hidden" name="id" value="<?php print $_GET['id'] ?>" />
			<input type="submit" name="submit" value="wijzig" />
		</td>
	</tr>
	<?php //foto ?>
</table>
	
</form>
<?php require 'html_foot.php'?>