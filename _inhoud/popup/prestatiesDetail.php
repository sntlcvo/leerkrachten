<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 10-feb-2010
 */
 
require 'prestatiesDetailController.php';
?>
<?php require 'html_head.php'?>
<h1>Overzicht gepresteerde uren <?php print $lk?></h1>
<table>
<?php foreach($prestaties as $p):?>
<tr>
	<td width="150"><?php print $p['van']?> - <?php print $p['tot']?></td>
	<td><?php print $p['wat']?></td>
</tr>
<?php endforeach?>
</table>
<script type="text/javascript">
jQuery('tr').addClass('even');
jQuery('tr:odd').addClass('oneven');
</script>

<?php require 'html_foot.php'?>