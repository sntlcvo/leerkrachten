<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 10-feb-2010
 */
 
require 'init.php';
$objLK = new TabPersoneel();
$objP = new TabPrestaties();
$lk = $objLK->ophalenEenvoudig($_GET['lkid']);
$prestaties = TabFuncties::htmlize($objP->getPrestatiesDetail($_GET['lkid']));
