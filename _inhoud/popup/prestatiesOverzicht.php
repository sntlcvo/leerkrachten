<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 10-feb-2010
 */
 
require 'prestatiesOverzichtController.php';
?>
<?php if (is_array($prestaties)): ?>
<table width="100%">
<tr>
	<th>leerkracht</th>
	<th>gepresteerd</th>
	<th>te presteren</th>
</tr>
<?php foreach ($prestaties as $p):?>
<tr>
	<td><a href="_inhoud/popup/prestatiesDetail.php?lkid=<?php print $p['lk_id']?>" class="fbox_small iframe"><?php print($p['voll_naam'])?></a></td>
	<td><?php print($p['gepresteerde_uren'])?></td>
	<td><?php print($p['te_presteren_uren'])?></td>
</tr>
<?php endforeach?>
</table>
<?php else: ?>
Kies eerst een schooljaar uit
<?php endif?>