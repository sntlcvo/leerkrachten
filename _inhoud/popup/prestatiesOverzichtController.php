<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 10-feb-2010
 */
 
require_once 'init.php';

if (isSet($_GET['schooljaarId'])){
	$schooljaarId = $_GET['schooljaarId'];
	$objP = new TabPrestaties();
	$prestaties = TabFuncties::htmlize($objP->getPrestaties($schooljaarId));
}

//print_r($prestaties);