<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 12-feb-2010
 */
 
require 'prestatiesUrenInstellenController.php';

?>
<?php require 'html_head.php'?>
<h1>instellen van te presteren uren voor <?php print $schooljaar ?></h1>
<!--select name="schooljaar"><?php print $schooljaren?></select-->
<table>
<?php foreach ($leerkrachten as $id => $lk):?>
<tr>
	<td><?php print $lk['naam']?></td>
	<td>
		<form method="post" action="prestatiesUrenInstellenVerwerk.php" class="submitForm">
		<input type="text" name="uren" value="<?php print $lk['uren']?>" size="3" maxlength="3" />
		<input type="hidden" name="lkId" value="<?php print $id ?>" />
		<input type="hidden" name="schooljaarId" value="<?php print $schooljaarId?>" />
		<input type="submit" name="submit" value="verzend" class="submitButton" />
		</form>
	</td>
</tr>
<?php endforeach;?>
</table>
<script type="text/javascript">
jQuery('tr').addClass('even');
jQuery('tr:odd').addClass('oneven');
jQuery('.submitButton').click(function(){
	$form = jQuery(this).parent('.submitForm');
	url = $form.attr('action');
	jQuery.post(url, $form.serialize());
	$form.parent().parent().effect('highlight', {}, 3000);
	
	return false;
});
</script>
<?php require 'html_foot.php'?>