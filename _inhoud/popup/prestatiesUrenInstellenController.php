<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 12-feb-2010
 */
 
require 'init.php';

$objLK = new TabPersoneel();
$objP = new TabPrestaties();

$schooljaarfull = $_GET['schooljaar'];
$sjf = explode('-', $schooljaarfull);

$schooljaarId = $sjf[0];
$schooljaar = $sjf[1];

$schooljaren = TabFuncties::createOptionList($objP->getSchooljaren(), -99, '--maak uw keuze--');
$leerkrachten = $objP->getTotalenSchooljaar($schooljaarId);


