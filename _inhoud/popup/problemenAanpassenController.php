<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'init.php';
$eenGegevenVerwacht = false;

$objProblemen = new TabProblemen();
$p = TabFuncties::htmlize($objProblemen->getProbleem($_GET['id']));

$opgelost = ($p['date_opgelost'] != '0000-00-00') ? "checked" : "";
