<?php
/**
 * @package snt-site
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'problemenToevoegenController.php';
?>
<?php require 'html_head.php'?>
<h1>Melden van problemen</h1>
<form method="post" action="problemenToevoegen2.php" id="formulier">
<table width="90%" border="0" cellspacing="0" cellpadding="0" class="admin">
<tr>
	<td>
	<ul>
	<?php foreach($soortenProblemen as $soortProbleem): ?>
		<li class="noshow"><input type="radio" name="soortProbleem" value="<?php print $soortProbleem['id']; ?>" />
				<?php print $soortProbleem['probleem_soort']; ?></li>
	<?php endforeach; ?>
	</ul>
	</td>
</tr>
	<tr>
		<td>
			<input type="submit" name="submit" value="verder" >
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>