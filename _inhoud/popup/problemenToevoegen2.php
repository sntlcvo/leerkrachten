<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'problemenToevoegen2Controller.php';

?>
<?php require 'html_head.php'?>
<form method="post" action="problemenVerwerk.php">
<table border="0" cellspacing="1" cellpadding="0" class="admin" align="left">
	<tr>
		<td colspan="2"><h1>melden van problemen van <?php print $probleemGegevens['probleem_soort'] ?></h1></td>
	</tr>
	<?php if ($probleemGegevens['pc_veld'] == 'ja'): ?>
	<tr>
		<td width="120"  align="left">computernummer: </td>
		<td align="left"><input type="text" name="computernummer" maxlength="7" size="7"></td>
	</tr>
	<?php endif; ?>
	<?php if ($probleemGegevens['lokaal_veld'] == 'ja'): ?>
	<tr>
		<td width="120" align="left">lokaal: </td>
		<td align="left"><input type="text" name="lokaal" maxlength="4" size="7"></td>
	</tr>
	<?php endif; ?>
	<?php if ($probleemGegevens['extra_veld'] == 'ja'): ?>
	<tr>
		<td width="120" align="left">soort toestel: </td>
		<td align="left"><input type="text" name="extra"></td>
	</tr>
	<?php endif; ?>
	<?php if ($probleemGegevens['omschrijving'] == 'ja'): ?>
	<tr>
		<td width="120" align="left">omschrijving: </td>
		<td align="left"><textarea name="omschrijving" rows="5" cols="40"></textarea></td>
	</tr>
	<?php endif; ?>
	<tr>
		<td></td>
		<td align="left">
			<input type="hidden" name="actie" value="nieuw" />
			<input type="hidden" name="soortProbleem" value="<?php print $probleemGegevens['id']; ?>" />
			<input type="submit" name="submit" value="verder" />
		</td>
	</tr>
</table>
</form>
<?php require 'html_foot.php'?>