<?php
/**
 * @package leerkrachten2009
 * @author tim brouckaert tim.b@snt-brugge.be
 * @version 5-mei-2009
 */

require 'problemenToonController.php';
?>
 <table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="80"></td>
		<td width="250"></td>
		<td width="100"></td>
		<td width="80"></td>
	</tr>
	<?php if (is_array($problemen)): ?>
	<?php foreach($problemen as $id => $p): ?>
	<tr class="menusubitem handje" onclick="toonVeld('#meer_info<?php print $id ?>', '.subRegels');">
		<td>
			<?php print $p['date_gemeld']?>
		</td>
		<td>
			<?php print $p['probleem_soort']?>
		</td>
		<td>
			<?php print $p['lokaal'] ?> <?php print ($p['computernummer'] != "") ? "/" : "" ?> <?php print $p['computernummer'] ?>
				<?php print $p['extra'] ?>
		</td>
		<td>
			<?php print ($p['date_opgelost'] != '00/00/00') ? $p['date_opgelost'] : "neen"; ?>
		</td>
	</tr>
	<tr id="meer_info<?php print $id; ?>" style="display: none;" class="subRegels detailClass">
		<td colspan="3">
		<ul>
			<li>omschrijving: <?php print nl2br($p['omschrijving'])?></li>
			<li>gemeld door: <?php print $p['leerkracht']?></li>
			<li>status: <?php print nl2br($p['status']) ?></li>
			<li>eerste consult op: <?php print ($p['date_eerste_consult'] != '00/00/00') ? $p['date_eerste_consult'] : "nog niet"; ?></li>
		</ul>
		</td>
		<td>
			<?php if ($_SESSION['objPersoneel']->getGroep() >= 30): ?>
			<a href="_inhoud/popup/problemenAanpassen.php?id=<?php print $p['probleem_id'];?>" class="fbox iframe">bijwerken</a>
			<?php endif; ?>
		</td>
	</tr>

	<?php endforeach;?>
	<?php endif; ?>
</table>