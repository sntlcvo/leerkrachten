<?php
/**
 * @package leerkrachten2009
 * @author tim brouckaert tim.b@snt-brugge.be
 * @version 5-mei-2009
 */

if (!($initLoaded)){
	require_once 'init.php';
}

$wat = null;
if (isSet($_GET['probleemId'])){
	$wat['probleem_soort_id'] = $probleemId = $_GET['probleemId'];
} else {
	$probleemId = -99;
}
if (isSet($_GET['lokaalId'])){
	$wat['lokaal'] = $lokaalId = $_GET['lokaalId'];
} else {
	$lokaalId = -99;
}
if (isSet($_GET['opgelost'])){
	$wat['opgelost'] = $opgelost = $_GET['opgelost'];
} else {
	$opgelost = -99;
}


$objProbs = new TabProblemen();
$problemen = TabFuncties::htmlize($objProbs->getProblemen($wat));

