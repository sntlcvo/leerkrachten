<?php

require_once 'init.php';

$objProblemen = new TabProblemen();

switch ($_POST['actie']) {
	case "update":
		$opgelost = (isSet($_POST['opgelost'])) ? true: false;
		$objProblemen->updateProbleem($_POST['id'], $_POST['omschrijving'], $_POST['status'], $opgelost);
		break;
	case "verwijderen":
		$objProblemen->deleteProbleem($_POST['id']);
		break;
	case "nieuw":
		$objProblemen->addProbleem($_POST['soortProbleem'], $_SESSION['objPersoneel']->getId(), $_POST['omschrijving'], $_POST['computernummer'], $_POST['lokaal'], $_POST['extra']);
		break;
	default:
		print "u heeft een foute keuze gemaakt";
		exit();
}

?>
<?php require 'html_head.php'?>
<?php require 'html_reload.php'?>
<?php require 'html_foot.php'?>