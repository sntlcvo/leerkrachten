<?php
/**
 * @package sntsite
 * @author tim
 * @version 2009.1 - 19-aug-2009
 */

require 'siteEditController.php';
?>
<?php require 'html_head.php'?>
<h1>SNT.be: inhoud bewerken</h1>
<?php if ($createEditor): ?>
<form method="post" action="<?php print $GLOBALS['root']?>/index.php?actie=submenu__inhoud&value=6__siteEdit">
<textarea cols="70" rows="10" name="ckeditor"><?php print $inhoud ?></textarea>
<input type="hidden" name="url" value="<?php print $file?>" />
</form>
<?php endif;?>
<?php if (is_array($htmls)): ?>
<ul>
<?php foreach ($htmls as $html => $fullName): ?>
<li><?php print $fullName?></li>
<!-- li><a href="<?php print $GLOBALS['root']?>/index.php?actie=submenu__inhoud&value=6__siteEdit&file=<?php print urlencode($html) ?>"><?php print substr($html, strlen($basisDir)+1) ?></a></li-->
<?php endforeach?>
</ul>
<?php endif;?>
<?php require 'html_foot.php'?>