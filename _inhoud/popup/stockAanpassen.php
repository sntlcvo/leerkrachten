<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */

require 'stockAanpassenController.php';

?>
<?php require 'html_head.php'?>
<h2>aanpassen van <?php print $item['eigenaarAfkorting']; ?><?php print $item['soortAfkorting']; ?><?php print $item['nummer']; ?></h2>
<form action="stockVerwerk.php" method="post" id="formulier">
<table cellpadding="0" cellspacing="0">
<tr>
	<td>nummer</td>
	<td>
		<input type="text" maxlength="150" size="20" name="nummer" value="<?php print $item['nummer']; ?>">
	</td>
</tr>
<!-- tr>
	<td>eigenaar</td>
	<td>
		<select name="eigenaar_id"><?php print $eigenaarsOptions; ?></select>
	</td>
</tr-->
<tr>
	<td>soort</td>
	<td>
		<select name="soort_id" onchange="disableInvoer(this, soortVeld, soortVeldAfk)"><?php print $soortenOptions; ?></select><br />
		of
		<input size="15" type="text" name="soort" value="soort"	id="soortVeld" onfocus="wisVeld(this);">
		<input size="5" type="text" name="soort_afk" value="soort_afk" onfocus="wisVeld(this);" id="soortVeldAfk">
	</td>
</tr>
<tr>
	<td>merk</td>
	<td>
		<select name="merk_id"><?php print $merkenOptions; ?></select><br />
		of
		<input size="15" type="text" name="merk" value="merk" onfocus="wisVeld(this);">
	</td>
</tr>
<tr>
	<td>type</td>
	<td>
		<select name="type_id"><?php print $typesOptions; ?></select><br />
		of
		<input size="15" type="text" name="type" value="type" onfocus="wisVeld(this);">
	</td>
</tr>
<tr>
	<td>omschrijving</td>
	<td>
		<textarea name="omschrijving"><?php print $item['omschrijving']; ?></textarea>
	</td>
</tr>
<tr>
	<td>serienummer</td>
	<td>
		<input type="text" name="serienr" value="<?php print $item['serienr']; ?>">
	</td>
</tr>
<tr>
	<td>leverancier</td>
	<td>
		<select name="leverancier_id"><?php print $leveranciersOptions; ?></select>
	</td>
</tr>
<tr>
	<td>in gebruik sedert</td>
	<td>
		<input type="text" name="ingebruiksedert" class="datepicker" value="<?php print $item['in_gebruik_sedert']?>" />
	</td>
</tr>
<tr>
	<td>lokaal</td>
	<td>
		<select name="lokaal_id"><?php print $lokalenOptions; ?></select>
	</td>
</tr>
<tr>
	<td>uitgeleend aan</td>
	<td>
		<select name="leerkracht_id"><?php print $leerkrachtenOptions; ?></select>
	</td>
</tr>
<tr>
	<td>verwijderen uit stock</td>
	<td><input type="checkbox" name="verwijderen"></td>
</tr>
<tr>
	<td align="center" colspan="2">
		<input type="hidden" name="id" value="<?php print $item['id']; ?>">
		<input type="submit" value="aanpassen" name="submit">
	</td>
</tr>
</table>
</form>

<?php require 'html_foot.php'?>