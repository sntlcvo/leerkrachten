<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'init.php';

$objStock = new TabStock();
$objLokaal = new TabLokaal();
$objLeverancier = new TabLeverancier();
$objLeerkracht = new TabPersoneel();

$lokalen = TabFuncties::htmlize($objLokaal->getLokalen(null, true));
$eigenaars = TabFuncties::htmlize($objStock->ophalenEigenaar());
$soorten = TabFuncties::htmlize($objStock->ophalenSoorten());
$merken = TabFuncties::htmlize($objStock->ophalenMerken());
$types = TabFuncties::htmlize($objStock->ophalenTypes());
$leveranciers = TabFuncties::htmlize($objLeverancier->ophalenEenvoudig());
$leerkrachten = TabFuncties::htmlize($objLeerkracht->ophalenEenvoudig());

if (isSet($_GET['id'])){
	$item = $objStock->getItems($_GET['id']);
	
	$ingebruik = explode("-", $item['in_gebruik_sedert']);
	$ingebruikDag = $ingebruik[2];
	$ingebruikMaand = $ingebruik[1];
	$ingebruikJaar = $ingebruik[0];
	
	$lokalenOptions = TabFuncties::createOptionList($lokalen, $item['lokaal_id']);
	$eigenaarsOptions = TabFuncties::createOptionList($eigenaars, $item['eigenaar_id']);
	$soortenOptions = TabFuncties::createOptionList($soorten, $item['soort_id']);
	$merkenOptions = TabFuncties::createOptionList($merken, $item['merk_id']);
	$typesOptions = TabFuncties::createOptionList($types, $item['type_id']);
	$leveranciersOptions = TabFuncties::createOptionList($leveranciers, $item['leverancier_id']);
	$leerkrachtenOptions = TabFuncties::createOptionList($leerkrachten, $item['uitgeleend_aan_id'], "------");	
} else {
	$lokalenOptions = TabFuncties::createOptionList($lokalen, 0, "------");
	$eigenaarsOptions = TabFuncties::createOptionList($eigenaars, 0, "------");
	$soortenOptions = TabFuncties::createOptionList($soorten, 0, "------");
	$merkenOptions = TabFuncties::createOptionList($merken, 0, "------");
	$typesOptions = TabFuncties::createOptionList($types, 0, "------");
	$leveranciersOptions = TabFuncties::createOptionList($leveranciers, 0, "------");
	$leerkrachtenOptions = TabFuncties::createOptionList($leerkrachten, 0, "------");
}
