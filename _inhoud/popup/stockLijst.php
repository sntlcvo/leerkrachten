<?php
/**
 * @package leerkrachten2009
 * @author tim brouckaert tim.b@snt-brugge.be
 * @version 12-mei-2009
 */

require 'stockLijstController.php';
//print_r($lijstStock);

?>
<?php //require 'html_head.php'?>
<table>
<?php if (is_array($lijstStock)): ?>
<tr>
	<td></td>
	<td colspan="4">aantal gevonden items: <?php print $aantalItems?></td>
</tr>
<?php foreach ($lijstStock as $item): ?>
<tr>
	<td width="30">
		<?php /* if ($_SESSION['objPersoneel']->getGroep() >= 30): */ ?>
			<a title="aanpassen item" class="fbox iframe" href="_inhoud/popup/stockAanpassen.php?id=<?php print $item['id']; ?>" target="_blank">
				<img src="_images/potlood.png" border="0"></a>
		<?php /* endif; */ ?>
	</td>
	<td width="100" onclick="toonVeld('#extraLijn<?php print $item['id']; ?>', '.extraLijnen');" class="handje">
		<?php //print $item['eigenaarAfkorting'] ?><?php print $item['soortAfkorting'] ?><?php print $item['nummer'] ?>
	</td>
	<td width="200">
		<?php print $item['merk'] ?>: <?php print $item['soort'] ?> - <?php print $item['type']?>
	</td>
	<td width="200">
		<?php print $item['lokaal'] ?>
	</td>
</tr>
<tr id="extraLijn<?php print $item['id']; ?>" class="extraLijnen">
	<td></td>
	<td colspan="2">
		<p>
		<?php print nl2br($item['omschrijving']) ?><br />
		<?php if ($item['serienr'] != ""): ?>
		sn: <?php print $item['serienr'] ?><br />
		<?php endif?>
		<?php if ($item['leerkracht'] != ""): ?>
		uitgeleend aan: <?php print $item['leerkracht'] ?><br />
		<?php endif?>	
		</p>
	</td>
	<td colspan="2">
		<p>
		<?php if ($item['leverancier'] != ""): ?>
		leverancier: <?php print $item['leverancier'] ?><br />
		<?php endif?>
		in gebruik sedert: <?php print $item['in_gebruik_sedert'] ?><br />
		</p>
	</td>
</tr>
<tr>
	<td colspan="6">
	<hr>
	</td>
</tr>
<?php endforeach; ?>
<?php else: ?>
<tr>
	<td colspan="6">geen items gevonden</td>
</tr>
<?php endif; ?>
<!-- tr>
	<td colspan="3"><a
		href="index.php?pag=stock&start=<?php print $vorigeLink; ?>">vorige</a></td>
	<td colspan="3" align="right"><a
		href="index.php?pag=stock&start=<?php print $volgendeLink; ?>">volgende</a></td>
</tr-->
</table>
<script type="text/javascript">
	$('.extraLijnen').hide();
	$(".fbox").fancybox({
		'frameWidth': 600,
		'frameHeight': 600
		});
</script>
<?php //require 'html_foot.php'?>