<?php
/**
 * @package leerkrachten2009
 * @author tim brouckaert tim.b@snt-brugge.be
 * @version 12-mei-2009
 */
 
require_once 'init.php';

$objStock = new TabStock();

if (isSet($_GET['herbegin'])){
	unset($_SESSION['stockGegevens']);
	exit();
}

if (isSet($_GET['merkId'])){
	$merkId = $_SESSION['stockGegevens']['merkId'] = $_GET['merkId'];
} elseif (isSet($_SESSION['stockGegevens']['merkId'])) {
	$merkId = $_SESSION['stockGegevens']['merkId'];
}
if (isSet($_GET['soortId'])){
	$soortId = $_SESSION['stockGegevens']['soortId'] = $_GET['soortId'];
} elseif (isSet($_SESSION['stockGegevens']['soortId'])){
	$soortId = $_SESSION['stockGegevens']['soortId'];
}
if (isSet($_GET['lokaalId'])){
	$lokaalId = $_SESSION['stockGegevens']['lokaalId'] = $_GET['lokaalId'];
} elseif (isSet($_SESSION['stockGegevens']['lokaalId'])){
	$lokaalId = $_SESSION['stockGegevens']['lokaalId'];
}

$stockOnderdelen = $objStock->getItems(null, $merkId, $soortId, $lokaalId);
$lijstStock = TabFuncties::htmlize($stockOnderdelen);

$aantalItems = count($stockOnderdelen);