<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require_once 'stockAanpassenController.php';

?>
<?php require 'html_head.php'?>
<h2>toevoegen van nieuw stock-item</h2>
<form action="stockVerwerk.php" method="post" id="formulier">
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="150">nummer</td>
	<td><input type="text" maxlength="150" size="20" name="nummer"></td>
</tr>
<!-- tr>
	<td>eigenaar</td>
	<td><select name="eigenaar_id"><?php print $eigenaarsOptions; ?></select></td>
</tr-->
<tr>
	<td>soort</td>
	<td>
		<select name="soort_id"><?php print $soortenOptions; ?></select><br />
		of<input size="15" type="text" name="soort" value="soort" id="soortVeld" onfocus="wisVeld('.soortVeld');">
		<input size="5" type="text" name="soort_afk" value="soort_afk" onfocus="wisVeld(this);">
	</td>
</tr>
<tr>
	<td>merk</td>
	<td>
		<select name="merk_id"><?php print $merkenOptions; ?></select><br />
		of<input size="15" type="text" name="merk" value="merk" onfocus="wisVeld(this);">
	</td>
</tr>
<tr>
	<td>type</td>
	<td>
		<select name="type_id"><?php print $typesOptions; ?></select><br />
		of<input size="15" type="text" name="type" value="type" onfocus="wisVeld(this);">
	</td>
</tr>
<tr>
	<td>omschrijving</td>
	<td><textarea name="omschrijving"></textarea></td>
</tr>
<tr>
	<td>serienummer</td>
	<td><input type="text" name="serienr" value="<?php print $item['serienr']; ?>"></td>
</tr>
<tr>
	<td>leverancier</td>
	<td><select name="leverancier_id"><?php print $leveranciersOptions; ?></select></td>
</tr>
<tr>
	<td>in gebruik sedert</td>
	<td>
		<input type="text" name="dag" value="<?php print date("d"); ?>" size="2" maxlength="2"> /  
		<input type="text" name="maand" value="<?php print date("m"); ?>" size="2" maxlength="2"> /
		<input type="text" name="jaar" value="<?php print date("Y"); ?>" size="4" maxlength="4">
	</td>
</tr>
<tr>
	<td>lokaal</td>
	<td><select name="lokaal_id"><?php print $lokalenOptions; ?></select></td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="voeg toe" name="submit"></td>
</tr>
</table>
</form>
<?php require 'html_foot.php'?>