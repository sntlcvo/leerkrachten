<?php
require_once 'init.php';

$objStock = new TabStock();
$actie = (isSet($_POST['verwijderen'])) ? "verwijderen" : $_POST['submit'];
$gegevens = $_POST;

$soortId = ($_POST['soort'] != "" && $_POST['soort'] != "soort") ? $objStock->addSoort($_POST['soort'], $_POST['soort_afk']) : $_POST['soort_id'];
$merkId = ($_POST['merk'] != "" && $_POST['merk'] != "merk") ? $objStock->addMerk($_POST['merk']) : $_POST['merk_id'];
$typeId = ($_POST['type'] != "" && $_POST['type'] != "type") ? $objStock->addType($_POST['type']) : $_POST['type_id'];    

switch ($actie){
	case "voeg toe": 
		$objStock->addStock($_POST['nummer'], $merkId, $_POST['eigenaar_id'], $typeId, $soortId, $_POST['omschrijving'], 
			$_POST['serienr'], $_POST['leverancier_id'], $_POST['dag'], $_POST['maand'], $_POST['jaar'], $_POST['lokaal_id']); 
		break;
	case "aanpassen": 
		$objStock->editStock($_POST['id'], $_POST['nummer'], $merkId, $_POST['eigenaar_id'], $typeId, $soortId, $_POST['omschrijving'], 
			$_POST['serienr'], $_POST['leverancier_id'], $_POST['ingebruiksedert'], $_POST['lokaal_id'], $_POST['leerkracht_id']); 
			break;
	case "ontlenen":
		$objStock->ontlenenAanvraag($_POST['itemId'], $_SESSION['objPersoon']->getId(), $_POST['vanafDag'], $_POST['vanafMaand'], $_POST['vanafJaar'], $_POST['totDag'], $_POST['totMaand'], $_POST['totJaar']);
		break;
	case "verwijderen":
		$objStock->verwijder($_POST['id']);
		break;
}

?>
<?php require 'html_head.php'?>
<?php //require 'html_reload.php'?>
<?php require 'html_foot.php'?>