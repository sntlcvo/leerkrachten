<?php
/**
 * @package leerkrachtennet
 * @author tim brouckaert
 * @version 2009.1
 */
require 'uurroostersLijstController.php';
?>
<table width="90%">
<?php if (is_array($rooster)): ?>
<?php foreach ($rooster as $r): ?>
<tr>
	<td width="30"><?php print $r['lokaal'] ?></td>
	<td><?php print $dagen[$r['tijdstip']] ?></td>
	<td><?php print $r['klascode'] ?></td>
</tr>
<tr>
	<td></td>
	<td><?php print $r['cursusnaam'] ?></td>
	<td><?php print $r['leerkracht'] ?></td>
</tr>
<tr>
	<td class="onderlijn" colspan=3"></td>
</tr>
<?php endforeach; ?>
<?php else: ?>
<tr><td>geen gegevens weer te geven</td></tr>
<?php endif; ?>
</table>