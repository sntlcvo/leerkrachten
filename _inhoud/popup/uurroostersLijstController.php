<?php
/**
 * @package leerkrachtennet
 * @author tim brouckaert
 * @version 2009.1
 */

require_once 'init.php';

//$objLokaal = new TabLokaal();
$objPersoneel = new TabPersoneel();
$objLessenrooster = new TabLessenrooster();

if (isSet($_GET['herbegin'])){
	unset($_SESSION['roosterGegevens']);
	exit();
}

$roosterGegevens = $_SESSION['roosterGegevens'];

if (isSet($_GET['periode_id'])){
	$_SESSION['roosterGegevens']['periode_id'] = $roosterGegevens['periode_id'] = $_GET['periode_id'];
} elseif (isSet($_SESSION['roosterGegevens']['periode_id'])) {
	$roosterGegevens['periode_id'] = $_SESSION['roosterGegevens']['periode_id'];
} else {
	$_SESSION['roosterGegevens']['periode_id'] = $roosterGegevens['periode_id'] = $objLessenrooster->getPeriodeId(); 
}

if (isset($_GET['lokaal']) && $_GET['lokaal'] != -99){
	$_SESSION['roosterGegevens']['lokaal_id'] = $roosterGegevens['lokaal_id'] = $_GET['lokaal'];
}

if (isset($_GET['tijdstip']) && $_GET['tijdstip'] != -99){
	$_SESSION['roosterGegevens']['lesmoment'] = $roosterGegevens['lesmoment'] = $_GET['tijdstip'];
}

if (isset($_GET['leerkracht']) && $_GET['leerkracht'] != -99 && $_GET['leerkracht'] != 0){
	$_SESSION['roosterGegevens']['leerkracht_id'] = $leerkracht = $roosterGegevens['leerkracht_id'] = $_GET['leerkracht'];
}

if (isset($_GET['vak']) && $_GET['vak'] != -99 && $_GET['vak'] != 0){
	$_SESSION['roosterGegevens']['vakken_id'] = $vak = $roosterGegevens['vakken_id'] = $_GET['vak'];
}

if (isSet($roosterGegevens) && count($roosterGegevens) > 1){
	$rooster = TabFuncties::htmlize($objLessenrooster->getLessenRoosterUitgebreid($roosterGegevens));
}

$dagen = array('110' => 'maandagvoormiddag', '120' => 'maandagnamiddag', '130' => 'maandagavond',
		'210' => 'dinsdagvoormiddag', '220' => 'dinsdagnamiddag', '230' => 'dinsdagavond',
		'310' => 'woensdagvoormiddag', '320' => 'woensdagnamiddag', '330' => 'woensdagavond',
		'410' => 'donderdagvoormiddag', '420' => 'donderdagnamiddag', '430' => 'donderdagavond',
		'510' => 'vrijdagvoormiddag', '520' => 'vrijdagnamiddag', '530' => 'vrijdagavond',
		'610' => 'zaterdagvoormiddag');
