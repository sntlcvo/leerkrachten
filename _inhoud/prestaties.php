<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 9-feb-2010
 */
require 'prestatiesController.php'
?>
<h1>Prestaties leerkrachten</h1>
<?php if ($_SESSION['objPersoneel']->getGroep() >= 30):?>
<?php foreach ($schooljarenfull as $id => $sj): ?>
<a href="_inhoud/popup/prestatiesUrenInstellen.php?schooljaar=<?php print $id ?>-<?php print $sj?>" class="fbox iframe">
	uren instellen (<?php print $sj ?>)</a><br />
<?php endforeach; ?>
<h2>nieuwe toevoegen</h2>
<form method="post" action="_inhoud/popup/prestatiesNieuw.php" id="frmPrestatiesInvoer" >
<table>
	<tr>
		<td>leerkracht:</td>
		<td><select name="leerkrachtId"><?php print $leerkrachten?></select></td>
	</tr>
	<tr>
		<td>schooljaar:</td>
		<td><select name="schooljaarId"><?php print $schooljaren?></select></td>
	</tr>
	<tr>
		<td>datum:</td>
		<td><input type="text" name="datum" class="datepicker" /></td>
	</tr>
	<tr>
		<td>beginuur: </td>
		<td><input type="text" name="beginUur" size="2"/>: <input type="text" name="beginMin" size="2"/></td>
	</tr>
	<tr>
		<td>einduur:</td>
		<td><input type="text" name="eindUur" size="2" />: <input type="text" name="eindMin" size="2" /></td>
	</tr>
	<tr>
		<td>omschrijving:</td>
		<td><input type="text" name="wat" size="50" /></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="eind" value="invoeren" id="smbInvoer" /></td>
	</tr>
</table>
</form>
<div id="status"></div>
<?php endif;?>
<h2>overzicht gepresteerde uren</h2>
<form method="post" id="frmSchooljaar">
<select id="selectSchooljaar" name="schooljaarId"><?php print $schooljaren?></select>
<input type="button" id="submitoverzicht" name="submitoverzicht" value="zoek uren" />
</form>
<div id="overzichtUren" style="width: 500px">

</div>
<script type="text/javascript">
$('.datepicker').datepicker({
	dateFormat: 'dd/mm/yy'
});

jQuery('#status').hide();
jQuery('#smbInvoer').click(function(){
	url = jQuery('#frmPrestatiesInvoer').attr('action');
	jQuery.post(url, jQuery('#frmPrestatiesInvoer').serialize(), function(d){
		jQuery('#status').text(d).show();
		//jQuery('#overzichtUren').load('_inhoud/popup/prestatiesOverzicht.php');
		$(':input','#frmPrestatiesInvoer').not(':button, :submit, :reset, :hidden')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');

	});
	return false;
});

jQuery('#selectSchooljaar').change(function(){
	haalPrestatiesOverzicht();
});

jQuery('#submitoverzicht').click(function(){
	haalPrestatiesOverzicht();
});

function haalPrestatiesOverzicht(){
	data = jQuery('#frmSchooljaar').serialize();
	jQuery('#overzichtUren').load('_inhoud/popup/prestatiesOverzicht.php', data);
}

</script>