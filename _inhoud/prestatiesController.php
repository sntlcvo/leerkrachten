<?php
/**
 * @package lknet2010
 * @author tim brouckaert
 * @version 9-feb-2010
 */
 
$objLK = new TabPersoneel();
$objPres = new TabPrestaties();
$schooljarenfull = $objPres->getSchooljaren();

foreach ($schooljarenfull as $id => $sj){
	$schooljarenId = $id;
}

//$leerkrachten = TabFuncties::createOptionList($objLK->ophalenEenvoudig(), -99, '--maak uw keuze--');
$leerkrachten = TabFuncties::createOptionList($objPres->getLKmetUren(1), -99, '--kies de leerkracht--');
$schooljaren = TabFuncties::createOptionList($schooljarenfull, $schooljarenId, '--kies het schooljaar');