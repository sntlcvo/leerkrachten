<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2008.1
 */
require 'problemenController.php';
?>
<h1>overzicht problemen</h1>
<form method="post" action="_inhoud/problemen.php" id="formulier">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="4"><a href="_inhoud/popup/problemenToevoegen.php" class="fbox_small iframe">
			toevoegen van nieuw probleem</a>
		</td>
	</tr>
	<tr>
		<td width="80">datum</td>
		<td width="250">categorie</td>
		<td width="100">klas / toestel</td>
		<td width="80">opgelost</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<select name="probleemId" onchange="updateVeld('#toonProbs', '_inhoud/popup/problemenToon.php?probleemId='+this.value);" id="probleemId">
				<?php print $soortenProblemenLijst; ?>
			</select>
		</td>
		<td>
			<select name="lokaalId" onchange="updateVeld('#toonProbs', '_inhoud/popup/problemenToon.php?lokaalId='+this.value);" id="lokaalId">
				<?php print $lokaalLijst; ?>
			</select>
		</td>
		<td>
			<select name="opgelost" onchange="updateVeld('#toonProbs', '_inhoud/popup/problemenToon.php?opgelost='+this.value);" id="opgelost">
				<?php print $opgelostLijst; ?>
			</select>
		</td>
	</tr>
</table>
</form>
<div id="toonProbs">
<?php require_once '_inhoud/popup/problemenToon.php'?>
</div>