<?php
/**
 * @package sntsite
 * @author tim brouckaert
 * @version 2009
 */

$objProbs = new TabProblemen();

$soortenProblemenLijst = TabFuncties::createOptionList($objProbs->alleenProbleemOmschrijving(), '', "kies een categorie");

$lokaalLijst = TabFuncties::createOptionList($objProbs->getLokalen(), -99, "--");

$opgelostLijst = TabFuncties::createOptionList(array("neen" => "neen", "ja" => "ja"), -99, "--");

?>