<?php
require 'stockController.php';
?>

<h1>Materiaal per klas</h1>
<?php /* if ($_SESSION['objPersoneel']->getGroep() >= 30): */ ?> 
<a title="voeg nieuwe toe" href="_inhoud/popup/stockNieuw.php" class="fbox iframe">voeg nieuwe toe</a>
<?php /* endif; */ ?>

<div id="stock_zoek">
<form method="post"	action="index.php?pag=stock&start=<?php print $startLijst; ?>">
<table>
	<tr>
		<td>merk:</td>
		<td>
			<select id="merkId" name="merkId" onchange="updateVeld('#stock_inhoud', '_inhoud/popup/stockLijst.php?merkId='+this.value);">
				<?php print $merkenOptions; ?></select>
		</td>
	</tr>
	<tr>
		<td>soort:</td>
		<td>
			<select id="soortId" name="soortId" onchange="updateVeld('#stock_inhoud', '_inhoud/popup/stockLijst.php?soortId='+this.value);">
				<?php print $soortOptions; ?></select>
		</td>
	</tr>
	<tr>
		<td>lokaal:</td>
		<td>
			<select id="lokaalId" name="lokaalId" onchange="updateVeld('#stock_inhoud', '_inhoud/popup/stockLijst.php?lokaalId='+this.value);">
				<?php print $lokalenOptions; ?></select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="button" name="herbegin" value="herbegin"
			onclick="updateVeld('#stock_inhoud', '_inhoud/popup/stockLijst.php?herbegin=true'); resetSelect('lokaalId'); resetSelect('soortId'); resetSelect('merkId');">
		</td>
	</tr>
</table>
</form>
</div>

<div id="stock_inhoud">

</div>
