<?php

$objStock = new TabStock();
$objLokaal = new TabLokaal();

$startLijst = (isSet($_GET['start'])) ? $_GET['start'] : "0";
$volgendeLink = $startLijst + 50;
$vorigeLink = ($startLijst >= 50) ? $startLijst - 50 : 0;



$merken = TabFuncties::htmlize($objStock->ophalenMerken());
$merkenOptions = TabFuncties::createOptionList($merken, $merkId, "----");

$soorten = TabFuncties::htmlize($objStock->ophalenSoorten());
$soortOptions = TabFuncties::createOptionList($soorten, $soortId, "----");

$lokalen = TabFuncties::htmlize($objLokaal->getLokalen(null, true));
$lokalenOptions = TabFuncties::createOptionList($lokalen, $lokaalId, "----");

?>