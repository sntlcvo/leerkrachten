<?php require 'uurroostersController.php'; ?>
<div id="uurRoosterForm">
<form method="post" action="">
<table width="90%">
	<tr>
		<td width="20"></td>
		<td colspan="3">
		<h1>lessenroosters</h1>
		</td>
	</tr>
	<tr>
		<td width="20"></td>
		<td>kies lokaal</td>
		<td><select name="lokaal" id="lokaal"
			onchange="updateVeld('#uurRoosterLijst', '_inhoud/popup/uurroostersLijst.php?lokaal='+this.value);">
			<?php print $lokalenLijstOption; ?>
		</select></td>
	</tr>
	<tr>
		<td></td>
		<td>kies tijdstip</td>
		<td><select name="tijdstip" id="tijdstip"
			onchange="updateVeld('#uurRoosterLijst', '_inhoud/popup/uurroostersLijst.php?tijdstip='+this.value);">
			<?php print $dagenLijstOption; ?>
		</select></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td>kies leerkracht</td>
		<td><select name="leerkracht" id="leerkracht"
			onchange="updateVeld('#uurRoosterLijst', '_inhoud/popup/uurroostersLijst.php?leerkracht='+this.value);">
			<?php print $leerkrachtenLijstOption; ?>
		</select></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td>kies vak</td>
		<td><select name="vak_id" id="vak_id"
			onchange="updateVeld('#uurRoosterLijst', '_inhoud/popup/uurroostersLijst.php?vak='+this.value);">
			<?php print $vakLijstOption; ?>
		</select></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td>kies semester</td>
		<td><select name="periode_id" id="periode_id"
			onchange="updateVeld('#uurRoosterLijst', '_inhoud/popup/uurroostersLijst.php?periode_id='+this.value);">
			<?php print $periodeLijstOption; ?>
		</select></td>
	</tr>
	<tr>
		<td width="20"></td>
		<td></td>
		<td><input type="button" name="herbegin" value="herbegin"
			onclick="updateVeld('#uurRoosterLijst', '_inhoud/popup/uurroostersLijst.php?herbegin=true'); resetSelect('#lokaal'); resetSelect('tijdstip'); resetSelect('leerkracht'); resetSelect('periode_id');">
		</td>
	</tr>
</table>
</form>
</div>
<div id="uurRoosterLijst"></div>
