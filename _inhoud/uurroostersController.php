<?php
require_once '_controllers/init.php';

$objLokaal = new TabLokaal();
$objPersoneel = new TabPersoneel();
$objLessenrooster = new TabLessenrooster();

$lokalenLijst = $objLokaal->getLokalen(null, true);
$lokalenLijstOption = TabFuncties::createOptionList($lokalenLijst, -99, "maak uw keuze");
$dagen = array('110' => 'maandagvoormiddag', '120' => 'maandagnamiddag', '130' => 'maandagavond',
		'210' => 'dinsdagvoormiddag', '220' => 'dinsdagnamiddag', '230' => 'dinsdagavond',
		'310' => 'woensdagvoormiddag', '320' => 'woensdagnamiddag', '330' => 'woensdagavond',
		'410' => 'donderdagvoormiddag', '420' => 'donderdagnamiddag', '430' => 'donderdagavond',
		'510' => 'vrijdagvoormiddag', '520' => 'vrijdagnamiddag', '530' => 'vrijdagavond',
		'610' => 'zaterdagvoormiddag');
$dagenLijstOption = TabFuncties::createOptionList($dagen, $_POST['tijdstip'], "maak uw keuze");

$leerkrachtenLijst = $objPersoneel->ophalenEenvoudig();
$leerkrachtenLijstOption = TabFuncties::createOptionList($leerkrachtenLijst, $leerkracht, "maak uw keuze");

$periodeId = $objLessenrooster->getPeriodeId();

$periodeLijst = $objLessenrooster->getPeriodesEenvoudig();
$periodeLijstOption = TabFuncties::createOptionList($periodeLijst, $periodeId);

$vakLijstOption = TabFuncties::createOptionList($objLessenrooster->getVakken(null, true), $_POST['vak_id'], "maak uw keuze");
