<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 27-nov-2009
 */
require '../../includes/init.php';

$objFS = new TabFileSystem();

$dir = realpath("../../htdocs/h_aanbod");
$inhoud = $objFS->readDir($dir, true, false, true, null, true);
$objFS->chmodRecursive($inhoud, 777, array('inhoud.html'));