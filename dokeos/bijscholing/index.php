<?php
session_start();
if (($_SESSION['login_ok'])!="ok"){
  header ("location: login.php");
  exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Tools</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="/css/algemeen.css" rel="stylesheet" type="text/css">
<link href="/css/sophie.css" rel="stylesheet" type="text/css"></head>

<body>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h1>Bijscholing Dokeos  </h1>
      <blockquote>
        <p><a href="../opleiding/brochure/brochure/index.php" target="inhoud"></a></p>
        <table width="949" border="0">
          <tr>
            <td width="100"><img src="teacher.png" alt="mr dokeos" width="449" height="340"></td>
            <td width="833"><blockquote>
              <p>Wens je ook kennis te maken met Mr Dokeos? Schrijf je dan in voor de cursus &quot;Elektronische leerplatform - dokeos&quot; op vrijdagnamiddag (tweede semester schooljaar 2006-2007).</p>
            </blockquote></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
		<p><font color=green><b>Veel gestelde vragen ivm de bijscholing Dokeos voor leerkrachten</b></font></p>
		<p><font color=blue><b>V:</b></font> Hoeveel opleidingcheques moet ik aanvragen?<br>
	    <font color=orange><b>A:</b></font> Vraag aan de VDAB voor 40 euro aan opleidingscheques aan. Je zal de helft moeten betalen, maar het bedrag wordt door de SNT teruggestort na het indienen van de cheques.</p>
		<p> <font color=blue><b>V:</b></font> Hoeveel opleidingcheques moet ik aanvragen als ik ook een USB-stick wens aan te schaffen?<br>
	    <font color=orange><b>A:</b></font> Vraag aan de VDAB voor 30 euro aan opleidingscheques aan (voor de cursus en de USB-stick moet je dus in totaal voor 70 euro aanvragen). Je zal de helft moeten betalen, maar het bedrag wordt door de SNT teruggestort na het indienen van de cheques.</p>
		<p><font color=blue><b>V:</b></font> Wanneer gaan de cursussen door?<br>
	    <font color=orange><b>A:</b></font> De cursus gaat om de 14 dagen door op vrijdagnamiddag (14u25 tem 17u45). De week dat je niet naar de SNT komt, zal er gevraagd worden om oefeningen op te lossen. Startdatum = 19 januari 2007. </p>
		<p><font color=blue><b>V:</b></font> Wanneer kan ik mij inschrijven?<br>
          <font color=orange><b>A:</b></font> Je mag nu al inschrijven op het SNT-secretariaat. </p>
		<p><font color=blue><b>        V:</b></font> Wat wordt er gegeven in de cursus? <br>
	    <font color=orange><b>A:</b></font></p>
		<table width="63%" border="0" cellspacing="5" cellpadding="2">
          <tr bgcolor="#99CC99">
            <td align="center" width="57">Lesweek</div></td>
            <td width="63">Waar?</td>
            <td width="445">Onderwerp</td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">1</td>
            <td>SNT</td>
            <td><ul>
              <li>Dokeos:
                <ul>
                    <li>Wat </li>
                  <li> Doel</li>
                  <li> Overlopen van de mogelijkheden</li>
                </ul>
              </li>
              <li> Open Source
                <ul>
                    <li> Wat</li>
                  <li> Waarom</li>
                  <li> Voor- en nadelen</li>
                </ul>
              </li>
              <li>Webbrowser Mozilla FireFox</li>
            </ul>              </td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">2</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">3</td>
            <td>SNT</td>
            <td><ul>
              <li>Cursussen
                <ul>
                    <li> Aanmaken</li>
                  <li> Beheer</li>
                  <li> Cursuseigenschappen</li>
                  <li> Statistiek</li>
                  <li> Kopi&euml;ren van de cursusinhoud</li>
                  <li> Cursusbeschrijving</li>
                  <li> Gebruikers</li>
                  <li> Backup maken en terugzetten</li>
                </ul>
              </li>
              <li>Agenda</li>
            </ul>             </td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">4</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">5</td>
            <td>SNT</td>
            <td><ul>
              <li>Aankondigingen</li>
              <li> Links</li>
              <li> Chat</li>
              <li>Forum</li>
            </ul>              
            </td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">6</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">7</td>
            <td>SNT</td>
            <td><ul>
              <li>Documenten
                <ul>
                    <li> kopi&euml;ren in de cursus</li>
                  <li> Bewerken</li>
                  <li> Verwijderen, verplaatsen, &hellip;</li>
                </ul>
              </li>
              <li>Winzip/gecomprimeerde bestanden</li>
            </ul>       
            </td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">8</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">9</td>
            <td>SNT</td>
            <td>
              <ul>
                <li>Acrobat</li>
                <li>Leerpad</li>
              </ul>
            </td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">10</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">11</td>
            <td>SNT</td>
            <td><ul>
              <li>De module  oefeningen
                <ul>
                    <li> Basisoefeningen</li>
                  <li> Hotpotatoes</li>
                </ul>
              </li>
              <li>Integreren van BIS-oefeningen</li>
            </ul>              
            </td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">12</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">13</td>
            <td>SNT</td>
            <td><blockquote>
              <p>Werken  met groepen + invloed op alle andere onderdelen</p>
            </blockquote></td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td  align="center">14</td>
            <td>Thuis</td>
            <td><p>Oefeningen</p></td>
          </tr>
          <tr bgcolor="#FFFFCC">
            <td  align="center">15</td>
            <td>SNT</td>
            <td><blockquote>
              <p>Upgrade  naar Dokeos 1.8</p>
            </blockquote></td>
          </tr>
        </table>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
      </blockquote></td>
  </tr>
</table>
</body>
</html>
