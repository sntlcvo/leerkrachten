<?php
session_start();
if (($_SESSION['login_ok'])!="ok"){
  header ("location: login.php");
  exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Tools</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="/css/algemeen.css" rel="stylesheet" type="text/css">
<link href="/css/sophie.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {color: #0000FF}
-->
</style>
</head>

<body>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h1>Brochure Dokeos </h1>
      <blockquote>
        <p><a href="brochure/brochure/index.php" target="inhoud"></a>De SNT kreeg van Karel Schiepers (Provinciale Handelsschool te Hasselt) de toelating om de brochure te publiceren op het leerkrachtennet. De brochure mag gebruikt worden voor intern gebruik. Indien je zaken wenst te kopi&euml;ren, aan te passen, ... dan moet je de wel rekening houden met de volgende &quot;Licentieovereenkomst&quot;:</p>
        <blockquote>
          <p class="style1">Iedereen mag deze handleiding gebruiken en aanpassen onder de volgende voorwaarden:</p>
          <ul>
            <li class="style1">De naam van de maker weer te geven.</li>
            <li class="style1">Niet voor commerci&euml;le doeleinden te gebruiken.</li>
            <li class="style1">Wijzigingen en aanpassingen vrij ter beschikking te stellen. (De SNT heeft hierop een uitzondering gekregen voor het opleiden van de leerkrachten) </li>
          </ul>
          <p>&nbsp;</p>
        </blockquote>
        <p>Er ontbreken een aantal onderdelen in de brochure. Het was de bedoeling dat de brochure zou vervolledigd worden, maar aangezien de Provinciale Handelsschool te Hasselt besloten heeft om in de toekomst te werken met Smartschool, zal de brochure niet meer afgewerkt worden.</p>
        <table width="949" border="0">
          <tr>
            <td width="100"><img src="teacher_100.png" alt="mr dokeos" width="100" height="100"></td>
            <td width="833"><blockquote>
              <p>Wens je ook kennis te maken met Mr Dokeos? Schrijf je dan in voor de cursus &quot;Elektronische leerplatform - dokeos&quot; op vrijdagnamiddag (tweede semester schooljaar 2006-2007).</p>
            </blockquote></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p><a href="brohure.pdf" target="inhoud">Klik hier om de brochure te downloaden in pdf-formaat. </a> </p>
      </blockquote></td>
  </tr>
</table>
</body>
</html>
