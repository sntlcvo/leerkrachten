<?php
/**
 * script to generate all user-data from a given classcode
 * @author tim brouckaert
 * @version 2008.1
 * @copyright snt
 */

/**
 * klascode zit in sntsite.dokeos_klas_dokeos_code --> dokeoscode uithalen
 * user zit in dokeos_main.user -->userid + alle gegevens
 * relatie: dokeos_main.course_rel_user -->
 */

require '../include/inc.classes.php';
ini_set("display_errors", "on");
$objCourses = new dokeosCourses();

if (isSet($_POST['classcode'])){
	//lijst met de juiste cursistgegevens weergeven
	$gegevens = $objCourses->findUsers($_POST['classcode']);
	if (is_array($gegevens)){
		$klasCode = $objCourses->findClass($_POST['classcode']);
		$lijst = "<tr><td colspan=\"3\">dit zijn de gevonden cursisten voor $klasCode</td></tr>";
		foreach ($gegevens as $cursist){
			$naam = htmlentities($cursist['lastname']);
			$voornaam = htmlentities($cursist['firstname']);
			$username = htmlentities($cursist['username']);
			$lijst .= "<tr><td>$naam</td><td>$voornaam</td><td>$username</td></tr>\n";
		}
		$lijst .= "<tr><td colspan=\"3\">het wachtwoord is van de vorm JJJJMMDD<br>
		vb: geboortedatum: 01 september 1974 --&gt; 19740901</td></tr>";
	} else {
		$lijst = "<tr><td>geen cursisten gevonden</td></tr>";
	}
}

$klasDokeosOptions = $objCourses->buildClassCodes($_POST['classcode']);


?>
<html>
<head>
<title>Leerkrachtennet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/algemeen.css" rel="stylesheet" type="text/css">
<link href="../css/sophie.css" rel="stylesheet" type="text/css">
</head>

<body>

<form method="post" action="<?php print $_SERVER['PHP_SELF']; ?>">kies
uw klascode: <select name="classcode">
<?php print $klasDokeosOptions; ?>
</select> <input type="submit" value="verzenden"></form>
<table>
<?php print $lijst; ?>
</table>
</body>
</html>
