<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require_once 'class.db.php';
class dokeosCourses extends db
{
	private $_tblCourses = "course";
	private $_codesLijst = "";
	
	function __construct(){
		try {
			parent::__construct("dokeos");
		} catch (Exception $e){
			print $e->getMessage();
			exit();
		}
		
	}
	
	/**
	 * lijst opbouwen van alle cursuscodes, 
	 * deze worden uit dokeos-db gehaald
	 *
	 * @return string
	 */
	function buildCourseCodes($klasCode)
	{
		$this->getCourseCodes();
		//$codesLijst = $this->getCourseCodes();
		$objKL = new sntSiteKlassen();
		$gegeven = $objKL->haalKlasDokeosCode($klasCode);
		if (is_array($this->_codesLijst)){
			$options = <<<eod
<option value="0">maak uw keuze</option>
<option value="0">niet van toepassing</option>
<option value="ZZZZ">terug verwijderen</option>

eod;
			foreach ($this->_codesLijst as $codeGegevens){
				$selected = ($gegeven['dokeoscode'] == $codeGegevens['code']) ? "selected" : "";
				$options .= <<<eod
<option value="{$codeGegevens['code']}" $selected>{$codeGegevens['title']}</option>
 
eod;
			}
		} else {
			$options = "";
		}
		return $options;
		
	}
	
	/**
	 * codes en titles uit de db halen
	 *
	 * @return array
	 */
	function getCourseCodes(){
		$this->query = "SELECT code, title 
		FROM $this->_tblCourses
		ORDER BY title";
		return $this->_codesLijst = $this->execute_query("gegevens");
	}
}
?>