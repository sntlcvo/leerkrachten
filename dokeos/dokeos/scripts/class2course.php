<?php require 'class2courseBuilder.php' ?>
<html>
<head>
<title>importeren csv-bestand</title>
</head>
<body>
<font color="red">
<?php if (isSet($meldingen)): ?>
<?php foreach ($meldingen as $melding): ?>
<?php print $melding ?><br />
<?php endforeach;?>
<?php endif; ?>

<?php if (isSet($foute_klas_lijst)): ?>
<br /><br />onderstaande klassen bestonden nog niet en dienen eerst aangemaakt te worden<br>
<?php foreach ($foute_klas_lijst as $klas): ?>
<?php print $klas ?><br />
<?php endforeach ?>
<?php endif;?>

<?php if (isSet($foute_cursus_lijst)): ?>
<br /><br/>onderstaande cursussen bestonden nog niet en dienen eerst aangemaakt te worden<br>
<?php foreach ($foute_cursus_lijst as $cursus): ?>
<?php print $cursus ?><br />
<?php endforeach; ?>
<?php endif; ?>

<?php 
	if (isSet($bestaat_al)){
		print "<br><br>onderstaande cursussen-klassen bestonden wel al en werden dus niet aangemaakt<br>";
		foreach ($bestaat_al as $regel){
			print "$regel<br>";
		}
	}
?>
</font>
<form method="post" enctype="multipart/form-data" action="<?php print $_SERVER['PHP_SELF']; ?>">
	geef het csv-bestand op om klassen aan cursussen toe te voegen. Dit bestand moet van de vorm:<br>
	&nbsp;&nbsp;&nbsp;<em>klas;cursus</em><br>
	zijn. Zowel klas als cursus moeten al bestaan.
	<input type="file" name="csvbestand">
	<input type="submit" name="submit" value="ok">
</form>
</body>
</html>
