<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require_once 'class.sntsiteklassen.php';
require_once 'class.dokeoscourses.php';
session_start();
//session_start();
//print_r($_POST);
//eerst nog id van periode ophalen -> OK
//haal alle klascodes op uit de leerkrachtensite
//haal alle codes op uit dokeos
//genereer een lijst waarbij de klascode wordt weergeven
//en een selectielijst met alle dokeoscodes
//resultaat wegschrijven naar db
//achteraf mogelijk om te exporteren naar csv

//stap 1
//lijst ophalen voor de periodes
unset($_SESSION['periodeId']);
unset($_SESSION['currLetter']); 
$objLK = new sntSiteKlassen();
$periodeLijst = $objLK->bouwPeriodes();

for ($i = 97; $i <= 122; $i++){
	$letter = chr($i);
	//de effectieve waarde moet eentje lager zijn dan waar we willen beginnen
	//in de volgende pagina wordt er eentje bijgeteld
	$prevLetter = chr($i-1);
	$lijstLetters .= "<option value=\"$prevLetter\">$letter</option>";
}

?>
<html>
<head>
<title>genereren van klascodes</title>
</head>
<body>
maak uw keuze uit deze periodes:<br>
<form method="post" action="classcodegenerator2.php">
kies de periode: <select name="periodeId"><?php print $periodeLijst; ?></select><br>
kies de letter waarbij je wilt beginnen: <select name="letter"><?php print $lijstLetters; ?></select><br>
<input type="submit" name="submit" value="periode kiezen">
</form>
</body>
</html>
