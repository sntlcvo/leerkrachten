<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require_once 'class.sntsiteklassen.php';
require_once 'class.dokeoscourses.php';
session_start();

//opbouwen csv, en een link maken om het bestand te downloaden
//csv is opgebouwd als klascode;dokeoscode;

//haal alle gegevens op uit de db
$objLK = new sntSiteKlassen();
$lijst = $objLK->haalAlleKlasDokeosCodes();

foreach($lijst as $klassen){
	$klasCode = $klassen['klascode'];
	$dokeosCode = $klassen['dokeoscode'];
	$regels[] = "$klasCode;$dokeosCode\n";
}

file_put_contents("tmp/lijst.csv", $regels);


?>
<html>
<head>
<title>aanmaken csv-bestand</title>
</head>
<body>
<h1>aanmaken van het csv-bestand</h1>
<a href="tmp/lijst.csv">download het bestand</a>
<br>
<a href="classcodegenerator.php">terug naar het begin</a>

</body>