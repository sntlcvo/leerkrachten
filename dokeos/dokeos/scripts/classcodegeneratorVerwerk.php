<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require_once 'class.sntsiteklassen.php';
require_once 'class.dokeoscourses.php';
session_start();
//print_r($_POST);
//print_r($_SESSION);
//resultaat wegschrijven naar db
//achteraf mogelijk om te exporteren naar csv

if (is_array($_POST['klascodes'])){
	foreach($_POST['klascodes'] as $klasCode => $dokeosCode){
		if ($dokeosCode != '0'){
			$objKL = new sntSiteKlassen();
			if ($dokeosCode == 'ZZZZ'){
				//het gegeven moet verwijderd worden
				$objKL->verwijderKlasDokeosCode($klasCode);
			} else {
				//het gegeven moet bijgewerkt / toegevoegd worden
				$objKL->schrijfKlasDokeosCode($klasCode, $dokeosCode);
			}
		}
	}
}

if ($_SESSION['currLetter'] == "z"){
	//spring naar het eindblad
	header("location: classcodegeneratorEind.php");
} else {
	//spring terug naar de ingave
	header("location: classcodegenerator2.php");
}

?>