<?php
session_start();
if (($_SESSION['login_ok'])!="ok"){
	header ("location: login.php");
	exit();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Cursistennet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="../../../css/algemeen.css" rel="stylesheet" type="text/css">
<link href="../../../css/sophie.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<h1>Toevoegen cursisten </h1>
		<p>De cursisten worden automatisch toegevoegd aan jouw cursus. Dit gebeurt elke vrijdag (eerste 4 weken van het semester).
			Indien je wenst dat jouw cursisten worden toegevoegd aan hun cursus,
			stuur een mailtje naar sophie.d@snt.be met de volgende gegevens:      </p>
		<ul>
			<li>klascode</li>
			<li>cursuscode<br>
			De lijst met klascodes kan je <a href="lijst.php" target="inhoud">hier</a> vinden: <a href="lijst.php" target="inhoud">lijst</a></li>
		</ul>
		<p>Hoe kan een cursist dan inloggen? </p>
		<ul>
			<li>Gebruikersnaam = snt + stamnummer.
				De cursist kan zijn stamnummer vinden op zijn inschrijvingsfiche of op zijn studentenkaart.</li>
			<li>Wachtwoord = geboortedatum van de cursist.</li>
			<li>Emailadres = iedere cursist krijgt een standaard email-adres.
				De eerste keer dat hij of zij inlogt is het de bedoeling dat de cursist via &quot;Mijn Profiel&quot; (knop bovenaan) zijn emailadres wijzigt.
				De cursist kan enkel zijn email-adres, naam en voornaam wijzigen.</li>
		</ul>
		<p>Lijsten met ingeschreven cursisten: </p>
		<ul>
		<li>Klik op het volgend bestand om alle ingeschreven cursisten van het eerste semester terug te vinden:
			<a href="dokeos - ingeschreven cursisten - volledig -eerste sem.xls">dokeos - ingeschreven cursisten -eerste sem.xls</a>
		</li>
        	<li>Klik op het volgend bestand om alle ingeschreven cursisten van het eerste semester terug te vinden:
        		<a href="dokeos - ingeschreven cursisten - volledig -tweede sem.xls">dokeos - ingeschreven cursisten -tweede sem.xls</a></li>
	</ul>
	</td>
</tr>
</table>
</body>
</html>