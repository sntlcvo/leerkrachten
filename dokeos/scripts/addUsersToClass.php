<?php
//set database => dokeos_main
$database = "dokeos_main";
$host = "localhost";

//set database user=> root
//set database user Pwd => kabopre
$user = "root";
$pwd = "kabopre";

//where is the file going to be uploaded
$uploadPath = "invoer/";

//get current page url
$currentPage = $_SERVER['PHP_SELF'];

function csvstring_to_array($string, $separatorChar = ';', $enclosureChar = '"', $newlineChar = "\n") {
    // @author: Klemen Nagode
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue = "";
    $isEnclosured = false;
    for ($i = 0; $i < $size; $i++) {

        $char = $string{$i};
        $addChar = "";

        if ($isEnclosured) {
            if ($char == $enclosureChar) {

                if ($i + 1 < $size && $string{$i + 1} == $enclosureChar) {
                    // escaped char
                    $addChar = $char;
                    $i++; // dont check next char
                } else {
                    $isEnclosured = false;
                }
            } else {
                $addChar = $char;
            }
        } else {
            if ($char == $enclosureChar) {
                $isEnclosured = true;
            } else {

                if ($char == $separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue = "";

                    $columnIndex++;
                } elseif ($char == $newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue = "";
                    $columnIndex = 0;
                    $rowIndex++;
                } else {
                    $addChar = $char;
                }
            }
        }
        if ($addChar != "") {
            $fieldValue.=$addChar;
        }
    }

    if ($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }
    return $array;
}

/* Add the original filename to our target path.
  Result is "invoer/filename.extension" */
$filesToProcess = array();
for ($index = 0; $index < count($_FILES["uploadedfile"]); $index++) {

    $targetPath = $uploadPath . basename($_FILES['uploadedfile']['name'][$index]);
    if (!empty($_FILES['uploadedfile']['name'][$index])) {
        if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'][$index], $targetPath)) {
            echo "The file " . basename($_FILES['uploadedfile']['name'][$index]) .
            " has been uploaded<br/>";
            $filesToProcess[] = $targetPath;
        } else {
            echo "There was an error uploading the file, please try again!";
        }
    }
}

//var_dump($filesToProcess);

$arrData = array();
//read all files and process their inner data
foreach ($filesToProcess as $csvFile) {
    $file = fopen($csvFile, 'r');
    //echo $csvFile . "<br/>------------<br/>";

    while (($result = fgetcsv($file)) !== false) {
        if (!strstr($result[0], 'ClassName')) {
            $arrData = array_merge($arrData, csvstring_to_array($result[0]));
        }
    }


    fclose($file);
}


mysql_connect($host, $user, $pwd) or die(mysql_error());
mysql_select_db($database);
//go through all Data en add users to class;
$currentUserIds = array();
$currentClassIds = array();
//build all select queries

$selectQueries = array();

foreach ($arrData as $value) {
    //cfr UserName;ClassCode
    $currentUserCode = $value[0];
    $currentClassName = $value[1];

    $sql = "SELECT c.id, u.user_id FROM class c, user u WHERE c.name = '$currentClassName' and (u.official_code = '$currentUserCode' OR u.username = '$currentUserCode')";

    $selectQueries[] = array($currentUserCode, $currentClassName, $sql);
}

$arrClassUserSql = array();
$selectCourseRelClass = array();
foreach ($selectQueries as $data) {
    //cfr UserName;ClassCode
    $currentUserCode = $data[0];
    $currentClassName = $data[1];
    $query = $data[2];

    // Get all the data from the "example" table
    $rs = mysql_query($query) or die(mysql_error() . $query);
    if (mysql_num_rows($rs)) {
        $r = mysql_fetch_row($rs);
        if (!empty($r[0]) && !empty($r[1])) {
            $CourseRelClassSql = "select course_code from course_rel_class where class_id =$r[0]";
            $selectCourseRelClass[] = array($r[0], $r[1], $CourseRelClassSql);

            $arrClassUserSql[] = '("' . mysql_real_escape_string($r[0]) . '", ' . $r[1] . ')';
        }
    }
}




//check if inserts are found in course _rel -> if found append to insert for course_rel_class later on
$arrCourseRelUser=array();
foreach ($selectCourseRelClass as $data) {
    $currentUserId = $data[1];
    $currentClassCode = $data[0];
    $qry = $data[2];

    $rs = mysql_query($qry) or die(mysql_error() . $qry);
    //var_dump($qry);
    if (mysql_num_rows($rs)) {
        $r = mysql_fetch_row($rs);

        /*
         * course_code 	user_id 	status 	role 	group_id 	tutor_id 	sort 	user_course_cat 	relation_type
         */
        $arrCourseRelUser[] = '("' . mysql_real_escape_string($r[0]) . '", ' . $currentUserId . ',5,NULL,0,0,NULL,0,0)';
    }
}

//insert data into class_user && course_rel_class before exiting
mysql_query('INSERT IGNORE INTO class_user (class_id, user_id) VALUES '.implode(',', $arrClassUserSql));
mysql_query('INSERT IGNORE INTO course_rel_user (course_code,user_id,status,role,group_id,tutor_id,sort,user_course_cat,relation_type) VALUES '.implode(',', $arrCourseRelUser));

//echo 'INSERT IGNORE INTO class_user (class_id, user_id) VALUES '.implode(',', $arrClassUserSql);
//echo 'INSERT IGNORE INTO course_rel_user (course_code,user_id,status,role,group_id,tutor_id,sort,user_course_cat,relation_type) VALUES '.implode(',', $arrCourseRelUser);
mysql_close();

?>

<form enctype="multipart/form-data" action="<?php echo $currentPage ?>" method="POST">
    <table>
        <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
        <tr><td>Selecteer het cursisten bestand <br/><i>(UserName;ClassName)</i></td> <td><input name="uploadedfile[]" type="file" multiple /></td></tr>
        <tr>
            <td colspan="2"><input style="float:right;margin-top:0.5em" type="submit" value="Cursisten toevoegen" /><td>
        </tr>
    </table>
</form>