<?php
class db
{
	private $_conn;
	private $_user = "root";
	private $_password = "kabopre";
	private $_host = "localhost";
	private $_database_dokeos = "dokeos_main";
	private $_database_sntsite = "sntsite";
	protected $query = null;

	public function __construct($what)
	{
		switch ($what){
			case "dokeos":
				$db = $this->_database_dokeos;
				break;
			case "sntsite":
				$db = $this->_database_sntsite;
				break;
			default:
				throw new exception ("Geen geldige database opgegeven");
		}
		$this->_conn = mysql_connect($this->_host, $this->_user, $this->_password) or $this->mysql_fout();
		mysql_select_db($db, $this->_conn) or $this->mysql_fout();
	}

	/**
	 * uitvoeren van een query
	 *
	 * @param string $weergeven (gegevens, aantal, wijzigingen)
	 * @param boolean $eenGegevenVerwacht -> exact 1 gegeven
	 * @return array
	 */
	protected function execute_query($weergeven, $eenGegevenVerwacht = false)
	{
		if ($this->query == null){
			throw new Exception("Geen query opgegeven");
		}
 		$sql = mysql_query($this->query) or $this->mysql_fout();
		switch ($weergeven){
			case "gegevens":
				//geef de gegevens weer
				while ($gegevens = mysql_fetch_array($sql, MYSQL_ASSOC)){
					$gegevensArray[] = $gegevens;
				}
				if ($eenGegevenVerwacht && count($gegevensArray) == 1){
					return $gegevensArray[0];
				} else {
					return $gegevensArray;
				}
				break;
			case "aantal":
				//return het aantal toegevoegde gegevens
				return mysql_num_rows($sql);
				break;
			case "wijzigingen":
				return mysql_affected_rows();
				break;
			default:
				throw new Exception("ongeldige weergave");
		}
	}

	protected function mysql_fout()
	{
		die(mysql_errno() . ": " .mysql_error());
		exit();
	}
}
?>