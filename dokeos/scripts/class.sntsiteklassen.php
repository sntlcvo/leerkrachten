<?php
require_once 'class.db.php';
class sntSiteKlassen extends db
{
	private $_tblLessenrooster = "lessenrooster";
	private $_tblPeriodes = "lessenrooster_periode";
	private $_tblKlasDokeosCode = "dokeos_klas_dokeos_code";

	function __construct()
	{
		try {
			parent::__construct("sntsite");
		} catch (Exception $e){
			print $e->getMessage();
			exit();
		}
	}

	/**
	 * haalt alle gegevens van de periodes op
	 *
	 * @return array
	 */
	function getPeriodes(){
		$this->query = "SELECT id, semester, schooljaar
		FROM $this->_tblPeriodes
		ORDER BY schooljaar DESC, semester DESC";
		return $this->execute_query("gegevens");
	}

	/**
	 * lijst opbouwen van de periodes
	 *
	 * @return string
	 */
	function bouwPeriodes(){
		$lijstPeriodes = $this->getPeriodes();
		if (is_array($lijstPeriodes)){
			foreach ($lijstPeriodes as $periodeGegevens){
				$options .= <<<eod
				<option value="{$periodeGegevens['id']}">semester {$periodeGegevens['semester']} - schooljaar {$periodeGegevens['schooljaar']}</option>
 
eod;
			}
		} else {
			$options = "";
		}
		return $options;
	}

	/**
	 * haalt de klascodes en cursusnamen op uit de lessenroosters
	 *
	 * @return array
	 */
	/**
	 * haal de klascodes op uit de lessenrooster tabel
	 *
	 * @param string $letter
	 * @return array
	 */
	function getAlleKlascodes($periodeId, $letter = null)
	{
		if ($letter == null){
			$this->query = "SELECT klascode, cursusnaam
			FROM $this->_tblLessenrooster
			WHERE cursusnaam != '' AND periode_id = $periodeId
			ORDER BY cursusnaam";
		} else {
			$this->query = "SELECT klascode, cursusnaam
			FROM $this->_tblLessenrooster
			WHERE cursusnaam LIKE '$letter%' AND periode_id = $periodeId
			ORDER BY cursusnaam";
		}
		return $this->execute_query("gegevens");
	}

	/**
	 * voert controle uit of een klascode al bestaat,
	 * en voegt de dokeoscode toe, of doet een update van de tabel
	 *
	 * @param string $klasCode
	 * @param string $dokeosCode
	 */
	function schrijfKlasDokeosCode($klasCode, $dokeosCode)
	{
		//eerst controle als klascode al bestaat, als deze bestaat, doe een update
		//anders voer een insert uit
		if (!$this->zoekKlasCode($klasCode)){
			//de code bestaat nog niet, dus moet ze toegevoegd worden
			$this->query = "INSERT INTO $this->_tblKlasDokeosCode
			(klascode, dokeoscode) VALUES ('$klasCode', '$dokeosCode')";
		} else {
			//de code bestaat wel, dus moet ze upgedatet worden
			$this->query = "UPDATE $this->_tblKlasDokeosCode
			SET dokeoscode = '$dokeosCode' WHERE klascode = '$klasCode'";
		}
		$this->execute_query("wijzigingen");
	}

	/**
	 * verwijdert de klascode
	 *
	 * @param string $klasCode
	 */
	function verwijderKlasDokeosCode($klasCode)
	{
		//eerst controle als klascode al bestaat, als deze bestaat, doe een update
		//anders voer een insert uit
		if ($this->zoekKlasCode($klasCode)){
			//de code bestaat nog niet, dus moet ze toegevoegd worden
			$this->query = "DELETE FROM $this->_tblKlasDokeosCode
			WHERE klascode='$klasCode'";
		}
		$this->execute_query("wijzigingen");
	}
	
	/**
	 * zoekt klascode op in de tabel dokeos_klas_dokeos_code
	 * als deze voorkomt wordt er een true teruggegeven, anders een false
	 *
	 * @param string $klasCode
	 * @return array
	 */
	function zoekKlasCode($klasCode)
	{
		$this->query = "SELECT * FROM $this->_tblKlasDokeosCode
		WHERE klascode = '$klasCode'";
		$aantal = $this->execute_query("aantal");
		if ($aantal > 0){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * haalt alle klascodes en dokeoscodes op
	 * geordend op klascode
	 *
	 * @return array
	 */
	function haalAlleKlasDokeosCodes()
	{
		$this->query = "SELECT * FROM $this->_tblKlasDokeosCode ORDER BY klascode";
		return $this->execute_query("gegevens");
	}

	/**
	 * haalt de dokeoscode op adh van de klascode
	 *
	 * @param string $klascode
	 * @return array
	 */
	function haalKlasDokeosCode($klascode)
	{
		$this->query = "SELECT dokeoscode FROM $this->_tblKlasDokeosCode
		WHERE klascode = '$klascode'";
		return $this->execute_query("gegevens", true);

	}
}
?>