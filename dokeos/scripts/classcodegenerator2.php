<?php
/**
 * @package dokeosTim
 * @author Tim brouckaert
 * @copyright 18/08/2008 for SNT
 */
require_once 'class.sntsiteklassen.php';
require_once 'class.dokeoscourses.php';
session_start();
//print_r($_POST);
//print_r($_SESSION);

//haal alle klascodes op uit de leerkrachtensite ->ok
//haal alle codes op uit dokeos ->ok
//genereer een lijst waarbij de klascode wordt weergeven ->ok
//en een selectielijst met alle dokeoscodes -> ok
//resultaat wegschrijven naar db
//achteraf mogelijk om te exporteren naar csv

if ($_POST['submit'] == "periode kiezen"){
	$_SESSION['periodeId'] = $_POST['periodeId'];
	$_SESSION['currLetter'] = $_POST['letter'];
	//1 letter terugkeren
} elseif (!isSet($_SESSION['periodeId'])){
	header("location: classcodegenerator.php");
	exit();
}

if (isSet($_SESSION['currLetter'])){
	//de eerste letter bepalen,
	//als deze niet is ingesteld wordt er begonnen met de a
	$prevLetter = $_SESSION['currLetter'];
	$intLetter = ord($prevLetter);
	if ($intLetter >= 97 && $intLetter <= 122){
		$_SESSION['currLetter'] = $currLetter = chr($intLetter+1);
	} else {
		$_SESSION['currLetter'] = $currLetter = 'a';
	}
} else {
	$_SESSION['currLetter'] = $currLetter = 'a';
}
$objLK = new sntSiteKlassen();
$klascodeLijst = $objLK->getAlleKlascodes($_SESSION['periodeId'], $currLetter);
if (!is_array($klascodeLijst)){
	$html = <<<eod
	er zijn geen klascodes die beginnen met de letter '$currLetter'
eod;
} else {
	foreach ($klascodeLijst as $klascodeGegevens){
		$objDok = new dokeosCourses();
		$dokeosCodesLijst = $objDok->buildCourseCodes($klascodeGegevens['klascode']);

		$html .= <<<eod
		{$klascodeGegevens['cursusnaam']} - {$klascodeGegevens['klascode']}
		<select name="klascodes[{$klascodeGegevens['klascode']}]">$dokeosCodesLijst</select><br>

eod;
	}

}
$html .= <<<eod
<input type="submit" name="submit" value="wegschrijven">
		
eod;

?>
<html>
<body>
<h1>klascodes beginnend met de letter '<?php print $currLetter; ?>'</h1>
<form method="post" action="classcodegeneratorVerwerk.php"><?php print $html; ?>
</form>
<br>
<a href="classcodegenerator.php">terug naar het begin</a>
</body>
</html>
