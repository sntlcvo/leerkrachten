<?php require 'indexController.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>SNT Leerkrachtennet</title>
    <meta http-equiv="Content-Type" content="text/html;"/>
    <link href="_css/stijl.css" rel="stylesheet" type="text/css"/>
    <link href="_css/jquery-ui-1.7.2.custom.css" rel="stylesheet"
          type="text/css"/>
    <link href="_css/jquery.treeview.css" rel="stylesheet" type="text/css"/>
    <script src="_js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="_js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"
            charset="utf-8"></script>
    <script src="_js/jquery.form.js" type="text/javascript" charset="utf-8"></script>
    <script src="_js/tab.js" type="text/javascript" charset="utf-8"></script>
    <script src="_js/tabSpecials.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css"
          href="_js/jquery.fancybox/jquery.fancybox.css" media="screen"/>
    <script type="text/javascript"
            src="_js/jquery.fancybox/jquery.easing.1.3.js"></script>
    <script type="text/javascript"
            src="_js/jquery.fancybox/jquery.fancybox-1.2.1.pack.js"></script>
    <script type="text/javascript" src="_js/jquery.select.js"></script>
    <script type="text/javascript" src="_js/jquery.treeview.js"></script>
    <script type="text/javascript" src="_js/basic.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".fbox").fancybox({
                'frameWidth': 600,
                'frameHeight': 600
            });
            $(".fbox_small").fancybox({
                'frameWidth': 600,
                'frameHeight': 335
            });
            $(".fbox_wide").fancybox({
                'frameWidth': 800,
                'frameHeight': 600
            });
            $(".fbox_xwide").fancybox({
                'frameWidth': 1024,
                'frameHeight': 600
            });
            $("#menu").treeview();
        });
    </script>


</head>
<body>
<div id="container">
    <div id="header">
        <div id="headerl"><a href="index.php"><h1
                        style="color: white; font-size: 24pt; position: relative; top: -20px;">SNT Plus</h1></a></div>
        <div id="headerr"><?php if (isSet($_SESSION['loginOK']) && $_SESSION['loginOK']): ?><?php // require 'index_zoeken.php'?>
            <?php endif; ?></div>
    </div>

    <div id="navi"><?php if (isSet($_SESSION['loginOK']) && $_SESSION['loginOK']): ?><?php require 'index_navigatie.php' ?>
        <?php endif; ?></div>

    <div id="content">
        <?php if ($toonIndexTop): ?>
            <div id="top"><?php require 'index_top.php' ?></div>

        <?php endif; ?>
        <?php if (isSet($_SESSION['loginOK']) && $_SESSION['loginOK']): ?>

            <div id="midden">

                <?php if (isset($_SESSION['zimlet']) && $_SESSION['zimlet'] == true): ?>
                    <a href="http://leerkrachten.snt.be/index.php?actie=inhoud&value=zimlet_common">&nbsp;&gt;Terug naar
                        SNT
                        Plus</a>
                <?php endif ?>

                <div id="uitklapmenu" class="moduletable"><?php if (isset($uitklapmenu) && is_array($uitklapmenu)): ?>
                        <h3><?php print $menuTitel; ?></h3>
                        <ul>
                            <?php foreach ($uitklapmenu as $subitem): ?>

                                <?php if ($subitem['zichtbaar_vanaf_groep'] <= $_SESSION['objPersoneel']->getGroep()): ?>
                                    <?php //print_r($subitem)?>
                                    <?php if ($subitem['actie'] == 'toonsubitem'): ?>
                                        <?php $url = '#' ?>
                                        <?php $klasse = 'toonsubitemklasse' ?>
                                    <?php else: ?>
                                        <?php $url = "index.php?actie=submenu__{$subitem['actie']}&value={$subMenuValue}__{$subitem['actie_value']}&extras={$subitem['extras']}"; ?>
                                        <?php $klasse = '' ?>
                                    <?php endif; ?>

                                    <li class="noshow">
                                        <?php if ($subitem['subitem'] == 1): ?>
                                            <span class="subitem menuTeller_<?php print $menuTeller ?>"
                                                  style="position: relative; left: 15px;">
	<ul>
	<li>
	<a href="<?php print $url ?>"><?php print $subitem['naam'] ?></a></li>
	</ul>
	</span>
                                        <?php else: ?>
                                            <?php $menuTeller++ ?>
                                            <a href="<?php print $url ?>"
                                               class="toonsubitem menuTeller_<?php print $menuTeller ?> <?php print $klasse ?>">
                                                <?php print $subitem['naam'] ?></a>
                                        <?php endif; ?>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach ?>
                        </ul>
                    <?php endif; ?></div>

                <div id="inhoud"><?php if (isSet($inhoud)): ?><?php require "_inhoud/$inhoud" ?>
                    <?php else: ?><?php require "_inhoud/default.php" ?><?php endif; ?></div>
            </div>
        <?php endif; ?></div>

    <div id="footer">Copyright SNT-Brugge 2009 - <?php print date('Y') ?></div>

</div>
</body>
</html>
