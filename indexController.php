<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 11-sep-2009
 */
define('BASE_PATH', realpath(dirname(__FILE__)));
require '_controllers/init.php';

$objMenu = new TabLKMenu();
$objMededelingen = new TabMededelingen();
$mededNT2 = $objMededelingen->haalRecentsteBerichten(7);

$mededInfo = $objMededelingen->haalRecentsteBerichten(5);

$mededTalen = $objMededelingen->haalRecentsteBerichten(4);

$subMenuValue = 1;
$hoofdmenu = $objMenu->getHoofdItems();
$toonIndexTop = true;
//$menuTitel = $objMenu->getSubTitle(1);
//$uitklapmenu = $objMenu->getSubItems(1);
if (isSet($_GET['actie'])) {
    $acties = explode("__", $_GET['actie']);
    $values = explode("__", $_GET['value']);
    foreach ($acties as $id => $actie) {
        $gets[$actie] = $values[$id];
    }

    foreach ($gets as $actie => $value) {
        switch ($actie) {
            case 'extern_script':
                header("location: {$_GET['value']}");
                exit();
                break;
            case 'intern_script':
                require "_scripts/" . $_GET['value'] . ".php";
                break;
            case 'submenu':
                $subMenuValue = $value;
                $menuTitel = $objMenu->getSubTitle($value);
                $uitklapmenu = $objMenu->getSubItems($value);
                $toonIndexTop = false;
                break;
            case 'inhoud':
                $params = explode("_-_", $value);
                $inhoud = array_shift($params);
                $menuInhoud = $inhoud;
                $inhoud .= ".php";
                $toonIndexTop = false;
                break;
            case 'document':
                break;
            default:
                //throw new myException("ongeldig actie");
        }
    }
} else {
    $subMenuValue = 1;
}

$menuTeller = 0;