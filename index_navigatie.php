<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 23-sep-2009
 */
//print_r($hoofdmenu);
?>
<ul id="mainlevel-nav">
<?php foreach ($hoofdmenu as $menuitem): ?>
<?php if ($_SESSION['objPersoneel']->getGroep() >= $menuitem['zichtbaar_vanaf_groep']):?>
<li class="noshow">
	<a class="mainlevel-nav" href="index.php?actie=<?php print $menuitem['actie']?>&value=<?php print $menuitem['actie_value']?><?php print ($menuitem['extras'] != "") ? "&extras={$menuitem['extras']}" : "" ?>">
		<?php print $menuitem['naam']?></a></li>
<?php endif;?>
<?php endforeach?>
</ul>