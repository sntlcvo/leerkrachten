<?php
/**
 * @package lknet2010
 * @author tim
 * @version 2009.1 - 23-sep-2009
 */
?>
<?php if (!$_SESSION['loginOK']):?>
	<div id="bovenband"></div>
	<?php else: ?>
	<div id="kolom1" class="moduletable">
		<h3>Andere Talen</h3>
		<div class="kolomInhoud">
		<?php if (is_array($mededTalen)): ?>
		<ul>
		<?php foreach($mededTalen as $bericht): ?>
			<li><b><?php TabFuncties::htmlize(strtolower($bericht['titel']))?></b> (<?php print $bericht['datum']?>)<br />
			<?php print strip_tags(stripslashes($bericht['bericht']))?>...
			</li>
		<?php endforeach;?>
		</ul>
		<a href="index.php?actie=submenu__inhoud&value=1__mededelingen_-_4">meer</a>
		<?php else: ?>
		Geen berichten
		<?php endif;?>
		</div>
		
	</div>
	<div id="kolom2" class="moduletable">
		<h3>Informatica</h3>
		<div class="kolomInhoud">
		<?php if (is_array($mededInfo)): ?>
		<ul>
		<?php foreach($mededInfo as $bericht): ?>
			<li><b><?php TabFuncties::htmlize(strtolower($bericht['titel']))?></b> (<?php print $bericht['datum']?>)<br />
			<?php print strip_tags(stripslashes($bericht['bericht']))?>...
			</li>
		<?php endforeach;?>
		</ul>
		<a href="index.php?actie=submenu__inhoud&value=1__mededelingen_-_5">meer</a>
		<?php else: ?>
		Geen berichten
		<?php endif;?>
		</div>
	</div>
	<?php endif;?>
	<div id="kolom3" class="moduletable">
		<?php if (!$_SESSION['loginOK']):?>
		
		<?php require 'index_logon.php'?>
		<?php else: ?>
		<h3>NT2</h3>
		<div class="kolomInhoud">
		<?php if (is_array($mededNT2)): ?>
		<ul>
		<?php foreach($mededNT2 as $bericht): ?>
			<li><b><?php TabFuncties::htmlize(strtolower($bericht['titel']))?></b> (<?php print $bericht['datum']?>)<br />
			<?php print strip_tags(stripslashes($bericht['bericht']))?>...
			</li>
			<?php endforeach;?>
		</ul>
		<a href="index.php?actie=submenu__inhoud&value=1__mededelingen_-_7">meer</a>
		<?php else: ?>
		Geen berichten
		<?php endif;?>
		</div>
		<?php endif;?>
	</div>