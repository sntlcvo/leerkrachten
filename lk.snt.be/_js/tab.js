function toonVeld(veld, teVerbergenVelden)
{
	//alert (veld);
	if (teVerbergenVelden != null){
		$(teVerbergenVelden).hide();
	}
	$(veld).show();
}

function verbergVeld(veld)
{
	$(veld).hide();
}

function updateVeld(veld, url)
{
	$(veld).load(url);
}

function resetSelect(){
	$('select').val('maak uw keuze'); 
}

function disableVeld(veld){
	alert (veld);
}

function wisVeld(veld, standaardwaarde){
	//alert (standaardwaarde);
	if ($(veld).val() == standaardwaarde){
		$(veld).val("");
	}
}

function disableInvoer(){
	/*first arg = select*/
	/*next args, all fields*/
	selectLijst = arguments[0];
	var waarde = selectLijst.options[selectLijst.selectedIndex].value;
    if (waarde != -99) {
        for (var i = 1; i < arguments.length; i++) {
        	var veld = "#" + arguments[i];
            $(veld).attr('disabled', true);
        }
    }
    else {
        for (var i = 1; i < arguments.length; i++) {
        	var veld = "#" + arguments[i];
        	$(veld).attr('disabled', false);
        }
    }
}

function verzendForm(form, highlightDiv, updateDiv){
	/**
	 * aanpassen zodat updatediv bijgewerkt wordt met de gereturnde waarde
	 */
	//alert(form);
    $(form).ajaxForm(function() { 
    	//alert(highlightDiv);
    		//$(highlightDiv).addClass('rode_achtergrond');
    	lichtop(highlightDiv);
    }
    ); 
}

function lichtop(veld)
{
	$(veld).effect('highlight',
			{
		color: "#00FF00"
	},3000);
}

function kopiewaarde(vanVeld, naarVeld, append, lengte_minder){
	var waarde;
	if ($(vanVeld).is("select")){
		waarde = $(vanVeld + " option:selected").text();
	} else if ($(vanVeld).is("input")){
		waarde = $(vanVeld).val();
	} else {
		alert ("type is ongeldig");
	}
	if (lengte_minder != null){
		var lengte = waarde.length;
		waarde = waarde.substr(0, lengte-lengte_minder);
	}
	if (append == true){
		waarde = $(naarVeld).val() + waarde;
	}
	$(naarVeld).val(waarde);
}

function bevestig(veld)
{
	var waarde = $(veld+':checkbox:checked').val();
	if (waarde){
		var a = confirm("weet u zeker dat u dit item wilt wissen?");
		if (a){
			$(veld).attr('checked', true);
		} else {
			$(veld).attr('checked', false);
		}
	}
}

function createDatepicker(veld)
{
//	$(veld).datepicker();
	$("#datepicker").datepicker({ 
		dateFormat: 'dd-mm-yyyy',
		changeMonth: true,
		changeYear: true
		});
}